package com.exabit.dpromo;

import com.exabit.dpromo.bd.ConexionBD;
import com.exabit.dpromo.bd.controllers.DepartamentoJPAController;
import com.exabit.dpromo.bd.controllers.ParametrosJPAController;
import com.exabit.dpromo.bd.controllers.PromocionJPAController;
import com.exabit.dpromo.bd.controllers.TiendaJPAController;
import com.exabit.dpromo.bd.entidades.Departamento;
import com.exabit.dpromo.bd.entidades.Tienda;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;


public class MainApp extends Application {
    private static final ObjectProperty<PantallaPrincipalController> CONTR_PANTALLA_PRINC=new SimpleObjectProperty<>();
    private static Stage stage; 

    public static Stage getStage() {
        return stage;
    }

    public static void setStage(Stage stage) {
        MainApp.stage = stage;
    }
    

    @Override
    public void start(Stage stage) throws Exception {
        MainApp.stage=stage;
        this.inicializarBaseDatos();
        FXMLLoader loader=new FXMLLoader();
        Parent root = loader.load(getClass().getResourceAsStream("/fxml/main/PantallaPrincipal.fxml"));
        MainApp.CONTR_PANTALLA_PRINC.setValue(loader.getController());
        Scene scene = new Scene(root);
        scene.getStylesheets().add("/styles/Styles.css");
        
        stage.setScene(scene);
        stage.getIcons().add(new Image(getClass().getResourceAsStream("/imagenes/app-dshopping.png")));
        stage.setTitle("DPromo - Gestor de Cupones");
        ParametrosJPAController prjpa=new ParametrosJPAController();
        String ventMax=prjpa.getParametroSistema(ParametrosJPAController.VENTANA_PRINCIPAL_MAX);
        if(ventMax!=null){
            Boolean ventanaMax=Boolean.parseBoolean(ventMax);
            stage.setMaximized(ventanaMax);
        }
        stage.maximizedProperty().addListener((ObservableValue<? extends Boolean> obs, Boolean oldValue, Boolean newValue)->{
            prjpa.setParametroSistema(ParametrosJPAController.VENTANA_PRINCIPAL_MAX, newValue.toString());
        });
        stage.show();
    }
    
    public static void mostrarMensaje(String msg, int tipoMensaje){        
        MainApp.CONTR_PANTALLA_PRINC.getValue().mostrarMensaje(msg, tipoMensaje);
    }
    
    public static void mostrarProgress(boolean mostrar){
        MainApp.CONTR_PANTALLA_PRINC.getValue().mostrarProgress(mostrar);
    }

    private void inicializarBaseDatos(){
        //TIENDAS
        TiendaJPAController tjpa=new TiendaJPAController(ConexionBD.EMF.createEntityManager());
        
        
        if(tjpa.getTodasTiendas().isEmpty()){
            Tienda sin=new Tienda();
            sin.setIdTienda(1);
            sin.setNombre("(Sin Tienda)");
            sin.setActivo(true);
            tjpa.modificarTienda(sin);
            TiendaJPAController.LST_TIENDAS_DEFAULT.stream().forEach((ti)->{
                Tienda tie=ti;
                ti.setIdTienda(null);
                tjpa.registrarTienda(tie);
            });
        }
        
        /*Tienda dportivo=new Tienda();
        dportivo.setIdTienda(2);
        dportivo.setNombre("Dportivo");
        dportivo.setActivo(true);
        tjpa.modificarTienda(dportivo);
        
        Tienda rinconsport=new Tienda();
        rinconsport.setIdTienda(3);
        rinconsport.setNombre("Rincón Sport");
        rinconsport.setActivo(true);
        tjpa.modificarTienda(rinconsport);
        
        Tienda kilk=new Tienda();
        kilk.setIdTienda(4);
        kilk.setNombre("Kilkenny");
        kilk.setActivo(true);
        tjpa.modificarTienda(kilk);
        
        Tienda pelupichi=new Tienda();
        pelupichi.setIdTienda(5);
        pelupichi.setNombre("Pelupichi Park");
        pelupichi.setActivo(true);
        tjpa.modificarTienda(pelupichi);
        
        Tienda tobago=new Tienda();
        tobago.setIdTienda(6);
        tobago.setNombre("Tobago");
        tobago.setActivo(true);
        tjpa.modificarTienda(tobago);
        
        Tienda luomo=new Tienda();
        luomo.setIdTienda(7);
        luomo.setNombre("L'uomo");
        luomo.setActivo(true);
        tjpa.modificarTienda(luomo);
        
        Tienda nice=new Tienda();
        nice.setIdTienda(8);
        nice.setNombre("Nice");
        nice.setActivo(true);
        tjpa.modificarTienda(nice);
        
        Tienda artemisa=new Tienda();
        artemisa.setIdTienda(9);
        artemisa.setNombre("Artemisa");
        artemisa.setActivo(true);
        tjpa.modificarTienda(artemisa);
        
        Tienda rondina=new Tienda();
        rondina.setIdTienda(10);
        rondina.setNombre("Rondina");
        rondina.setActivo(true);
        tjpa.modificarTienda(rondina);
        
        Tienda eustaquia=new Tienda();
        eustaquia.setIdTienda(11);
        eustaquia.setNombre("Ña Eustaquia");
        eustaquia.setActivo(true);
        tjpa.modificarTienda(eustaquia);
        
        Tienda maranello=new Tienda();
        maranello.setIdTienda(12);
        maranello.setNombre("Maranello");
        maranello.setActivo(true);
        tjpa.modificarTienda(maranello);
        
        Tienda buffalo=new Tienda();
        buffalo.setIdTienda(13);
        buffalo.setNombre("Buffalo");
        buffalo.setActivo(true);
        tjpa.modificarTienda(buffalo);
        
        Tienda donvito=new Tienda();
        donvito.setIdTienda(14);
        donvito.setNombre("Don Vito");
        donvito.setActivo(true);
        tjpa.modificarTienda(donvito);
        
        Tienda bellini=new Tienda();
        bellini.setIdTienda(15);
        bellini.setNombre("Bellini");
        bellini.setActivo(true);
        tjpa.modificarTienda(bellini);
        
        Tienda amandau=new Tienda();
        amandau.setIdTienda(16);
        amandau.setNombre("Amandau");
        amandau.setActivo(true);
        tjpa.modificarTienda(amandau);
        
        Tienda vision=new Tienda();
        vision.setIdTienda(17);
        vision.setNombre("Vision Banco");
        vision.setActivo(true);
        tjpa.modificarTienda(vision);
        
        Tienda personal=new Tienda();
        personal.setIdTienda(18);
        personal.setNombre("Personal");
        personal.setActivo(true);
        tjpa.modificarTienda(personal);
        
        Tienda tigo=new Tienda();
        tigo.setIdTienda(19);
        tigo.setNombre("Tigo");
        tigo.setActivo(true);
        tjpa.modificarTienda(tigo);
        
        Tienda citymark=new Tienda();
        citymark.setIdTienda(20);
        citymark.setNombre("City Market");
        citymark.setActivo(true);
        tjpa.modificarTienda(citymark);
        
        Tienda claro=new Tienda();
        claro.setIdTienda(21);
        claro.setNombre("Claro");
        claro.setActivo(true);
        tjpa.modificarTienda(claro);
        */
        
        //DEPARTAMENTOS
        DepartamentoJPAController djpa=new DepartamentoJPAController(ConexionBD.EMF.createEntityManager());
        Departamento depCaaguazu=new Departamento();
        depCaaguazu.setNombre("Caaguazu");
        depCaaguazu.setIdDepartamento(5);
        djpa.modificarDepartamento(depCaaguazu);
        Departamento depGuaira=new Departamento();
        depGuaira.setNombre("Guairá");
        depGuaira.setIdDepartamento(6);
        djpa.modificarDepartamento(depGuaira);
        
        //CIUDADES
        /*CiudadJPAController ciujpa=new CiudadJPAController(ConexionBD.EMF.createEntityManager());
        Ciudad ciuCnelOviedo=new Ciudad();
        ciuCnelOviedo.setIdCiudad(4);
        ciuCnelOviedo.setNombre("Coronel Oviedo");
        ciuCnelOviedo.setDepartamento(depCaaguazu);
        ciujpa.modificarCiudad(ciuCnelOviedo);
        
        Ciudad ciuCaaguazu=new Ciudad();
        ciuCaaguazu.setIdCiudad(2);
        ciuCaaguazu.setNombre("Caaguazú");
        ciuCaaguazu.setDepartamento(depCaaguazu);
        ciujpa.modificarCiudad(ciuCaaguazu);
        
        Ciudad ciuCarayao=new Ciudad();
        ciuCarayao.setIdCiudad(3);
        ciuCarayao.setNombre("Carayaó");
        ciuCarayao.setDepartamento(depCaaguazu);
        ciujpa.modificarCiudad(ciuCarayao);
        
        Ciudad ciuCecilio=new Ciudad();
        ciuCecilio.setIdCiudad(6);
        ciuCecilio.setNombre("Cecilio Báez");
        ciuCecilio.setDepartamento(depCaaguazu);
        ciujpa.modificarCiudad(ciuCecilio);
        
        Ciudad ciuEulogio=new Ciudad();
        ciuEulogio.setIdCiudad(7);
        ciuEulogio.setNombre("J. Eulogio Estigarribia");
        ciuEulogio.setDepartamento(depCaaguazu);
        ciujpa.modificarCiudad(ciuEulogio);
        
        Ciudad ciuJuanManuel=new Ciudad();
        ciuJuanManuel.setIdCiudad(8);
        ciuJuanManuel.setNombre("Juan Manuel Frutos");
        ciuJuanManuel.setDepartamento(depCaaguazu);
        ciujpa.modificarCiudad(ciuJuanManuel);
        
        Ciudad ciuJoseDomingo=new Ciudad();
        ciuJoseDomingo.setIdCiudad(9);
        ciuJoseDomingo.setNombre("José Domingo Ocampos");
        ciuJoseDomingo.setDepartamento(depCaaguazu);
        ciujpa.modificarCiudad(ciuJoseDomingo);
        
        Ciudad ciuPastora=new Ciudad();
        ciuPastora.setIdCiudad(10);
        ciuPastora.setNombre("La Pastora");
        ciuPastora.setDepartamento(depCaaguazu);
        ciujpa.modificarCiudad(ciuPastora);
        
        Ciudad ciuFranciscoLopez=new Ciudad();
        ciuFranciscoLopez.setIdCiudad(11);
        ciuFranciscoLopez.setNombre("Mcal. Francisco S. Lopez");
        ciuFranciscoLopez.setDepartamento(depCaaguazu);
        ciujpa.modificarCiudad(ciuFranciscoLopez);
        
        Ciudad ciuNuevaLondres=new Ciudad();
        ciuNuevaLondres.setIdCiudad(12);
        ciuNuevaLondres.setNombre("Nueva Londres");
        ciuNuevaLondres.setDepartamento(depCaaguazu);
        ciujpa.modificarCiudad(ciuNuevaLondres);
        
        Ciudad ciuRArsenio=new Ciudad();
        ciuRArsenio.setIdCiudad(13);
        ciuRArsenio.setNombre("Raúl Arsenio Oviedo");
        ciuRArsenio.setDepartamento(depCaaguazu);
        ciujpa.modificarCiudad(ciuRArsenio);
        
        Ciudad ciuRepatriacion=new Ciudad();
        ciuRepatriacion.setIdCiudad(14);
        ciuRepatriacion.setNombre("Repatriación");
        ciuRepatriacion.setDepartamento(depCaaguazu);
        ciujpa.modificarCiudad(ciuRepatriacion);
        
        Ciudad ciuRI=new Ciudad();
        ciuRI.setIdCiudad(15);
        ciuRI.setNombre("R.I. Tres Corrales");
        ciuRI.setDepartamento(depCaaguazu);
        ciujpa.modificarCiudad(ciuRI);
        
        Ciudad ciuSanJoaquin=new Ciudad();
        ciuSanJoaquin.setIdCiudad(16);
        ciuSanJoaquin.setNombre("San Joaquín");
        ciuSanJoaquin.setDepartamento(depCaaguazu);
        ciujpa.modificarCiudad(ciuSanJoaquin);
        
        Ciudad ciuSanJose=new Ciudad();
        ciuSanJose.setIdCiudad(17);
        ciuSanJose.setNombre("San José de los Arroyos");
        ciuSanJose.setDepartamento(depCaaguazu);
        ciujpa.modificarCiudad(ciuSanJose);
        
        Ciudad ciuSantaRosa=new Ciudad();
        ciuSantaRosa.setIdCiudad(18);
        ciuSantaRosa.setNombre("Santa Rosa del Mbutuy");
        ciuSantaRosa.setDepartamento(depCaaguazu);
        ciujpa.modificarCiudad(ciuSantaRosa);
        
        Ciudad ciuSimonBolivar=new Ciudad();
        ciuSimonBolivar.setIdCiudad(19);
        ciuSimonBolivar.setNombre("Simón Bolívar");
        ciuSimonBolivar.setDepartamento(depCaaguazu);
        ciujpa.modificarCiudad(ciuSimonBolivar);
        
        Ciudad ciuTresFebrero=new Ciudad();
        ciuTresFebrero.setIdCiudad(20);
        ciuTresFebrero.setNombre("Tres de Febrero");
        ciuTresFebrero.setDepartamento(depCaaguazu);
        ciujpa.modificarCiudad(ciuTresFebrero);
        
        Ciudad ciuVaqueria=new Ciudad();
        ciuVaqueria.setIdCiudad(21);
        ciuVaqueria.setNombre("Vaquería");
        ciuVaqueria.setDepartamento(depCaaguazu);
        ciujpa.modificarCiudad(ciuVaqueria);
        
        Ciudad ciuYhu=new Ciudad();
        ciuYhu.setIdCiudad(22);
        ciuYhu.setNombre("Yhú");
        ciuYhu.setDepartamento(depCaaguazu);
        ciujpa.modificarCiudad(ciuYhu);
        
        Ciudad ciuTembiapora=new Ciudad();
        ciuTembiapora.setIdCiudad(23);
        ciuTembiapora.setNombre("Tembiaporá");
        ciuTembiapora.setDepartamento(depCaaguazu);
        ciujpa.modificarCiudad(ciuTembiapora);
        
        Ciudad ciuNuevaToledo=new Ciudad();
        ciuNuevaToledo.setIdCiudad(24);
        ciuNuevaToledo.setNombre("Nueva Toledo");
        ciuNuevaToledo.setDepartamento(depCaaguazu);
        ciujpa.modificarCiudad(ciuNuevaToledo);
        
        ClienteJPAController clijpa=new ClienteJPAController(ConexionBD.EMF.createEntityManager());
        Cliente c=new Cliente();
        c.setCi(999999);
        c.setNombres("Juan");
        c.setApellidos("Perez");
        c.setIdCliente(1);
        c.setCelular("(0000)000000");
        c.setTelefono("000000");
        c.setEmail("juanperez@mail.com");
        c.setDireccion("Calle Uno c/ Calle Dos Nro. 123");
        c.setCiudad(ciuCnelOviedo);
        clijpa.modificarCliente(c);*/
        
        PromocionJPAController projpa=new PromocionJPAController(ConexionBD.EMF.createEntityManager());
        projpa.crearPromocionDefault();
    }

    /**
     * The main() method is ignored in correctly deployed JavaFX application.
     * main() serves only as fallback in case the application can not be
     * launched through deployment artifacts, e.g., in IDEs with limited FX
     * support. NetBeans ignores main().
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

}
