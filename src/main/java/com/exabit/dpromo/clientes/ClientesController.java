/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exabit.dpromo.clientes;

import com.exabit.dpromo.bd.ConexionBD;
import com.exabit.dpromo.bd.controllers.CiudadJPAController;
import com.exabit.dpromo.bd.controllers.ClienteJPAController;
import com.exabit.dpromo.bd.entidades.Ciudad;
import com.exabit.dpromo.bd.entidades.Cliente;
import com.exabit.dpromo.generador.CiudadListCell;
import java.net.URL;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Accordion;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListView;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.TitledPane;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;

/**
 * FXML Controller class
 *
 * @author cristhian
 */
public class ClientesController implements Initializable {

    @FXML
    private Button btnNuevoCliente;
    @FXML
    private TextField txfBusarClientes;
    @FXML
    private TableView<Cliente> tblClientes;
    @FXML
    private TableColumn<Cliente, Integer> colCodigoCliente;
    @FXML
    private TableColumn<Cliente, String> colNombresCliente;
    @FXML
    private TableColumn<Cliente, String> colApellidosCliente;
    @FXML
    private TableColumn<Cliente, String> colCiCliente;
    @FXML
    private Accordion accPrincipalClientes;
    @FXML
    private TitledPane tpaneDatosPrinc;
    @FXML
    private TitledPane tpaneDatosExtras;
    @FXML
    private TextField txfCodigoCliente;
    @FXML
    private TextField txfNombresCliente;
    @FXML
    private TextField txfApellidosCliente;
    @FXML
    private TextField txfCiCliente;
    @FXML
    private ComboBox<Ciudad> cmbCiudadCliente;
    @FXML
    private TextField txfDireccionCliente;
    @FXML
    private TextField txfTelefonoCliente;
    @FXML
    private TextField txfCelularCliente;
    @FXML
    private TextField txfEmailCliente;
    @FXML
    private VBox vboxDatosPrinc;
    @FXML
    private Button btnEliminarCliente;
    @FXML
    private Button btnGuardarCliente;
    @FXML
    private ImageView imgvBuscar;
    
    private final ObservableList<Cliente> lstClientes=FXCollections.observableArrayList();
    private final ObservableList<Ciudad> lstCiudadesCombo=FXCollections.observableArrayList();
    private final SimpleObjectProperty<Cliente> clienteSeleccionado=new SimpleObjectProperty<>();
    
    private static final int OPERACION_REGISTRAR=1;
    private static final int OPERACION_MODIFICAR=2;
    
    private int operacionActual=ClientesController.OPERACION_REGISTRAR;
    
    
    private final ChangeListener<Cliente> chLnrSeleccionCliente=(ObservableValue<? extends Cliente> obs, Cliente oldValue, Cliente newValue)->{
        if(newValue!=null){
            this.clienteSeleccionado.setValue(newValue);
        }
    };
    
    private final ChangeListener<Cliente> chLnrClienteActual=(ObservableValue<? extends Cliente> obs, Cliente oldValue, Cliente newValue)->{
        if(newValue!=null){
            this.operacionActual=ClientesController.OPERACION_MODIFICAR;
            
            Image imgGuardar=new Image(getClass().getResourceAsStream("/iconos/x16/edit.png"));
            this.btnGuardarCliente.setGraphic(new ImageView(imgGuardar));
            this.btnGuardarCliente.setText("Modificar");
            
            this.txfCodigoCliente.setText(newValue.getIdCliente().toString());
            this.txfNombresCliente.setText(newValue.getNombres());
            this.txfApellidosCliente.setText(newValue.getApellidos());
            this.txfCiCliente.setText(newValue.getCi().toString());
            for(Ciudad ciu : lstCiudadesCombo){
                if(ciu.getIdCiudad().equals(newValue.getCiudad().getIdCiudad())){
                    this.cmbCiudadCliente.getSelectionModel().select(ciu);
                    break;
                }
            }
            this.txfDireccionCliente.setText(newValue.getDireccion());
            this.txfTelefonoCliente.setText(newValue.getTelefono());
            this.txfCelularCliente.setText(newValue.getCelular());
            this.txfEmailCliente.setText(newValue.getEmail());
        }else{
            this.operacionActual=ClientesController.OPERACION_REGISTRAR;
            Image imgGuardar=new Image(getClass().getResourceAsStream("/iconos/x16/save.png"));
            this.btnGuardarCliente.setGraphic(new ImageView(imgGuardar));
            this.btnGuardarCliente.setText("Registrar");
            
            this.txfCodigoCliente.setText("");
            this.txfNombresCliente.setText("");
            this.txfApellidosCliente.setText("");
            this.txfCiCliente.setText("");
            this.cmbCiudadCliente.getSelectionModel().clearSelection();
            this.txfDireccionCliente.setText("");
            this.txfTelefonoCliente.setText("");
            this.txfCelularCliente.setText("");
            this.txfEmailCliente.setText("");
        }
    };
    
    private final ChangeListener<String> chLnrCriterioBusqueda=(ObservableValue<? extends String> obs, String oldValue, String newValue)->{
        this.lstClientes.clear();
        ClienteJPAController clijpa=new ClienteJPAController(ConexionBD.EMF.createEntityManager());
        if(newValue!=null){
            if(!newValue.isEmpty()){
                this.lstClientes.addAll(clijpa.buscarCliente(newValue));
            }else{
                this.lstClientes.addAll(clijpa.getTodosLosClientes());
            }
        }else{
            this.lstClientes.addAll(clijpa.getTodosLosClientes());
        }
    };
    
    /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        this.inicializarTabla();
        this.inicalizarComboCiudades();
        this.cargarIconos();
        
        this.tblClientes.getSelectionModel().selectedItemProperty().addListener(this.chLnrSeleccionCliente);
        this.clienteSeleccionado.addListener(this.chLnrClienteActual);
        
        ClienteJPAController clijpa=new ClienteJPAController(ConexionBD.EMF.createEntityManager());
        this.lstClientes.addAll(clijpa.getTodosLosClientes());
        CiudadJPAController cjpa=new CiudadJPAController(ConexionBD.EMF.createEntityManager());
        this.lstCiudadesCombo.addAll(cjpa.getCiudadesOrdenado());
        this.accPrincipalClientes.setExpandedPane(this.tpaneDatosPrinc);
        this.txfBusarClientes.textProperty().addListener(this.chLnrCriterioBusqueda);
    }    
    
    private void cargarIconos(){
        Image imgNuevo=new Image(getClass().getResourceAsStream("/iconos/x16/document.png"));
        this.btnNuevoCliente.setGraphic(new ImageView(imgNuevo));
        
        Image imgGuardar=new Image(getClass().getResourceAsStream("/iconos/x16/save.png"));
        this.btnGuardarCliente.setGraphic(new ImageView(imgGuardar));
        
        Image imgEliminar=new Image(getClass().getResourceAsStream("/iconos/x16/delete.png"));
        this.btnEliminarCliente.setGraphic(new ImageView(imgEliminar));
        
        Image imgBuscar=new Image(getClass().getResourceAsStream("/iconos/x16/search.png"));
        this.imgvBuscar.setImage(imgBuscar);
    }
    
    private void inicializarTabla(){
        this.tblClientes.setItems(this.lstClientes);
        this.colCodigoCliente.setCellValueFactory(new PropertyValueFactory<>("idCliente"));
        this.colNombresCliente.setCellValueFactory(new PropertyValueFactory<>("nombres"));
        this.colApellidosCliente.setCellValueFactory(new PropertyValueFactory<>("apellidos"));
        this.colCiCliente.setCellValueFactory(new PropertyValueFactory<>("ci"));
    }
    
    private void inicalizarComboCiudades(){
        this.cmbCiudadCliente.setItems(this.lstCiudadesCombo);
        this.cmbCiudadCliente.prefWidthProperty().bind(this.vboxDatosPrinc.widthProperty());
        this.cmbCiudadCliente.setCellFactory((ListView<Ciudad> data)->new CiudadListCell());
        this.cmbCiudadCliente.setButtonCell(new CiudadListCell());
    }

    @FXML
    private void onActionBtnNuevo(ActionEvent event) {
        this.clienteSeleccionado.setValue(null);
    }

    @FXML
    private void onActionBtnGuardarCliente(ActionEvent event) {
        if(this.operacionActual==ClientesController.OPERACION_REGISTRAR){
            this.registrarCliente();
        }else{
            this.modificarCliente();
        }
    }

    @FXML
    private void onActionBtnEliminarCliente(ActionEvent event) {
        this.eliminarCliente();
    }
    
    private void registrarCliente(){
        if(this.validacionCampos()){
            ClienteJPAController clijpa=new ClienteJPAController(ConexionBD.EMF.createEntityManager());
            Cliente c=this.crearClienteDesdeCampos();
            clijpa.registrarCliente(c);
            this.lstClientes.add(c);
            this.tblClientes.getSelectionModel().select(c);
        }
    }
    
    private void modificarCliente(){
        if(this.validacionCampos()){
            ClienteJPAController clijpa=new ClienteJPAController(ConexionBD.EMF.createEntityManager());
            int posicionCliente=-1;
            for(Cliente cli:this.lstClientes){
                if(cli.getIdCliente().equals(Integer.parseInt(this.txfCodigoCliente.getText()))){
                    posicionCliente=this.lstClientes.indexOf(cli);
                }
            }
            Cliente c=this.crearClienteDesdeCampos();
            c.setIdCliente(Integer.parseInt(this.txfCodigoCliente.getText()));
            clijpa.modificarCliente(c);
            this.lstClientes.remove(posicionCliente);
            this.lstClientes.add(posicionCliente, c);
            this.tblClientes.getSelectionModel().select(c);
        }
    }
    
    private void eliminarCliente(){
        if(this.tblClientes.getSelectionModel().isEmpty()){
            Alert dlg=new Alert(AlertType.WARNING);
            dlg.setTitle("Eliminar Clientes");
            dlg.setContentText("Debe seleccionar por lo menos un Cliente");
            dlg.showAndWait();
        }else{
            Alert conf=new Alert(AlertType.CONFIRMATION);
            conf.setTitle("Eliminar Clientes");
            conf.setContentText("Se eliminarán los Clientes seleccionados.\n¿Desea continuar?");
            Optional<ButtonType> eleccion=conf.showAndWait();
            if(eleccion.get().equals(ButtonType.OK)){
                List<Cliente> lstCliEliminar=this.tblClientes.getSelectionModel().getSelectedItems();
                ClienteJPAController clijpa=new ClienteJPAController(ConexionBD.EMF.createEntityManager());
                for(Cliente cl : lstCliEliminar){
                    if(!clijpa.tieneRegistroCupones(cl) && !clijpa.tieneRegistrosFactura(cl)){
                        clijpa.eliminarCliente(cl);
                        this.lstClientes.remove(cl);
                    }else{
                        Alert dlg=new Alert(AlertType.WARNING);
                        dlg.setTitle("Eliminar Cliente");
                        dlg.setContentText("No se puede eliminar el cliente '"+cl.getIdCliente()+" "+cl.getNombres()+" "+cl.getApellidos()+"'. \nExisten registros de facturas o cupones.");
                        dlg.showAndWait();
                    }
                    
                }
            }
        }
    }
    
    private Cliente crearClienteDesdeCampos(){
        Cliente c=new Cliente();
        c.setNombres(this.txfNombresCliente.getText());
        c.setApellidos(this.txfApellidosCliente.getText());
        c.setCi(Integer.parseInt(this.txfCiCliente.getText()));
        c.setCiudad(this.cmbCiudadCliente.getSelectionModel().getSelectedItem());
        c.setCelular(this.txfCelularCliente.getText());
        c.setTelefono(this.txfTelefonoCliente.getText());
        c.setDireccion(this.txfDireccionCliente.getText());
        c.setEmail(this.txfEmailCliente.getText());
        return c;
    }
    
    private boolean validacionCampos(){
        boolean datosValidos=false;
        Alert dlg=new Alert(AlertType.WARNING);
        dlg.setTitle("Registrar Cliente");
        try{
            Integer ci=Integer.parseInt(this.txfCiCliente.getText());
            if(!this.txfNombresCliente.getText().isEmpty()){
                if(!this.txfApellidosCliente.getText().isEmpty()){
                    if(!this.cmbCiudadCliente.getSelectionModel().isEmpty()){
                        datosValidos=true;
                    }else{
                        dlg.setContentText("Ninguna ciudad seleccionada");
                        dlg.showAndWait();
                    }
                }else{
                    dlg.setContentText("Campo 'Apellidos' en blanco.");
                    dlg.showAndWait();
                }
            }else{
                dlg.setContentText("Campo 'Nombres' en blanco");
                dlg.showAndWait();
            }
        }catch(NumberFormatException ex){
            System.out.println("Error al convertir CI de String a Integer: "+ex.getMessage()); 
        }
        
        return datosValidos;
    }
    
}
