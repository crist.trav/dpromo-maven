/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exabit.dpromo.ajustes;

import com.exabit.dpromo.bd.controllers.ParametrosJPAController;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.print.Printer;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.RadioButton;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

/**
 * FXML Controller class
 *
 * @author cristhian-kn
 */
public class AjustesController implements Initializable {

    private static final Logger LOG = Logger.getLogger(AjustesController.class.getName());

    @FXML
    private ComboBox<Printer> cmbImpresoras;
    @FXML
    private Button btnGuardar;

    /**
     * Initializes the controller class.
     *
     * @param url
     * @param rb
     */
    private final ObservableList<Printer> lstImpresoras = FXCollections.observableArrayList();
    @FXML
    private Slider sldEscala;

    private final ChangeListener<Number> lnrValorEscala = (ObservableValue<? extends Number> obs, Number oldValue, Number newValue) -> {
        this.lblEscalaTicket.setText(newValue.doubleValue() + "%");
    };
    @FXML
    private Label lblEscalaTicket;
    @FXML
    private RadioButton rdbBDLocal;
    @FXML
    private ToggleGroup tgbGrpUbicacionBD;
    @FXML
    private RadioButton rdbBDMySQL;
    @FXML
    private TextField txfHostMysql;
    @FXML
    private TextField txfPuertoMysql;
    @FXML
    private TextField txfUserMysql;
    @FXML
    private TextField txfBdMysql;
    @FXML
    private PasswordField pwfPassMysql;
    @FXML
    private ComboBox<String> cmbTamaioLetraCupon;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        for (Integer i = 10; i <= 50; i++){
            this.cmbTamaioLetraCupon.getItems().add(String.valueOf(i));
        }
        this.sldEscala.valueProperty().addListener(this.lnrValorEscala);
        this.btnGuardar.setGraphic(new ImageView(new Image(getClass().getResourceAsStream("/iconos/x16/save.png"))));
        this.cmbImpresoras.setItems(lstImpresoras);
        lstImpresoras.addAll(Printer.getAllPrinters());
        ParametrosJPAController pjpa = new ParametrosJPAController();
        String impre = pjpa.getParametroSistema(ParametrosJPAController.IMPRESORA_TICKETS);
        if (impre != null) {
            for (Printer i : this.lstImpresoras) {
                if (i.getName().equals(impre)) {
                    this.cmbImpresoras.getSelectionModel().select(i);
                    break;
                }
            }
        }
        String escala = pjpa.getParametroSistema(ParametrosJPAController.ESCALA_TICKET);
        if (escala != null) {
            this.sldEscala.setValue(Double.parseDouble(escala) * 100);
        } else {
            this.sldEscala.setValue(65);
        }

        String tipoBd = pjpa.getParametroSistema(ParametrosJPAController.TIPO_BD);
        if (tipoBd != null) {
            if (tipoBd.equals(ParametrosJPAController.TIPO_BD_MYSQL)) {
                this.rdbBDMySQL.setSelected(true);
                this.txfBdMysql.setDisable(false);
                this.txfHostMysql.setDisable(false);
                this.txfPuertoMysql.setDisable(false);
                this.txfUserMysql.setDisable(false);
                this.pwfPassMysql.setDisable(false);
            } else {
                this.rdbBDLocal.setSelected(true);
            }
        }

        String mysqlhost = pjpa.getParametroSistema(ParametrosJPAController.MYSQL_HOST);
        if (mysqlhost != null) {
            this.txfHostMysql.setText(mysqlhost);
        }
        String mysqlport = pjpa.getParametroSistema(ParametrosJPAController.MYSQL_PORT);
        if (mysqlport != null) {
            this.txfPuertoMysql.setText(mysqlport);
        }
        String mysqluser = pjpa.getParametroSistema(ParametrosJPAController.MYSQL_USER);
        if (mysqluser != null) {
            this.txfUserMysql.setText(mysqluser);
        }
        String mysqlpass = pjpa.getParametroSistema(ParametrosJPAController.MYSQL_PASS);
        if (mysqlpass != null) {
            this.pwfPassMysql.setText(mysqlpass);
        }
        String mysqlbd = pjpa.getParametroSistema(ParametrosJPAController.MYSQL_DB);
        if (mysqlbd != null) {
            this.txfBdMysql.setText(mysqlbd);
        }
        
        String tamanioLetraCupon = pjpa.getParametroSistema(ParametrosJPAController.TAMANIO_LETRA_CUPON);
        if(tamanioLetraCupon != null){
            this.cmbTamaioLetraCupon.getSelectionModel().select(tamanioLetraCupon);
        }else{
            this.cmbTamaioLetraCupon.getSelectionModel().select("13");
        }

        this.sldEscala.setBlockIncrement(1.0);
        
    }

    @FXML
    private void onActionBtnGuardar(ActionEvent event) {
        try {
            ParametrosJPAController pjpa = new ParametrosJPAController();
            pjpa.setParametroSistema(ParametrosJPAController.IMPRESORA_TICKETS, this.cmbImpresoras.getSelectionModel().getSelectedItem().getName());

            pjpa.setParametroSistema(ParametrosJPAController.ESCALA_TICKET, (this.sldEscala.getValue() / 100) + "");
            pjpa.setParametroSistema(ParametrosJPAController.TAMANIO_LETRA_CUPON, this.cmbTamaioLetraCupon.getSelectionModel().getSelectedItem());
            
            if (this.rdbBDLocal.isSelected()) {
                pjpa.setParametroSistema(ParametrosJPAController.TIPO_BD, ParametrosJPAController.TIPO_BD_H2);
            } else {
                pjpa.setParametroSistema(ParametrosJPAController.TIPO_BD, ParametrosJPAController.TIPO_BD_MYSQL);
                pjpa.setParametroSistema(ParametrosJPAController.MYSQL_HOST, this.txfHostMysql.getText());
                pjpa.setParametroSistema(ParametrosJPAController.MYSQL_PORT, this.txfPuertoMysql.getText());
                pjpa.setParametroSistema(ParametrosJPAController.MYSQL_USER, this.txfUserMysql.getText());
                pjpa.setParametroSistema(ParametrosJPAController.MYSQL_PASS, this.pwfPassMysql.getText());
                pjpa.setParametroSistema(ParametrosJPAController.MYSQL_DB, this.txfBdMysql.getText());
            }

            Alert dlg = new Alert(AlertType.INFORMATION);
            dlg.setTitle("Guardar Ajustes");
            dlg.setContentText("Exito");
            dlg.show();
        } catch (Exception ex) {
            Alert dlg = new Alert(AlertType.ERROR);
            dlg.setTitle("Guardar Ajustes");
            dlg.setContentText(ex.getMessage());
            dlg.show();
            LOG.log(Level.SEVERE, "Error al guardar ajustes", ex);
        }
    }

    @FXML
    private void onActionRdbLocal(ActionEvent event) {
        this.txfBdMysql.setDisable(true);
        this.txfHostMysql.setDisable(true);
        this.txfPuertoMysql.setDisable(true);
        this.txfUserMysql.setDisable(true);
        this.pwfPassMysql.setDisable(true);
    }

    @FXML
    private void onActionRdbMysql(ActionEvent event) {
        this.txfBdMysql.setDisable(false);
        this.txfHostMysql.setDisable(false);
        this.txfPuertoMysql.setDisable(false);
        this.txfUserMysql.setDisable(false);
        this.pwfPassMysql.setDisable(false);
    }

}
