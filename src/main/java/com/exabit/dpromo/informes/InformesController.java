package com.exabit.dpromo.informes;

import com.exabit.dpromo.MainApp;
import com.exabit.dpromo.PantallaPrincipalController;
import com.exabit.dpromo.bd.ConexionBD;
import com.exabit.dpromo.bd.controllers.PromocionJPAController;
import com.exabit.dpromo.bd.entidades.Cliente;
import com.exabit.dpromo.bd.entidades.Promocion;
import com.exabit.dpromo.bd.entidades.Tienda;
import com.exabit.dpromo.informes.beans.ClienteCuponesMontoBean;
import com.exabit.dpromo.informes.beans.ClienteTiendaBean;
import com.exabit.dpromo.informes.beans.TiendaMontoBean;
import java.io.File;
import java.io.FileOutputStream;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.concurrent.Worker;
import javafx.concurrent.WorkerStateEvent;
import javafx.embed.swing.SwingNode;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Cursor;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.ListView;
import javafx.scene.control.RadioButton;
import javafx.scene.control.SplitPane;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.stage.DirectoryChooser;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.swing.JRViewer;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 * FXML Controller class
 *
 * @author cristhian
 */
public class InformesController implements Initializable {

    private static final Logger LOG = Logger.getLogger(InformesController.class.getName());

    @FXML
    private SplitPane splpaneInforme;
    @FXML
    private AnchorPane apaneIzqInforme;
    @FXML
    private Button btnGenerarInforme;

    SwingNode swNode = new SwingNode();
    @FXML
    private ComboBox<Promocion> cmbPromocionPorCliente;
    @FXML
    private VBox vboxItemsPorCliente;
    @FXML
    private ToggleGroup tggRango;
    @FXML
    private DatePicker dpkDesde;
    @FXML
    private ComboBox<Integer> cmbHoraDesde;
    @FXML
    private ComboBox<Integer> cmbMinutoDesde;
    @FXML
    private DatePicker dpkHasta;
    @FXML
    private ComboBox<Integer> cmbHoraHasta;
    @FXML
    private ComboBox<Integer> cmbMinutoHasta;
    @FXML
    private RadioButton rdbTodosCupones;
    @FXML
    private RadioButton rdbRancoFechaCupones;
    @FXML
    private Button btnExportarXLS;
    @FXML
    private RadioButton rdbTipoPorCliente;
    @FXML
    private ToggleGroup tggTipoReporte;
    @FXML
    private RadioButton rdbTipoPorClienteTienda;

    private final ObservableList<Promocion> lstPromociones = FXCollections.observableArrayList();
    private final ObservableList<Integer> lstHoras = FXCollections.observableArrayList();
    private final ObservableList<Integer> lstMinutos = FXCollections.observableArrayList();

    ObservableList<ClienteCuponesMontoBean> lstDatosXLS = FXCollections.observableArrayList();
    ObservableList<ClienteTiendaBean> lstDatosXLSTienda = FXCollections.observableArrayList();

    Map<Integer, String> mapaLetraColumna = new HashMap<>();

    File archSelec = null;

    private final Service<Void> srvGenerarInformeCFCliente = new Service<Void>() {
        @Override
        protected Task<Void> createTask() {
            return new Task<Void>() {
                @Override
                protected Void call() throws Exception {
                    Map<String, Object> parametros = new HashMap<>();
                    JasperReport report = null;
                    JRBeanCollectionDataSource dataSource = null;
                    Promocion p = cmbPromocionPorCliente.getSelectionModel().getSelectedItem();
                    lstDatosXLS.clear();
                    lstDatosXLSTienda.clear();
                    try {
                        if (rdbTipoPorCliente.isSelected()) {
                            report = (JasperReport) JRLoader.loadObject(getClass().getResourceAsStream("/jasper/informe-cupones-monto.jasper"));
                            PromocionJPAController prjpa = new PromocionJPAController(ConexionBD.EMF.createEntityManager());
                            InformesDataFactory.getClienteCuponesMonto(prjpa.getPromocionActiva());

                            if (rdbTodosCupones.isSelected()) {
                                lstDatosXLS.addAll(InformesDataFactory.getClienteCuponesMonto(cmbPromocionPorCliente.getSelectionModel().getSelectedItem()));
                                dataSource = new JRBeanCollectionDataSource(lstDatosXLS);
                            } else {
                                SimpleDateFormat df = new SimpleDateFormat("dd/MM/yy HH:mm");
                                Date ddesde = new Date(getFechaDesde().getTimeInMillis());
                                Date dhasta = new Date(getFechaHasta().getTimeInMillis());
                                String rango = "Desde: " + df.format(ddesde) + " | Hasta: " + df.format(dhasta);
                                parametros.put("rangoFechaHora", rango);
                                lstDatosXLS.addAll(InformesDataFactory.getClienteCuponesMontoRango(p, getFechaDesde(), getFechaHasta()));
                                dataSource = new JRBeanCollectionDataSource(lstDatosXLS);
                            }

                        } else if (rdbTipoPorClienteTienda.isSelected()) {
                            //lstDatosXLS.clear();
                            //lstDatosXLSTienda.clear();
                            report = (JasperReport) JRLoader.loadObject(getClass().getResourceAsStream("/jasper/ComprasPorTiendaCliente.jasper"));
                            JasperReport subreport = (JasperReport) JRLoader.loadObject(getClass().getResourceAsStream("/jasper/CompraPorTienda-Sub.jasper"));
                            parametros.put("subReporte", subreport);
                            if (rdbTodosCupones.isSelected()) {
                                lstDatosXLSTienda.addAll(InformesDataFactory.getClienteTiendas(p));
                                dataSource = new JRBeanCollectionDataSource(lstDatosXLSTienda);
                            } else {
                                SimpleDateFormat df = new SimpleDateFormat("dd/MM/yy HH:mm");
                                Date ddesde = new Date(getFechaDesde().getTimeInMillis());
                                Date dhasta = new Date(getFechaHasta().getTimeInMillis());
                                String rango = "Desde: " + df.format(ddesde) + " | Hasta: " + df.format(dhasta);
                                parametros.put("rango", rango);
                                lstDatosXLSTienda.addAll(InformesDataFactory.getClienteTiendasRango(p, getFechaDesde(), getFechaHasta()));
                                dataSource = new JRBeanCollectionDataSource(lstDatosXLSTienda);
                            }
                        }
                        JasperPrint jprint = JasperFillManager.fillReport(report, parametros, dataSource);
                        JRViewer jv = new JRViewer(jprint);
                        swNode.setContent(jv);
                    } catch (JRException ex) {
                        Logger.getLogger(InformesController.class.getName()).log(Level.SEVERE, "Error al cargar reporte", ex);
                    }
                    return null;
                }
            };
        }

    };

    private final Service<Void> srvExportarXLS = new Service<Void>() {
        @Override
        protected Task<Void> createTask() {
            return new Task<Void>() {
                @Override
                protected Void call() throws Exception {
                    if (!lstDatosXLS.isEmpty()) {
                        exportarXlsTotal();
                    } else if (!lstDatosXLSTienda.isEmpty()) {
                        exportarXlsPorTienda();
                    }
                    return null;
                }
            };
        }

    };

    private final EventHandler<WorkerStateEvent> evtHndSrvGenerarInformeCFCliente = (WorkerStateEvent evt) -> {
        if (evt.getEventType().equals(WorkerStateEvent.WORKER_STATE_RUNNING)) {
            MainApp.getStage().getScene().setCursor(Cursor.WAIT);
            this.btnGenerarInforme.setDisable(true);
            this.verificarActivacionBoton();
            MainApp.mostrarMensaje("Generando Informe", PantallaPrincipalController.TIPO_MENSAJE_INFO);
            MainApp.mostrarProgress(true);
        }

        if (evt.getEventType().equals(WorkerStateEvent.WORKER_STATE_SUCCEEDED)) {
            MainApp.getStage().getScene().setCursor(Cursor.DEFAULT);
            this.btnGenerarInforme.setDisable(false);
            this.verificarActivacionBoton();
            MainApp.mostrarMensaje("Informe Generado", PantallaPrincipalController.TIPO_MENSAJE_INFO);
            MainApp.mostrarProgress(false);
        }

        if (evt.getEventType().equals(WorkerStateEvent.WORKER_STATE_FAILED)) {
            MainApp.getStage().getScene().setCursor(Cursor.DEFAULT);
            this.btnGenerarInforme.setDisable(false);
            this.verificarActivacionBoton();
            MainApp.mostrarMensaje("Error al generar informe: " + evt.getSource().getException().getMessage(), PantallaPrincipalController.TIPO_MENSAJE_ERROR);
            MainApp.mostrarProgress(false);
            LOG.log(Level.WARNING, "Error al generar informe", evt.getSource().getException());
        }
    };

    private final EventHandler<WorkerStateEvent> evtHndExportarXLS = (WorkerStateEvent evt) -> {
        if (evt.getEventType().equals(WorkerStateEvent.WORKER_STATE_RUNNING)) {
            MainApp.getStage().getScene().setCursor(Cursor.WAIT);
            btnExportarXLS.setDisable(true);
            btnGenerarInforme.setDisable(true);
            MainApp.mostrarMensaje("Exportando a archivo XLS", PantallaPrincipalController.TIPO_MENSAJE_INFO);
            MainApp.mostrarProgress(true);
        }
        if (evt.getEventType().equals(WorkerStateEvent.WORKER_STATE_SUCCEEDED)) {
            MainApp.getStage().getScene().setCursor(Cursor.DEFAULT);
            this.verificarActivacionBoton();
            btnGenerarInforme.setDisable(false);
            MainApp.mostrarMensaje("Archivo exportado exitosamente", PantallaPrincipalController.TIPO_MENSAJE_INFO);
            MainApp.mostrarProgress(false);
        }
        if (evt.getEventType().equals(WorkerStateEvent.WORKER_STATE_FAILED)) {
            MainApp.getStage().getScene().setCursor(Cursor.DEFAULT);
            this.verificarActivacionBoton();
            btnGenerarInforme.setDisable(false);
            MainApp.mostrarMensaje("Error al exportar XLS: " + evt.getSource().getException().getMessage(), PantallaPrincipalController.TIPO_MENSAJE_ERROR);
            MainApp.mostrarProgress(false);
            LOG.log(Level.SEVERE, "Error al exportar xls", evt.getSource().getException());
        }

    };

    private final ChangeListener<Boolean> chLnrOpcionRango = (ObservableValue<? extends Boolean> obs, Boolean oldValue, Boolean newValue) -> {
        this.dpkDesde.setDisable(!newValue);
        this.dpkHasta.setDisable(!newValue);
        this.cmbHoraDesde.setDisable(!newValue);
        this.cmbHoraHasta.setDisable(!newValue);
        this.cmbMinutoDesde.setDisable(!newValue);
        this.cmbMinutoHasta.setDisable(!newValue);
    };

    private final ListChangeListener<ClienteCuponesMontoBean> lstChLnrDatosXLS = (ListChangeListener.Change<? extends ClienteCuponesMontoBean> c) -> {
        c.next();
        Platform.runLater(() -> {
            if (!srvExportarXLS.isRunning() && !this.srvGenerarInformeCFCliente.isRunning()) {
                verificarActivacionBoton();
            }
        });
    };
    private final ListChangeListener<ClienteTiendaBean> lstChLnrDatosXLSTienda = (ListChangeListener.Change<? extends ClienteTiendaBean> c) -> {
        c.next();
        Platform.runLater(() -> {
            if (!srvExportarXLS.isRunning() && !this.srvGenerarInformeCFCliente.isRunning()) {
                verificarActivacionBoton();
            }
        });
    };

    private void verificarActivacionBoton() {
        //System.out.println("Se verifica listas xls");
        if (this.lstDatosXLS.isEmpty()) {
          //  System.out.println("Ddatos xls vacio");
            if (this.lstDatosXLSTienda.isEmpty()) {
            //    System.out.println("datos xls tienda vacio");
                btnExportarXLS.setDisable(true);
            } else {
              //  System.out.println("datos xls tienda NO vacio");
                btnExportarXLS.setDisable(false);
            }
        } else {
           // System.out.println("Ddatos xls NO vacio");
            btnExportarXLS.setDisable(false);
            /*if(this.lstDatosXLSTienda.isEmpty()){
                System.out.println("datos xls tienda vacio");
                btnExportarXLS.setDisable(true);
            }else{
                System.out.println("datos xls tienda NO vacio");
                
            }*/
        }
    }

    /**
     * Initializes the controller class.
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        this.inicializarMapaLetrasColumna();
        for (int i = 0; i < 24; i++) {
            this.lstHoras.add(i);
        }
        for (int i = 0; i < 60; i = i + 5) {
            this.lstMinutos.add(i);
        }
        this.cmbHoraDesde.setItems(this.lstHoras);
        this.cmbHoraHasta.setItems(this.lstHoras);
        this.cmbMinutoDesde.setItems(this.lstMinutos);
        this.cmbMinutoHasta.setItems(this.lstMinutos);

        this.dpkDesde.setValue(LocalDate.now());
        this.dpkHasta.setValue(LocalDate.now());
        this.cmbHoraDesde.getSelectionModel().selectFirst();
        this.cmbMinutoDesde.getSelectionModel().selectFirst();
        this.cmbHoraHasta.getSelectionModel().selectLast();
        this.cmbMinutoHasta.getSelectionModel().selectLast();

        this.cargarIconos();
        this.inicializarComboPromocionesPorCliente();
        this.asignarEventHandler();
        this.cmbPromocionPorCliente.prefWidthProperty().bind(this.vboxItemsPorCliente.widthProperty());
        AnchorPane.setTopAnchor(swNode, 0.0);
        AnchorPane.setBottomAnchor(swNode, 0.0);
        AnchorPane.setLeftAnchor(swNode, 0.0);
        AnchorPane.setRightAnchor(swNode, 0.0);
        this.apaneIzqInforme.getChildren().add(swNode);
        PromocionJPAController prjpa = new PromocionJPAController(ConexionBD.EMF.createEntityManager());
        this.lstPromociones.addAll(prjpa.getTodasPromociones());
        if (!this.lstPromociones.isEmpty()) {
            boolean hayActivo = false;
            for (Promocion pro : lstPromociones) {
                if (pro.isActivo()) {
                    this.cmbPromocionPorCliente.getSelectionModel().select(pro);
                    hayActivo = true;
                    break;
                }
            }
            if (!hayActivo) {
                this.cmbPromocionPorCliente.getSelectionModel().select(lstPromociones.get(0));
            }
            //this.cmbPromocionPorCliente.getSelectionModel().select(this.lstPromociones.get(0));
        }

        this.dpkDesde.prefWidthProperty().bind(this.vboxItemsPorCliente.widthProperty());
        this.dpkHasta.prefWidthProperty().bind(this.vboxItemsPorCliente.widthProperty());
        this.rdbRancoFechaCupones.selectedProperty().addListener(this.chLnrOpcionRango);
        this.lstDatosXLS.addListener(this.lstChLnrDatosXLS);
        this.lstDatosXLSTienda.addListener(this.lstChLnrDatosXLSTienda);
    }

    private void inicializarMapaLetrasColumna() {
        this.mapaLetraColumna.put(0, "A");
        this.mapaLetraColumna.put(1, "B");
        this.mapaLetraColumna.put(2, "C");
        this.mapaLetraColumna.put(3, "D");
        this.mapaLetraColumna.put(4, "E");
        this.mapaLetraColumna.put(5, "F");
        this.mapaLetraColumna.put(6, "G");
        this.mapaLetraColumna.put(7, "H");
        this.mapaLetraColumna.put(8, "I");
        this.mapaLetraColumna.put(9, "J");
        this.mapaLetraColumna.put(10, "K");
        this.mapaLetraColumna.put(11, "L");
        this.mapaLetraColumna.put(12, "M");
        this.mapaLetraColumna.put(13, "N");
        this.mapaLetraColumna.put(14, "O");
        this.mapaLetraColumna.put(15, "P");
        this.mapaLetraColumna.put(16, "Q");
        this.mapaLetraColumna.put(17, "R");
        this.mapaLetraColumna.put(18, "S");
        this.mapaLetraColumna.put(19, "T");
        this.mapaLetraColumna.put(20, "U");
        this.mapaLetraColumna.put(21, "V");
        this.mapaLetraColumna.put(22, "W");
        this.mapaLetraColumna.put(23, "X");
        this.mapaLetraColumna.put(24, "Y");
        this.mapaLetraColumna.put(25, "Z");
        this.mapaLetraColumna.put(26, "AA");
        this.mapaLetraColumna.put(27, "AB");
        this.mapaLetraColumna.put(28, "AC");
        this.mapaLetraColumna.put(29, "AD");
        this.mapaLetraColumna.put(30, "AE");
        this.mapaLetraColumna.put(31, "AF");
        this.mapaLetraColumna.put(32, "AG");
        this.mapaLetraColumna.put(33, "AH");
        this.mapaLetraColumna.put(34, "AI");
        this.mapaLetraColumna.put(35, "AJ");
        this.mapaLetraColumna.put(36, "AK");
        this.mapaLetraColumna.put(37, "AL");
        this.mapaLetraColumna.put(38, "AM");
        this.mapaLetraColumna.put(39, "AN");
        this.mapaLetraColumna.put(40, "AO");
        this.mapaLetraColumna.put(41, "AP");
        this.mapaLetraColumna.put(42, "AQ");
        this.mapaLetraColumna.put(43, "AR");
        this.mapaLetraColumna.put(44, "AS");
        this.mapaLetraColumna.put(45, "AT");
        this.mapaLetraColumna.put(46, "AU");
        this.mapaLetraColumna.put(47, "AV");
        this.mapaLetraColumna.put(48, "AW");
        this.mapaLetraColumna.put(49, "AX");
        this.mapaLetraColumna.put(50, "AY");
        this.mapaLetraColumna.put(51, "AZ");
        this.mapaLetraColumna.put(52, "BA");
        this.mapaLetraColumna.put(53, "BB");
        this.mapaLetraColumna.put(54, "BC");
        this.mapaLetraColumna.put(55, "BD");
        this.mapaLetraColumna.put(56, "BE");
        this.mapaLetraColumna.put(57, "BF");
        this.mapaLetraColumna.put(58, "BG");
        this.mapaLetraColumna.put(59, "BH");
        this.mapaLetraColumna.put(60, "BI");
        this.mapaLetraColumna.put(61, "BJ");
        this.mapaLetraColumna.put(62, "BK");
        this.mapaLetraColumna.put(63, "BL");
        this.mapaLetraColumna.put(64, "BM");
        this.mapaLetraColumna.put(65, "BN");
        this.mapaLetraColumna.put(66, "BO");
        this.mapaLetraColumna.put(67, "BP");
        this.mapaLetraColumna.put(68, "BQ");
        this.mapaLetraColumna.put(69, "BR");
        this.mapaLetraColumna.put(70, "BS");
        this.mapaLetraColumna.put(71, "BT");
        this.mapaLetraColumna.put(72, "BU");
        this.mapaLetraColumna.put(73, "BV");
        this.mapaLetraColumna.put(74, "BW");
        this.mapaLetraColumna.put(75, "BX");
        this.mapaLetraColumna.put(76, "BY");
        this.mapaLetraColumna.put(77, "BZ");
    }

    private Calendar getFechaDesde() {
        Calendar fdesde = new GregorianCalendar();
        LocalDate ldDesde = dpkDesde.getValue();
        fdesde.set(Calendar.YEAR, ldDesde.getYear());
        fdesde.set(Calendar.MONTH, (ldDesde.getMonthValue() - 1));
        fdesde.set(Calendar.DAY_OF_MONTH, ldDesde.getDayOfMonth());
        fdesde.set(Calendar.HOUR_OF_DAY, cmbHoraDesde.getSelectionModel().getSelectedItem());
        fdesde.set(Calendar.MINUTE, cmbMinutoDesde.getSelectionModel().getSelectedItem());
        fdesde.set(Calendar.SECOND, 0);
        return fdesde;
    }

    private Calendar getFechaHasta() {
        Calendar fhasta = new GregorianCalendar();
        LocalDate ldHasta = dpkHasta.getValue();
        fhasta.set(Calendar.YEAR, ldHasta.getYear());
        fhasta.set(Calendar.MONTH, (ldHasta.getMonthValue() - 1));
        fhasta.set(Calendar.DAY_OF_MONTH, ldHasta.getDayOfMonth());
        fhasta.set(Calendar.HOUR_OF_DAY, cmbHoraHasta.getSelectionModel().getSelectedItem());
        fhasta.set(Calendar.MINUTE, cmbMinutoHasta.getSelectionModel().getSelectedItem());
        fhasta.set(Calendar.SECOND, 0);
        return fhasta;
    }

    private void asignarEventHandler() {
        this.srvGenerarInformeCFCliente.setOnRunning(evtHndSrvGenerarInformeCFCliente);
        this.srvGenerarInformeCFCliente.setOnSucceeded(evtHndSrvGenerarInformeCFCliente);
        this.srvGenerarInformeCFCliente.setOnFailed(evtHndSrvGenerarInformeCFCliente);

        this.srvExportarXLS.setOnRunning(this.evtHndExportarXLS);
        this.srvExportarXLS.setOnSucceeded(this.evtHndExportarXLS);
        this.srvExportarXLS.setOnFailed(this.evtHndExportarXLS);
    }

    private void cargarIconos() {
        Image imgReporte = new Image(getClass().getResourceAsStream("/iconos/x16/report.png"));
        this.btnGenerarInforme.setGraphic(new ImageView(imgReporte));
        Image imgXls = new Image(getClass().getResourceAsStream("/iconos/x16/excel.png"));
        this.btnExportarXLS.setGraphic(new ImageView(imgXls));
    }

    private void inicializarComboPromocionesPorCliente() {
        this.cmbPromocionPorCliente.setItems(this.lstPromociones);
        this.cmbPromocionPorCliente.setCellFactory((ListView<Promocion> lc) -> new PromocionListCell());
        this.cmbPromocionPorCliente.setButtonCell(new PromocionListCell());
    }

    private void generarInforme() {
        if (this.cmbPromocionPorCliente.getSelectionModel().isEmpty()) {
            Alert al = new Alert(AlertType.WARNING);
            al.setTitle("Generar Informe");
            al.setContentText("Debe seleccionar una Promoción");
            al.showAndWait();
        } else if (!this.srvGenerarInformeCFCliente.isRunning()) {
            if (this.srvGenerarInformeCFCliente.getState().equals(Worker.State.SUCCEEDED) || this.srvGenerarInformeCFCliente.getState().equals(Worker.State.FAILED) || this.srvGenerarInformeCFCliente.getState().equals(Worker.State.CANCELLED)) {
                this.srvGenerarInformeCFCliente.reset();
            }
            this.srvGenerarInformeCFCliente.start();
        }
    }

    @FXML
    private void onActionBtnGenerarInforme(ActionEvent event) {
        this.generarInforme();

    }

    @FXML
    private void onActionRdbRango(ActionEvent event) {

    }

    @FXML
    private void onActionExportarXLS(ActionEvent event) {
        /*if(this.rdbTipoPorCliente.isSelected()){
            this.exportarXlsTotal();
        }else if(this.rdbTipoPorClienteTienda.isSelected()){
            this.exportarXlsPorTienda();
        }*/
        this.btnExportarXLS.setDisable(true);
        DirectoryChooser dirChoose = new DirectoryChooser();
        if (!this.lstDatosXLS.isEmpty()) {
            archSelec = dirChoose.showDialog(MainApp.getStage());
        } else if (!this.lstDatosXLSTienda.isEmpty()) {
            archSelec = dirChoose.showDialog(MainApp.getStage());
        } else {
            Alert al = new Alert(AlertType.WARNING);
            al.setTitle("Exportar datos");
            al.setContentText("No se ha generado ningun informe");
            al.show();
        }
        if (this.archSelec != null) {
            //this.exportarXlsPorTienda();
            if (!this.srvExportarXLS.isRunning()) {
                if (this.srvExportarXLS.getState().equals(Worker.State.SUCCEEDED) || this.srvExportarXLS.getState().equals(Worker.State.FAILED) || this.srvExportarXLS.getState().equals(Worker.State.CANCELLED)) {
                    this.srvExportarXLS.reset();
                }
                this.srvExportarXLS.start();
            }
        } else {
            this.verificarActivacionBoton();
        }
    }

    private void exportarXlsTotal() {
        if (archSelec != null) {
            lstDatosXLS.clear();
            String rango = "";
            if (rdbTodosCupones.isSelected()) {
                lstDatosXLS.addAll(InformesDataFactory.getClienteCuponesMonto(cmbPromocionPorCliente.getSelectionModel().getSelectedItem()));
            } else {
                Calendar fdesde = new GregorianCalendar();
                Calendar fhasta = new GregorianCalendar();

                LocalDate ldDesde = dpkDesde.getValue();
                fdesde.set(Calendar.YEAR, ldDesde.getYear());
                fdesde.set(Calendar.MONTH, (ldDesde.getMonthValue() - 1));
                fdesde.set(Calendar.DAY_OF_MONTH, ldDesde.getDayOfMonth());
                fdesde.set(Calendar.HOUR_OF_DAY, cmbHoraDesde.getSelectionModel().getSelectedItem());
                fdesde.set(Calendar.MINUTE, cmbMinutoDesde.getSelectionModel().getSelectedItem());
                fdesde.set(Calendar.SECOND, 0);

                LocalDate ldHasta = dpkHasta.getValue();
                fhasta.set(Calendar.YEAR, ldHasta.getYear());
                fhasta.set(Calendar.MONTH, (ldHasta.getMonthValue() - 1));
                fhasta.set(Calendar.DAY_OF_MONTH, ldHasta.getDayOfMonth());
                fhasta.set(Calendar.HOUR_OF_DAY, cmbHoraHasta.getSelectionModel().getSelectedItem());
                fhasta.set(Calendar.MINUTE, cmbMinutoHasta.getSelectionModel().getSelectedItem());
                fhasta.set(Calendar.SECOND, 0);

                SimpleDateFormat df = new SimpleDateFormat("dd/MM/yy HH:mm");
                Date ddesde = new Date(fdesde.getTimeInMillis());
                Date dhasta = new Date(fhasta.getTimeInMillis());
                rango = "Desde: " + df.format(ddesde) + " | Hasta: " + df.format(dhasta);
                lstDatosXLS.addAll(InformesDataFactory.getClienteCuponesMontoRango(cmbPromocionPorCliente.getSelectionModel().getSelectedItem(), fdesde, fhasta));
            }

            XSSFWorkbook libro = new XSSFWorkbook();
            XSSFSheet hoja = libro.createSheet("Cupones");
            XSSFFont fuente = libro.createFont();
            fuente.setFontHeightInPoints((short) 10);

            XSSFCellStyle estiloCabecera = libro.createCellStyle();
            fuente.setBold(true);
            estiloCabecera.setFont(fuente);
            estiloCabecera.setBorderBottom(BorderStyle.THIN);
            estiloCabecera.setBorderTop(BorderStyle.THIN);
            estiloCabecera.setBorderLeft(BorderStyle.THIN);
            estiloCabecera.setBorderRight(BorderStyle.THIN);

            XSSFColor color = new XSSFColor(java.awt.Color.GRAY);
            estiloCabecera.setFillBackgroundColor(color);

            Row fCabecera = hoja.createRow(1);
            Cell ctApel = fCabecera.createCell(0);
            ctApel.setCellValue("Apellidos");
            ctApel.setCellStyle(estiloCabecera);

            Cell ctNomb = fCabecera.createCell(1);
            ctNomb.setCellValue("Nombres");
            ctNomb.setCellStyle(estiloCabecera);

            Cell ctCi = fCabecera.createCell(2);
            ctCi.setCellValue("Documento");
            ctCi.setCellStyle(estiloCabecera);

            Cell ctCel = fCabecera.createCell(3);
            ctCel.setCellValue("Celular");
            ctCel.setCellStyle(estiloCabecera);

            Cell ctTel = fCabecera.createCell(4);
            ctTel.setCellValue("Teléfono");
            ctTel.setCellStyle(estiloCabecera);

            Cell ctEmail = fCabecera.createCell(5);
            ctEmail.setCellValue("Email");
            ctEmail.setCellStyle(estiloCabecera);

            Cell ctDir = fCabecera.createCell(6);
            ctDir.setCellValue("Dirección");
            ctDir.setCellStyle(estiloCabecera);

            Cell ctCant = fCabecera.createCell(7);
            ctCant.setCellValue("Cant. Cupones");
            ctCant.setCellStyle(estiloCabecera);

            Cell ctMonto = fCabecera.createCell(8);
            ctMonto.setCellValue("Monto Compras");
            ctMonto.setCellStyle(estiloCabecera);

            Integer totalCupones = 0;
            Integer totalMonto = 0;

            for (int i = 0; i < lstDatosXLS.size(); i++) {
                ClienteCuponesMontoBean cliBean = lstDatosXLS.get(i);
                Cliente cli = cliBean.getClienteObj();
                Row f = hoja.createRow(i + 2);
                Cell cApel = f.createCell(0);
                cApel.setCellValue(cli.getApellidos());
                Cell cNomb = f.createCell(1);
                cNomb.setCellValue(cli.getNombres());
                Cell cCi = f.createCell(2);
                cCi.setCellValue(cli.getCi());
                Cell cCel = f.createCell(3);
                cCel.setCellValue(cli.getCelular());
                Cell cTel = f.createCell(4);
                cTel.setCellValue(cli.getTelefono());
                Cell cEmail = f.createCell(5);
                cEmail.setCellValue(cli.getEmail());
                Cell cDir = f.createCell(6);
                cDir.setCellValue(cli.getDireccion());
                Cell cCant = f.createCell(7);
                cCant.setCellValue(cliBean.getCantidadCupones());
                Cell cMonto = f.createCell(8);
                cMonto.setCellValue(cliBean.getMontoFacturas());

                totalCupones = totalCupones + cliBean.getCantidadCupones();
                totalMonto = totalMonto + cliBean.getMontoFacturas();
            }

            XSSFFont fuenteResumen = libro.createFont();
            fuenteResumen.setFontHeightInPoints((short) 10);
            fuenteResumen.setBold(true);

            XSSFCellStyle estiloResumen = libro.createCellStyle();
            estiloResumen.setFont(fuenteResumen);
            estiloResumen.setBorderTop(BorderStyle.THIN);
            estiloResumen.setBorderBottom(BorderStyle.THIN);
            estiloResumen.setBorderLeft(BorderStyle.THIN);
            estiloResumen.setBorderRight(BorderStyle.THIN);

            Row filaTotal = hoja.createRow(lstDatosXLS.size() + 2);

            for (int i = 1; i < 7; i++) {
                Cell celdaResumen = filaTotal.createCell(i);
                celdaResumen.setCellStyle(estiloResumen);
            }

            Cell cEtiTotal = filaTotal.createCell(0);
            cEtiTotal.setCellValue("Total");
            cEtiTotal.setCellStyle(estiloResumen);

            Cell cTotalCupones = filaTotal.createCell(7);
            cTotalCupones.setCellValue(totalCupones);
            cTotalCupones.setCellStyle(estiloResumen);
            Cell cTotalMonto = filaTotal.createCell(8);
            cTotalMonto.setCellValue(totalMonto);
            cTotalMonto.setCellStyle(estiloResumen);

            for (int i = 0; i < 8; i++) {
                hoja.autoSizeColumn(i);
            }

            XSSFCellStyle estiloTitulo = libro.createCellStyle();
            XSSFFont fuenteTitulo = libro.createFont();
            fuenteTitulo.setFontHeightInPoints((short) 11);
            fuenteTitulo.setBold(true);
            estiloTitulo.setFont(fuenteTitulo);

            Row fila = hoja.createRow(0);
            Cell celda = fila.createCell(0);
            String titulo = "Cantidad de cupones y Total de compras";
            if (!rango.isEmpty()) {
                titulo = titulo + "(" + rango + ")";
            }
            celda.setCellValue(titulo);
            celda.setCellStyle(estiloTitulo);

            Date fhActual = new Date();
            SimpleDateFormat ffhactual = new SimpleDateFormat("dd-MM-yy_HH'h'mm'm'ss's'");
            try {
                FileOutputStream salidaArchivo = new FileOutputStream(new File(archSelec.getAbsolutePath() + "/InformePromo(" + ffhactual.format(fhActual) + ").xls"));
                libro.write(salidaArchivo);
                salidaArchivo.close();

            } catch (Exception ex) {
                LOG.log(Level.SEVERE, "Error al exportar archivo", ex);
                System.out.println("Error al crear archivo: " + ex);
            }
            this.btnExportarXLS.setDisable(false);
        } else {

        }
    }

    private void exportarXlsPorTienda() {

        if (archSelec != null) {
            //lstDatosXLSTienda.clear();
            String rango = "";
            if (rdbTodosCupones.isSelected()) {
                //lstDatosXLSTienda.addAll(InformesDataFactory.getClienteCuponesMonto(cmbPromocionPorCliente.getSelectionModel().getSelectedItem()));
            } else {
                Calendar fdesde = new GregorianCalendar();
                Calendar fhasta = new GregorianCalendar();

                LocalDate ldDesde = dpkDesde.getValue();
                fdesde.set(Calendar.YEAR, ldDesde.getYear());
                fdesde.set(Calendar.MONTH, (ldDesde.getMonthValue() - 1));
                fdesde.set(Calendar.DAY_OF_MONTH, ldDesde.getDayOfMonth());
                fdesde.set(Calendar.HOUR_OF_DAY, cmbHoraDesde.getSelectionModel().getSelectedItem());
                fdesde.set(Calendar.MINUTE, cmbMinutoDesde.getSelectionModel().getSelectedItem());
                fdesde.set(Calendar.SECOND, 0);

                LocalDate ldHasta = dpkHasta.getValue();
                fhasta.set(Calendar.YEAR, ldHasta.getYear());
                fhasta.set(Calendar.MONTH, (ldHasta.getMonthValue() - 1));
                fhasta.set(Calendar.DAY_OF_MONTH, ldHasta.getDayOfMonth());
                fhasta.set(Calendar.HOUR_OF_DAY, cmbHoraHasta.getSelectionModel().getSelectedItem());
                fhasta.set(Calendar.MINUTE, cmbMinutoHasta.getSelectionModel().getSelectedItem());
                fhasta.set(Calendar.SECOND, 0);

                SimpleDateFormat df = new SimpleDateFormat("dd/MM/yy HH:mm");
                Date ddesde = new Date(fdesde.getTimeInMillis());
                Date dhasta = new Date(fhasta.getTimeInMillis());
                rango = "Desde: " + df.format(ddesde) + " | Hasta: " + df.format(dhasta);
                //lstDatosXLS.addAll(InformesDataFactory.getClienteCuponesMontoRango(cmbPromocionPorCliente.getSelectionModel().getSelectedItem(), fdesde, fhasta));
            }

            XSSFWorkbook libro = new XSSFWorkbook();
            XSSFSheet hoja = libro.createSheet("Cupones");
            XSSFFont fuente = libro.createFont();
            fuente.setFontHeightInPoints((short) 10);

            XSSFCellStyle estiloCabecera = libro.createCellStyle();
            fuente.setBold(true);
            estiloCabecera.setFont(fuente);
            estiloCabecera.setBorderBottom(BorderStyle.THIN);
            estiloCabecera.setBorderTop(BorderStyle.THIN);
            estiloCabecera.setBorderLeft(BorderStyle.THIN);
            estiloCabecera.setBorderRight(BorderStyle.THIN);

            XSSFColor color = new XSSFColor(java.awt.Color.GRAY);
            estiloCabecera.setFillBackgroundColor(color);

            //Crear Cabeceras de la tabla
            Row fCabecera = hoja.createRow(1);
            Cell ctApel = fCabecera.createCell(0);
            ctApel.setCellValue("Apellidos");
            ctApel.setCellStyle(estiloCabecera);

            Cell ctNomb = fCabecera.createCell(1);
            ctNomb.setCellValue("Nombres");
            ctNomb.setCellStyle(estiloCabecera);

            Cell ctCi = fCabecera.createCell(2);
            ctCi.setCellValue("Documento");
            ctCi.setCellStyle(estiloCabecera);

            Cell ctCel = fCabecera.createCell(3);
            ctCel.setCellValue("Celular");
            ctCel.setCellStyle(estiloCabecera);

            Cell ctTel = fCabecera.createCell(4);
            ctTel.setCellValue("Teléfono");
            ctTel.setCellStyle(estiloCabecera);

            Cell ctEmail = fCabecera.createCell(5);
            ctEmail.setCellValue("Email");
            ctEmail.setCellStyle(estiloCabecera);

            Cell ctDir = fCabecera.createCell(6);
            ctDir.setCellValue("Dirección");
            ctDir.setCellStyle(estiloCabecera);

            Cell ctCant = fCabecera.createCell(7);
            ctCant.setCellValue("Cant. Cupones");
            ctCant.setCellStyle(estiloCabecera);

            Integer totalCupones = 0;
            Integer totalMonto = 0;

            //Recorrer datos y llenar planilla
            List<Tienda> lstColTiendas = new ArrayList<>();
            for (int i = 0; i < lstDatosXLSTienda.size(); i++) {

                ClienteTiendaBean cliBean = lstDatosXLSTienda.get(i);

                //Cargar campos con datos del cliente
                Cliente cli = cliBean.getClienteObj();
                Row f = hoja.createRow(i + 2);
                Cell cApel = f.createCell(0);
                cApel.setCellValue(cli.getApellidos());
                Cell cNomb = f.createCell(1);
                cNomb.setCellValue(cli.getNombres());
                Cell cCi = f.createCell(2);
                cCi.setCellValue(cli.getCi());
                Cell cCel = f.createCell(3);
                cCel.setCellValue(cli.getCelular());
                Cell cTel = f.createCell(4);
                cTel.setCellValue(cli.getTelefono());
                Cell cEmail = f.createCell(5);
                cEmail.setCellValue(cli.getEmail());
                Cell cDir = f.createCell(6);
                cDir.setCellValue(cli.getDireccion());
                Cell cTotCupones = f.createCell(7);
                cTotCupones.setCellValue(cliBean.getCantidadCupones());
                //Se recorre la lista de tiendas compradas por el cliente para crear las columnas
                for (TiendaMontoBean timo : cliBean.getLstTiendaMonto()) {
                    int columnaTiendaCelda = -1;
                    for (Tienda t : lstColTiendas) {
                        if (t.getIdTienda().equals(timo.getTiendaObj().getIdTienda())) {
                            columnaTiendaCelda = 8 + lstColTiendas.indexOf(t);
                            //System.out.println("Tienda ya existe en planilla> " + t.getNombre());
                        }
                    }
                    if (columnaTiendaCelda == -1) {
                       //System.out.println("Tienda NO existe en planilla> " + timo.getTienda());
                        lstColTiendas.add(timo.getTiendaObj());
                        columnaTiendaCelda = 8 + lstColTiendas.indexOf(timo.getTiendaObj());
                        Cell cTiendaCabecera = fCabecera.createCell(columnaTiendaCelda);
                        cTiendaCabecera.setCellStyle(estiloCabecera);
                        cTiendaCabecera.setCellValue(timo.getTienda());
                    }
                    Cell cMontoTienda = f.createCell(columnaTiendaCelda);
                    cMontoTienda.setCellValue(timo.getMonto());
                }

            }

            //Se recorre la planilla y se completa con ceros los campos vacios de los montos de las tiendas
            for (int ic = 2; ic < (lstDatosXLSTienda.size() + 2); ic++) {
                for (int it = 8; it < (lstColTiendas.size() + 8); it++) {
                    Row fila = hoja.getRow(ic);
                    if (fila != null) {
                        try {
                            Cell c = fila.getCell(it);
                            System.out.println(c.getNumericCellValue());
                        } catch (Exception ex) {
                            Cell cNueva = fila.createCell(it);
                            cNueva.setCellValue(0);
                        }
                    }
                }
            }

            //Se cargar el total de compras de cada cliente en la ultima columna
            try {
                //System.out.println("Tamanio lstDatosXLSTienda: " + lstDatosXLSTienda.size());
                for (int i = 0; i < lstDatosXLSTienda.size(); i++) {
                    //System.out.println("Indice total tienda: " + i);

                    ClienteTiendaBean ctbean = lstDatosXLSTienda.get(i);
                    Row f = hoja.getRow(i + 2);
                    Cell cMontoTotalCli = f.createCell(lstColTiendas.size() + 8);
                    if(ctbean.getTotalGeneral()==null){
                        System.out.println("Total general de "+ctbean.getCliente()+" NULL");
                        cMontoTotalCli.setCellValue(0);
                    }else{
                        cMontoTotalCli.setCellValue(ctbean.getTotalGeneral());
                    }
                    //Thread.sleep(1000);
                }
            } catch (Exception ex) {
                LOG.log(Level.SEVERE, "Error al exportar xls por tienda", ex);
            }

            Cell ctMonto = fCabecera.createCell(lstColTiendas.size() + 8);
            ctMonto.setCellValue("Monto Compras");
            ctMonto.setCellStyle(estiloCabecera);

            XSSFFont fuenteResumen = libro.createFont();
            fuenteResumen.setFontHeightInPoints((short) 10);
            fuenteResumen.setBold(true);

            XSSFCellStyle estiloResumen = libro.createCellStyle();
            estiloResumen.setFont(fuenteResumen);
            estiloResumen.setBorderTop(BorderStyle.THIN);
            estiloResumen.setBorderBottom(BorderStyle.THIN);
            estiloResumen.setBorderLeft(BorderStyle.THIN);
            estiloResumen.setBorderRight(BorderStyle.THIN);

            Row filaTotal = hoja.createRow(lstDatosXLSTienda.size() + 2);

            for (int i = 1; i < 7; i++) {
                Cell celdaResumen = filaTotal.createCell(i);
                celdaResumen.setCellStyle(estiloResumen);
            }

            Cell cEtiTotal = filaTotal.createCell(0);
            cEtiTotal.setCellValue("Total");
            cEtiTotal.setCellStyle(estiloResumen);

            //Se carga la formula para calcular el total de cuponnes
            Cell cTotCupones = filaTotal.createCell(7);
            cTotCupones.setCellStyle(estiloResumen);
            cTotCupones.setCellType(XSSFCell.CELL_TYPE_FORMULA);
            cTotCupones.setCellFormula("SUM(" + mapaLetraColumna.get(7) + 3 + ":" + mapaLetraColumna.get(7) + (this.lstDatosXLSTienda.size() + 2) + ")");

            //Se carga una formula para mostrar el total comprado en cada tienda
            for (int i = 0; i < lstColTiendas.size(); i++) {
                String colActual = this.mapaLetraColumna.get(i + 8);
                Integer nroFila = this.lstDatosXLSTienda.size() + 2;
                Cell celda = filaTotal.createCell(8 + i);
                celda.setCellStyle(estiloResumen);
                celda.setCellType(XSSFCell.CELL_TYPE_FORMULA);
                //System.out.println("insertando formula: "+"SUM("+colActual+"3:"+colActual+nroFila+")");
                celda.setCellFormula("SUM(" + colActual + "3:" + colActual + nroFila + ")");
            }

            //Insertar formula para calcular total de compras
            Integer iColumnaTotal = 8 + lstColTiendas.size();
            System.out.println("Indice Columna: " + iColumnaTotal);
            String letraColumnaTotal = mapaLetraColumna.get(iColumnaTotal);
            System.out.println("Letra columna total: " + letraColumnaTotal);
            Cell cTotalGeneralMonto = filaTotal.createCell(iColumnaTotal);
            cTotalGeneralMonto.setCellStyle(estiloResumen);
            cTotalGeneralMonto.setCellType(XSSFCell.CELL_TYPE_FORMULA);
            System.out.println("String formula: " + "SUM(" + letraColumnaTotal + "3:" + letraColumnaTotal + (lstDatosXLSTienda.size() + 2) + ")");
            cTotalGeneralMonto.setCellFormula("SUM(" + letraColumnaTotal + "3:" + letraColumnaTotal + (lstDatosXLSTienda.size() + 2) + ")");

            for (int i = 0; i < 8; i++) {
                hoja.autoSizeColumn(i);
            }

            XSSFCellStyle estiloTitulo = libro.createCellStyle();
            XSSFFont fuenteTitulo = libro.createFont();
            fuenteTitulo.setFontHeightInPoints((short) 11);
            fuenteTitulo.setBold(true);
            estiloTitulo.setFont(fuenteTitulo);

            Row fila = hoja.createRow(0);
            Cell celda = fila.createCell(0);
            String titulo = "Cantidad de cupones y Total de compras";
            if (!rango.isEmpty()) {
                titulo = titulo + "(" + rango + ")";
            }
            celda.setCellValue(titulo);
            celda.setCellStyle(estiloTitulo);

            Date fhActual = new Date();
            SimpleDateFormat ffhactual = new SimpleDateFormat("dd-MM-yy_HH'h'mm'm'ss's'");
            try {
                FileOutputStream salidaArchivo = new FileOutputStream(new File(archSelec.getAbsolutePath() + "/InformePromo(" + ffhactual.format(fhActual) + ").xls"));
                libro.write(salidaArchivo);
                salidaArchivo.close();
            } catch (Exception ex) {
                LOG.log(Level.SEVERE, "Error al exportar archivo", ex);
                System.out.println("Error al crear archivo: " + ex);
            }

        } else {

        }

    }
}
