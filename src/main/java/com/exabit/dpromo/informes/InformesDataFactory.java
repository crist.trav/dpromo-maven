/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exabit.dpromo.informes;

import com.exabit.dpromo.bd.ConexionBD;
import com.exabit.dpromo.bd.controllers.CuponJPAController;
import com.exabit.dpromo.bd.controllers.FacturaJPAController;
import com.exabit.dpromo.bd.controllers.PromocionJPAController;
import com.exabit.dpromo.bd.controllers.TiendaJPAController;
import com.exabit.dpromo.bd.entidades.Cliente;
import com.exabit.dpromo.bd.entidades.Promocion;
import com.exabit.dpromo.bd.entidades.Tienda;
import com.exabit.dpromo.informes.beans.ClienteCuponesMontoBean;
import com.exabit.dpromo.informes.beans.ClienteTiendaBean;
import com.exabit.dpromo.informes.beans.TiendaMontoBean;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author cristhian
 */
public class InformesDataFactory {
    
    public static Collection<ClienteCuponesMontoBean> getClienteCuponesMonto(Promocion p){
        //Lista de clientes en la promocion
        CuponJPAController cujpa=new CuponJPAController(ConexionBD.EMF.createEntityManager());
        FacturaJPAController factjpa=new FacturaJPAController(ConexionBD.EMF.createEntityManager());
        PromocionJPAController promojpa=new PromocionJPAController(ConexionBD.EMF.createEntityManager());
        //Lista de clientes que participaron en la promocion
        List<Cliente> lstClientesParticipantes=promojpa.getClientesParticipantes(p);
        //Coleccion de beans que seran los datos para el informe
        Collection<ClienteCuponesMontoBean> lstCCMB=new ArrayList<>();
        //Se carga la cantidad de cupones y el monto facturado de cada cliente
        for(Cliente cli:lstClientesParticipantes){
            ClienteCuponesMontoBean ccmb=new ClienteCuponesMontoBean();
            ccmb.setCantidadCupones(cujpa.getCantidadCupones(p, cli));
            ccmb.setMontoFacturas(factjpa.getTotalFacturas(p, cli));
            ccmb.setClienteObj(cli);
            lstCCMB.add(ccmb);
        }
        return lstCCMB;
    }
    public static Collection<ClienteCuponesMontoBean> getClienteCuponesMontoRango(Promocion p, Calendar desde, Calendar hasta){
        //Lista de clientes en la promocion
        CuponJPAController cujpa=new CuponJPAController(ConexionBD.EMF.createEntityManager());
        FacturaJPAController factjpa=new FacturaJPAController(ConexionBD.EMF.createEntityManager());
        PromocionJPAController promojpa=new PromocionJPAController(ConexionBD.EMF.createEntityManager());
        //Lista de clientes que participaron en la promocion
        List<Cliente> lstClientesParticipantes=promojpa.getClientesParticipantesRango(p,desde,hasta);
        //Coleccion de beans que seran los datos para el informe
        Collection<ClienteCuponesMontoBean> lstCCMB=new ArrayList<>();
        //Se carga la cantidad de cupones y el monto facturado de cada cliente
        for(Cliente cli:lstClientesParticipantes){
            ClienteCuponesMontoBean ccmb=new ClienteCuponesMontoBean();
            ccmb.setCantidadCupones(cujpa.getCantidadCuponesRango(p, cli,desde,hasta));
            ccmb.setMontoFacturas(factjpa.getTotalFacturasRango(p, cli,desde,hasta));
            ccmb.setClienteObj(cli);
            lstCCMB.add(ccmb);
        }
        return lstCCMB;
    }
    
    public static Collection<ClienteTiendaBean> getClienteTiendas(Promocion p){
        PromocionJPAController projpa=new PromocionJPAController(ConexionBD.EMF.createEntityManager());
        TiendaJPAController tijpa=new TiendaJPAController(ConexionBD.EMF.createEntityManager());
        FacturaJPAController facjpa=new FacturaJPAController(ConexionBD.EMF.createEntityManager());
        CuponJPAController cujpa=new CuponJPAController(ConexionBD.EMF.createEntityManager());
        
        List<Cliente> lstCliParticipantes=projpa.getClientesParticipantes(p);
        for(Cliente cl:lstCliParticipantes){
            if(cl.getIdCliente().equals(973)){
                System.err.println("973 participa");
            }
        }
        List<ClienteTiendaBean> lstClienteTienda=new LinkedList<>();
        
        lstCliParticipantes.stream().forEach((cli)->{
            ClienteTiendaBean ctb=new ClienteTiendaBean();
            ctb.setClienteObj(cli);
            ctb.setCantidadCupones(cujpa.getCantidadCupones(p, cli));
            List<Tienda> lstTiendasCliente=tijpa.getTiendasClientesPromocion(p, cli);
            ObservableList<TiendaMontoBean> lstTiendaMonto=FXCollections.observableArrayList();
            for(Tienda t:lstTiendasCliente){
                TiendaMontoBean tmb=new TiendaMontoBean();
                tmb.setTiendaObj(t);
                tmb.setMonto(facjpa.getTotalFacturacionTiendaCliente(p, t, cli));
                lstTiendaMonto.add(tmb);
            }
            ctb.setLstTiendaMonto(lstTiendaMonto);
            lstClienteTienda.add(ctb);
        });
        return lstClienteTienda;
    }
    
    public static Collection<ClienteTiendaBean> getClienteTiendasRango(Promocion p, Calendar fdesde, Calendar fhasta){
        PromocionJPAController projpa=new PromocionJPAController(ConexionBD.EMF.createEntityManager());
        TiendaJPAController tijpa=new TiendaJPAController(ConexionBD.EMF.createEntityManager());
        FacturaJPAController facjpa=new FacturaJPAController(ConexionBD.EMF.createEntityManager());
        CuponJPAController cujpa=new CuponJPAController(ConexionBD.EMF.createEntityManager());
        
        List<Cliente> lstCliParticipantes=projpa.getClientesParticipantesRango(p,fdesde,fhasta);
        List<ClienteTiendaBean> lstClienteTienda=new LinkedList<>();
        
        lstCliParticipantes.stream().forEach((cli)->{
            ClienteTiendaBean ctb=new ClienteTiendaBean();
            ctb.setClienteObj(cli);
            ctb.setCantidadCupones(cujpa.getCantidadCuponesRango(p, cli, fdesde, fhasta));
            List<Tienda> lstTiendasCliente=tijpa.getTiendasClientesPromocionRango(p, cli, fdesde, fhasta);
            ObservableList<TiendaMontoBean> lstTiendaMonto=FXCollections.observableArrayList();
            for(Tienda t:lstTiendasCliente){
                TiendaMontoBean tmb=new TiendaMontoBean();
                tmb.setTiendaObj(t);
                tmb.setMonto(facjpa.getTotalFacturacionTiendaClienteRango(p, t, cli,fdesde,fhasta));
                lstTiendaMonto.add(tmb);
            }
            ctb.setLstTiendaMonto(lstTiendaMonto);
            lstClienteTienda.add(ctb);
        });
        return lstClienteTienda;
    }
    
}
