/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exabit.dpromo.informes;

import com.exabit.dpromo.bd.entidades.Promocion;
import java.text.SimpleDateFormat;
import java.util.Date;
import javafx.scene.control.ListCell;

/**
 *
 * @author cristhian
 */
public class PromocionListCell extends ListCell<Promocion> {
    
    @Override
    protected void updateItem(Promocion p, boolean empty){
        if(p!=null){
            SimpleDateFormat df=new SimpleDateFormat("dd/MM/yy");
            String fi="";
            String ff="";
            if(p.getFechaInicio()!=null){
                Date dfi=new Date(p.getFechaInicio().getTimeInMillis());
                fi=df.format(dfi);
            }
            if(p.getFechaFin()!=null){
                Date dff=new Date(p.getFechaFin().getTimeInMillis());
                ff=df.format(dff);
            }
            String desc=p.getDescripcion()!=null?p.getDescripcion():"";
            this.setText(desc+"|"+fi+"-"+ff);
        }else{
            this.setText("");
        }
        super.updateItem(p, empty);
    }
    
}
