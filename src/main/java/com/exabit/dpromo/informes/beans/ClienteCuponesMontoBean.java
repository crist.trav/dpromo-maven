/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exabit.dpromo.informes.beans;

import com.exabit.dpromo.bd.entidades.Cliente;
import java.io.Serializable;
import java.text.DecimalFormat;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;

/**
 *
 * @author cristhian
 */
public class ClienteCuponesMontoBean implements Serializable {

    private String cliente;
    private Integer montoFacturas;
    private Integer cantidadCupones;
    private final ObjectProperty<Cliente> clienteObj = new SimpleObjectProperty<>();
    
    private final ChangeListener<Cliente> chLnrCliente=(ObservableValue<? extends Cliente> obs, Cliente oldValue, Cliente newValue)->{
        if(newValue==null){
            this.cliente="";
        }else{
            DecimalFormat df=new DecimalFormat("###,###,###,###,###,###");
            this.cliente=newValue.getApellidos()+", "+newValue.getNombres()+" ("+df.format(newValue.getCi())+")";
        }
    };

    public ClienteCuponesMontoBean() {
        this.clienteObj.addListener(chLnrCliente);
    }

    
    public Cliente getClienteObj() {
        return clienteObj.get();
    }

    public void setClienteObj(Cliente value) {
        clienteObj.set(value);
    }

    public ObjectProperty clienteObjProperty() {
        return clienteObj;
    }

    public String getCliente() {
        return cliente;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    public Integer getMontoFacturas() {
        return montoFacturas;
    }

    public void setMontoFacturas(Integer montoFacturas) {
        this.montoFacturas = montoFacturas;
    }

    public Integer getCantidadCupones() {
        return cantidadCupones;
    }

    public void setCantidadCupones(Integer cantidadCupones) {
        this.cantidadCupones = cantidadCupones;
    }

}
