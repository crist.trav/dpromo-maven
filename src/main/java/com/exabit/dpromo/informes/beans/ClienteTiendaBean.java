/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exabit.dpromo.informes.beans;

import com.exabit.dpromo.bd.entidades.Cliente;
import java.util.List;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;

/**
 *
 * @author cristhian
 */
public class ClienteTiendaBean {

    private final ChangeListener<Cliente> chlnrCliente=(ObservableValue<? extends Cliente> obs, Cliente oldValue, Cliente newValue)->{
        if(newValue!=null){
            this.cliente=newValue.getApellidos()+", "+newValue.getNombres();
        }else{
            this.cliente="";
        }
    };
    
    private final ListChangeListener<TiendaMontoBean> lstChLnrTienda=(ListChangeListener.Change<? extends TiendaMontoBean> c)->{
         
            
        c.next();
        //if(this.clienteObj.getValue().getIdCliente()==973){
                //System.out.println("cli id:"+this.clienteObj.getValue().getIdCliente()+" TiendaMonto agragada: "+c.getList().size());
            //} 
        List<TiendaMontoBean> lstTM=(List<TiendaMontoBean>) c.getList();
        Integer t=0;
        for(TiendaMontoBean tm:lstTM){
           
            t=t+tm.getMonto();
        }
        totalGeneral=t;
    };
    
    private Integer totalGeneral=0;

    public Integer getTotalGeneral() {
        return totalGeneral;
    }

    public void setTotalGeneral(Integer totalGeneral) {
        this.totalGeneral = totalGeneral;
    }
    
    private Integer cantidadCupones;

    public Integer getCantidadCupones() {
        return cantidadCupones;
    }

    public void setCantidadCupones(Integer cantidadCupones) {
        this.cantidadCupones = cantidadCupones;
    }

    public ClienteTiendaBean() {
        this.clienteObj.addListener(chlnrCliente);
        this.lstTiendaMonto.addListener(this.lstChLnrTienda);
    }
    

    private final ObjectProperty<Cliente> clienteObj = new SimpleObjectProperty<>();

    public Cliente getClienteObj() {
        return clienteObj.get();
    }

    public void setClienteObj(Cliente value) {
        clienteObj.set(value);
    }

    public ObjectProperty clienteObjProperty() {
        return clienteObj;
    }
    
    
    
    private String cliente;

    public String getCliente() {
        return cliente;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    private final ObservableList<TiendaMontoBean> lstTiendaMonto=FXCollections.observableArrayList();

    public ObservableList<TiendaMontoBean> getLstTiendaMonto() {
        return lstTiendaMonto;
    }

    public void setLstTiendaMonto(ObservableList<TiendaMontoBean> lstTiendaMonto) {
        if(lstTiendaMonto!=null){
            if(lstTiendaMonto.isEmpty()){
                System.out.println("lstTiendaMonto Vacia "+this.clienteObj.getValue().getIdCliente());
            }
        }else{
            System.out.println("lstTiendaMonto NULL "+this.clienteObj.getValue().getIdCliente());
        }
        this.lstTiendaMonto.clear();
        this.lstTiendaMonto.addAll(lstTiendaMonto);
        //this.lstTiendaMonto = lstTiendaMonto;
    }

}
