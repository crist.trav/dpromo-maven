/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exabit.dpromo.informes.beans;

import com.exabit.dpromo.bd.entidades.Tienda;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;

/**
 *
 * @author cristhian
 */
public class TiendaMontoBean {

    private final ObjectProperty<Tienda> tiendaObj = new SimpleObjectProperty<>();

    private final ChangeListener<Tienda> chlnrTienda=(ObservableValue<? extends Tienda> obs, Tienda oldValue, Tienda newValue)->{
        if(newValue!=null){
            tienda=newValue.getNombre();
        }else{
            tienda="";
        }
    };
    
    public TiendaMontoBean() {
        this.tiendaObj.addListener(chlnrTienda);
    }
    

    public Tienda getTiendaObj() {
        return tiendaObj.get();
    }

    public void setTiendaObj(Tienda value) {
        tiendaObj.set(value);
    }

    public ObjectProperty tiendaObjProperty() {
        return tiendaObj;
    }
    
    
    private String tienda;

    public String getTienda() {
        return tienda;
    }

    public void setTienda(String tienda) {
        this.tienda = tienda;
    }

        private Integer monto;

    public Integer getMonto() {
        return monto;
    }

    public void setMonto(Integer monto) {
        this.monto = monto;
    }

    
}
