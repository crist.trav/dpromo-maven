/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exabit.dpromo.ciudades;

import com.exabit.dpromo.bd.ConexionBD;
import com.exabit.dpromo.bd.controllers.CiudadJPAController;
import com.exabit.dpromo.bd.controllers.DepartamentoJPAController;
import com.exabit.dpromo.bd.entidades.Ciudad;
import com.exabit.dpromo.bd.entidades.Departamento;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Accordion;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.TitledPane;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

/**
 * FXML Controller class
 *
 * @author cristhian
 */
public class CiudadesController implements Initializable {

    @FXML
    private Button btnRegistrarCiudad;
    @FXML
    private Button btnEliminarCiudad;
    @FXML
    private Button btnNuevaCiudad;
    @FXML
    private TableView<Ciudad> tblCiudades;
    @FXML
    private TableColumn<Ciudad, Integer> colCodigoCiudad;
    @FXML
    private TableColumn<Ciudad, String> colNombreCiudad;
    @FXML
    private TableColumn<Ciudad, Departamento> colDepartamentoCiudad;
    @FXML
    private Label Código;
    @FXML
    private Accordion accCiudad;
    @FXML
    private TitledPane tpaneDatos;
    @FXML
    private TextField txfNombreCiudad;
    @FXML
    private ComboBox<Departamento> cmbDepartamentoCiudad;
    @FXML
    private TextField txfCodigoCiudad;
    @FXML
    private ImageView imgvBuscar;
    @FXML
    private TextField txfBusquedaCiudad;
    
    private static final int OPERACION_REGISTRAR=1;
    private static final int OPERACION_MODIFICAR=2;
    private int operacionActual=OPERACION_REGISTRAR;
    
    private final ObservableList<Ciudad> lstCiudades=FXCollections.observableArrayList();
    private final ObservableList<Departamento> lstDepartamentosCmb=FXCollections.observableArrayList();
    
    private final ObjectProperty<Ciudad> seleccionCiudad=new SimpleObjectProperty<>();
    
    private final ChangeListener<Ciudad> chLnrSeleccionCiudad=(ObservableValue<? extends Ciudad> obs, Ciudad oldValue, Ciudad newValue)->{
        if(newValue!=null){
            this.operacionActual=CiudadesController.OPERACION_MODIFICAR;
            this.txfCodigoCiudad.setText(newValue.getIdCiudad().toString());
            this.txfNombreCiudad.setText(newValue.getNombre());
            
            for(Departamento d:this.lstDepartamentosCmb){
                if(d.getIdDepartamento().equals(newValue.getDepartamento().getIdDepartamento())){
                    this.cmbDepartamentoCiudad.getSelectionModel().select(d);
                    break;
                }
            }
            Image imgEditar=new Image(getClass().getResourceAsStream("/iconos/x16/edit.png"));
            this.btnRegistrarCiudad.setGraphic(new ImageView(imgEditar));
            this.btnRegistrarCiudad.setText("Modificar");
        }else{
            this.operacionActual=CiudadesController.OPERACION_REGISTRAR;
            this.txfCodigoCiudad.setText("");
            this.txfNombreCiudad.setText("");
            this.cmbDepartamentoCiudad.getSelectionModel().clearSelection();
            Image imgGuardar=new Image(getClass().getResourceAsStream("/iconos/x16/save.png"));
            this.btnRegistrarCiudad.setGraphic(new ImageView(imgGuardar));
            this.btnRegistrarCiudad.setText("Registrar");
        }
    };
    
    ListChangeListener<Ciudad> lstChLnrSeleccionCiudades=(ListChangeListener.Change<? extends Ciudad> c)->{
        c.next();
        if(c.getList().size()==1){
            this.seleccionCiudad.setValue(c.getList().get(0));
        }else if(c.getList().size()>1){
            this.seleccionCiudad.setValue(null);
        }
    };

    private final ChangeListener<String> chLnrStrBusqueda=(ObservableValue<? extends String> obs, String oldValue, String newValue)->{
        this.lstCiudades.clear();
        CiudadJPAController cjpa=new CiudadJPAController(ConexionBD.EMF.createEntityManager());
        if(!newValue.isEmpty()){
            this.lstCiudades.addAll(cjpa.buscarCiudad(newValue));
        }else{
            this.lstCiudades.addAll(cjpa.getTodasCiudades());
        }
    };
    

    /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        this.accCiudad.setExpandedPane(this.tpaneDatos);
        this.cargarIconos();
        this.cmbDepartamentoCiudad.prefWidthProperty().bind(this.txfNombreCiudad.widthProperty());
        this.inicializarTablaCiudades();
        this.inicializarComboDepartamentos();
        CiudadJPAController cjpa=new CiudadJPAController(ConexionBD.EMF.createEntityManager());
        this.lstCiudades.addAll(cjpa.getTodasCiudades());
        DepartamentoJPAController djpa=new DepartamentoJPAController(ConexionBD.EMF.createEntityManager());
        this.lstDepartamentosCmb.addAll(djpa.getDepartamentosOrdenado());
        this.seleccionCiudad.addListener(this.chLnrSeleccionCiudad);
        this.tblCiudades.getSelectionModel().getSelectedItems().addListener(this.lstChLnrSeleccionCiudades);
        this.txfBusquedaCiudad.textProperty().addListener(this.chLnrStrBusqueda);
    }    
    
    private boolean sonCamposValidos(){
        Alert dlg=new Alert(AlertType.WARNING);
        dlg.setTitle("Guardar Ciudad");
        boolean camposValidos=false;
        if(!this.txfNombreCiudad.getText().isEmpty()){
            if(!this.cmbDepartamentoCiudad.getSelectionModel().isEmpty()){
                camposValidos=true;
            }else{
                dlg.setContentText("No se ha seleccionado ningún Departamento");
                dlg.showAndWait();
            }
        }else{
            dlg.setContentText("El campo 'nombre' está en blanco");
            dlg.showAndWait();
        }
            
        return camposValidos;
    }
    
    private void registrarCiudad(){
        if(this.sonCamposValidos()){
            CiudadJPAController ciujpa=new CiudadJPAController(ConexionBD.EMF.createEntityManager());
            Ciudad c=new Ciudad();
            c.setNombre(this.txfNombreCiudad.getText());
            c.setDepartamento(this.cmbDepartamentoCiudad.getSelectionModel().getSelectedItem());
            ciujpa.registrarCiudad(c);
            this.lstCiudades.add(c);
            this.tblCiudades.getSelectionModel().select(c);
        }
    }
    
    private void modificarCiudad(){
        if(this.sonCamposValidos()){
            CiudadJPAController ciujpa=new CiudadJPAController(ConexionBD.EMF.createEntityManager());
            int posicionCiudad=this.lstCiudades.indexOf(this.seleccionCiudad.getValue());
            Ciudad c=new Ciudad();
            c.setIdCiudad(this.seleccionCiudad.getValue().getIdCiudad());
            c.setNombre(this.txfNombreCiudad.getText());
            c.setDepartamento(this.cmbDepartamentoCiudad.getSelectionModel().getSelectedItem());
            ciujpa.modificarCiudad(c);
            this.lstCiudades.remove(posicionCiudad);
            this.lstCiudades.add(posicionCiudad,c);
            this.tblCiudades.getSelectionModel().select(c);
        }
    }
    
    private void eliminarCiudades(){
        CiudadJPAController cjpa=new CiudadJPAController(ConexionBD.EMF.createEntityManager());
        
        if(!this.tblCiudades.getSelectionModel().isEmpty()){
            Alert confi=new Alert(AlertType.CONFIRMATION);
            confi.setTitle("Eliminar Ciudades");
            confi.setContentText("Se eliminarán las ciudades seleccionadas.\n¿Desea continuar?");
            Optional<ButtonType> eleccion=confi.showAndWait();
            if(eleccion.get().equals(ButtonType.OK)){
                List<Ciudad> lstAEliminar=new ArrayList<>();
                lstAEliminar.addAll(this.tblCiudades.getSelectionModel().getSelectedItems());
                for(Ciudad ci:lstAEliminar){
                    if(cjpa.registraClientes(ci)){
                        Alert al=new Alert(AlertType.ERROR);
                        al.setTitle("Eliminar Ciudades");
                        al.setContentText("No se puede eliminar '"+ci.getIdCiudad()+"-"+ci.getNombre()+" porque\nexisten clientes registrados en esta ciudad");
                        al.showAndWait();
                    }else{
                        cjpa.elimnarCiudad(ci);
                        this.lstCiudades.remove(ci);
                    }
                }
            }
        }else{
            Alert dlg=new Alert(AlertType.WARNING);
            dlg.setTitle("Eliminar ciudades");
            dlg.setContentText("Seleccione por lo menos una Ciudad");
            dlg.showAndWait();
        }
        
    }
    
    private void cargarIconos(){
        Image imgGuardar=new Image(getClass().getResourceAsStream("/iconos/x16/save.png"));
        this.btnRegistrarCiudad.setGraphic(new ImageView(imgGuardar));
        Image imgEliminar=new Image(getClass().getResourceAsStream("/iconos/x16/delete.png"));
        this.btnEliminarCiudad.setGraphic(new ImageView(imgEliminar));
        Image imgNuevo=new Image(getClass().getResourceAsStream("/iconos/x16/document.png"));
        this.btnNuevaCiudad.setGraphic(new ImageView(imgNuevo));
        Image imgBuscar=new Image(getClass().getResourceAsStream("/iconos/x16/search.png"));
        this.imgvBuscar.setImage(imgBuscar);
    }
    
    private void inicializarTablaCiudades(){
        this.tblCiudades.setItems(lstCiudades);
        this.colCodigoCiudad.setCellValueFactory(new PropertyValueFactory<>("idCiudad"));
        this.colNombreCiudad.setCellValueFactory(new PropertyValueFactory<>("nombre"));
        this.colDepartamentoCiudad.setCellValueFactory(new PropertyValueFactory<>("departamento"));
        this.colDepartamentoCiudad.setCellFactory((TableColumn<Ciudad,Departamento> col)->new DepartamentoTableCell());
    }
    
    private void inicializarComboDepartamentos(){
        this.cmbDepartamentoCiudad.setItems(this.lstDepartamentosCmb);
        this.cmbDepartamentoCiudad.setCellFactory((ListView<Departamento> lst)->new DepartamentoListCell());
        this.cmbDepartamentoCiudad.setButtonCell(new DepartamentoListCell());
    }

    @FXML
    private void onActionBtnRegistrarCiudad(ActionEvent event) {
        if(this.operacionActual==CiudadesController.OPERACION_REGISTRAR){
            this.registrarCiudad();
        }else if(this.operacionActual==CiudadesController.OPERACION_MODIFICAR){
            this.modificarCiudad();
        }
    }

    @FXML
    private void onActionBtnEliminarCiuadad(ActionEvent event) {
        this.eliminarCiudades();
    }

    @FXML
    private void onActionBtnNuevaCiudad(ActionEvent event) {
        this.seleccionCiudad.setValue(null);
    }
    
}
