/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exabit.dpromo.ciudades;

import com.exabit.dpromo.bd.entidades.Departamento;
import javafx.scene.control.ListCell;

/**
 *
 * @author cristhian
 */
public class DepartamentoListCell extends ListCell<Departamento> {
    
    @Override
    protected void updateItem(Departamento d, boolean empty){
        if(d!=null){
            this.setText(d.getNombre());
        }else{
            this.setText("");
        }
        super.updateItem(d, empty);
    }
    
}
