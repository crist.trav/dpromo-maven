/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exabit.dpromo.impresion;

import com.exabit.dpromo.bd.ConexionBD;
import com.exabit.dpromo.bd.controllers.CuponJPAController;
import com.exabit.dpromo.bd.controllers.ParametrosJPAController;
import com.exabit.dpromo.bd.entidades.Cliente;
import com.exabit.dpromo.bd.entidades.Cupon;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXMLLoader;
import javafx.print.PageLayout;
import javafx.print.PageOrientation;
import javafx.print.Printer;
import javafx.print.PrinterJob;
import javafx.scene.Node;
import javafx.scene.transform.Scale;

/**
 *
 * @author cristhian
 */
public class ImpresionCupon {

    private static final Logger LOG = Logger.getLogger(ImpresionCupon.class.getName());

    private final CuponJPAController cuponjpa = new CuponJPAController(ConexionBD.EMF.createEntityManager());

    public ImpresionCupon() {
    }

    /*public void imprimir(Factura f, List<Cupon> lstc) {
        List<Cupon> lstCupon = new ArrayList<>();
        lstCupon.addAll(lstc);
        System.out.println("En metodo imprimir");
        VBox vboxcupo = new VBox();
        vboxcupo.getTransforms().add(new Scale(0.65, 0.65));
        int cantidadCupones = lstCupon.size();
        int contadorRepeticiones = 0;
        for (Cupon cu : lstCupon) {
            contadorRepeticiones++;
            try {
                FXMLLoader loader = new FXMLLoader();
                Node cuponImpresion = loader.load(getClass().getResourceAsStream("PlantillaCupon.fxml"));

                PlantillaCuponController plcContr = loader.getController();
                plcContr.cargarDatos(f, cu);
                System.out.println("Cupon Np: " + cu.getIdCupon());

                System.out.println("Tamanio vbox: " + vboxcupo.getChildren().size());
                System.out.println("Repeticiones: " + contadorRepeticiones);
                System.out.println("Cantidad cupones: " + cantidadCupones);
                if (vboxcupo.getChildren().size() < 3) {
                    vboxcupo.getChildren().add(cuponImpresion);
                    System.out.println("Se agreca cupon a vbox cant actual>" + vboxcupo.getChildren().size());
                    if (contadorRepeticiones == cantidadCupones) {
                        System.out.println("Limite de cupones> 2+");

                        Printer impre = Printer.getDefaultPrinter();
                        PageLayout pl = impre.createPageLayout(impre.getDefaultPageLayout().getPaper(), PageOrientation.PORTRAIT, 0, 0, 0, 0);
                        PrinterJob job = PrinterJob.createPrinterJob(impre);
                        if (job != null) {
                            System.out.println("Trabajo de impresion no nulo");
                            boolean success = job.printPage(pl, vboxcupo);
                            if (success) {
                                job.endJob();
                            }
                        }
                        vboxcupo.getChildren().clear();
                    }
                } else if (vboxcupo.getChildren().size() == 3) {
                    System.out.println("Se imprime una pagina tamaniovbox:: " + vboxcupo.getChildren().size());

                    Printer impre = Printer.getDefaultPrinter();
                    PageLayout pl = impre.createPageLayout(impre.getDefaultPageLayout().getPaper(), PageOrientation.PORTRAIT, 0, 0, 0, 0);
                    PrinterJob job = PrinterJob.createPrinterJob(impre);
                    if (job != null) {
                        System.out.println("Trabajo de impresion no nulo");
                        boolean success = job.printPage(pl, vboxcupo);
                        if (success) {
                            job.endJob();
                        }
                    }

                    Printer impre2 = Printer.getDefaultPrinter();
                    PageLayout pl2 = impre.createPageLayout(impre.getDefaultPageLayout().getPaper(), PageOrientation.PORTRAIT, 0, 0, 0, 0);
                    PrinterJob job2 = PrinterJob.createPrinterJob(impre2);
                    vboxcupo.getChildren().clear();

                    vboxcupo.getChildren().add(cuponImpresion);
                    if (contadorRepeticiones == cantidadCupones) {
                        System.out.println("Limite de cupones> 1+");
                        if (job != null) {
                            System.out.println("Trabajo de impresion no nulo");
                            boolean success = job2.printPage(pl2, vboxcupo);
                            if (success) {
                                job2.endJob();
                            }
                        }
                        //vboxcupo.getChildren().clear();
                    }
                } else {
                    System.out.println("VBox mayor a 3");
                }
                System.out.println("--------------------");
            } catch (Exception ex) {
                System.err.println("Error al cargar fxml: " + ex.getMessage());
            }

        }
    }*/

    /*public void imprimir(Cliente c, List<Cupon> lstc) {
        List<Cupon> lstCupon = new ArrayList<>();
        lstCupon.addAll(lstc);
        System.out.println("En metodo imprimir");
        VBox vboxcupo = new VBox();
        List<Cupon> lstTmpCuponImpreso = new ArrayList<>();
        vboxcupo.getTransforms().add(new Scale(0.65, 0.65));
        int cantidadCupones = lstCupon.size();
        int contadorRepeticiones = 0;
        for (Cupon cu : lstCupon) {
            contadorRepeticiones++;
            try {
                FXMLLoader loader = new FXMLLoader();
                Node cuponImpresion = loader.load(getClass().getResourceAsStream("PlantillaCupon.fxml"));

                PlantillaCuponController plcContr = loader.getController();
                plcContr.cargarDatos(c, cu);
                System.out.println("Cupon Np: " + cu.getIdCupon());

                System.out.println("Tamanio vbox: " + vboxcupo.getChildren().size());
                System.out.println("Repeticiones: " + contadorRepeticiones);
                System.out.println("Cantidad cupones: " + cantidadCupones);
                if (vboxcupo.getChildren().size() < 3) {
                    vboxcupo.getChildren().add(cuponImpresion);
                    lstTmpCuponImpreso.add(cu);
                    System.out.println("Se agreca cupon a vbox cant actual>" + vboxcupo.getChildren().size());
                    if (contadorRepeticiones == cantidadCupones) {
                        System.out.println("Limite de cupones> 2+");

                        Printer impre = Printer.getDefaultPrinter();
                        PageLayout pl = impre.createPageLayout(impre.getDefaultPageLayout().getPaper(), PageOrientation.PORTRAIT, 0, 0, 0, 0);
                        PrinterJob job = PrinterJob.createPrinterJob(impre);
                        if (job != null) {
                            System.out.println("Trabajo de impresion no nulo");
                            boolean success = job.printPage(pl, vboxcupo);
                            if (success) {
                                job.endJob();
                                CuponJPAController cujpa = new CuponJPAController(ConexionBD.EMF.createEntityManager());
                                lstTmpCuponImpreso.stream().forEach((cui) -> {
                                    cujpa.marcarCuponImpreso(cui);
                                });
                            }
                        }
                        vboxcupo.getChildren().clear();
                        lstTmpCuponImpreso.clear();
                    }
                } else if (vboxcupo.getChildren().size() == 3) {
                    System.out.println("Se imprime una pagina tamaniovbox:: " + vboxcupo.getChildren().size());

                    Printer impre = Printer.getDefaultPrinter();
                    PageLayout pl = impre.createPageLayout(impre.getDefaultPageLayout().getPaper(), PageOrientation.PORTRAIT, 0, 0, 0, 0);
                    PrinterJob job = PrinterJob.createPrinterJob(impre);
                    if (job != null) {
                        System.out.println("Trabajo de impresion no nulo");
                        boolean success = job.printPage(pl, vboxcupo);
                        if (success) {
                            job.endJob();
                            CuponJPAController cujpa = new CuponJPAController(ConexionBD.EMF.createEntityManager());
                            lstTmpCuponImpreso.stream().forEach((cui) -> {
                                cujpa.marcarCuponImpreso(cui);
                            });
                        }
                    }

                    Printer impre2 = Printer.getDefaultPrinter();
                    PageLayout pl2 = impre.createPageLayout(impre.getDefaultPageLayout().getPaper(), PageOrientation.PORTRAIT, 0, 0, 0, 0);
                    PrinterJob job2 = PrinterJob.createPrinterJob(impre2);
                    vboxcupo.getChildren().clear();

                    vboxcupo.getChildren().add(cuponImpresion);
                    lstTmpCuponImpreso.add(cu);
                    if (contadorRepeticiones == cantidadCupones) {
                        System.out.println("Limite de cupones> 1+");
                        if (job != null) {
                            System.out.println("Trabajo de impresion no nulo");
                            boolean success = job2.printPage(pl2, vboxcupo);
                            if (success) {
                                job2.endJob();
                                CuponJPAController cujpa = new CuponJPAController(ConexionBD.EMF.createEntityManager());
                                lstTmpCuponImpreso.stream().forEach((cui) -> {
                                    cujpa.marcarCuponImpreso(cui);
                                });
                            }
                        }
                        //vboxcupo.getChildren().clear();
                    }
                } else {
                    System.out.println("VBox mayor a 3");
                }
                System.out.println("--------------------");
            } catch (Exception ex) {
                System.err.println("Error al cargar fxml: " + ex.getMessage());
            }

        }
    }*/
    public void imprimir(Cliente c, List<Cupon> lstc) {
        List<Cupon> lstCupon = new ArrayList<>();
        lstCupon.addAll(lstc);
        System.out.println("En metodo imprimir");
        for (Cupon cu : lstCupon) {
            try {
                ParametrosJPAController pjpa=new ParametrosJPAController();
                FXMLLoader loader = new FXMLLoader();
                Node cuponImpresion = loader.load(getClass().getResourceAsStream("/fxml/impresion/PlantillaCupon.fxml"));

                PlantillaCuponController plcContr = loader.getController();
                plcContr.cargarDatos(c, cu);
                System.out.println("Cupon Np: " + cu.getIdCupon());                
                String strImpre=pjpa.getParametroSistema(ParametrosJPAController.IMPRESORA_TICKETS);
                Printer impre = Printer.getDefaultPrinter();
                if(strImpre!=null){
                    for(Printer p:Printer.getAllPrinters()){
                        if(p.getName().equals(strImpre)){
                            impre=p;
                            break;
                        }
                    }
                }else{
                    impre = Printer.getDefaultPrinter();    
                }
                PageLayout pl = impre.createPageLayout(impre.getDefaultPageLayout().getPaper(), PageOrientation.PORTRAIT, 0, 0, 0, 0);
                PrinterJob job = PrinterJob.createPrinterJob(impre);
                String escala=pjpa.getParametroSistema(ParametrosJPAController.ESCALA_TICKET);
                if(escala!=null){
                    Double sc=Double.parseDouble(escala);
                    cuponImpresion.getTransforms().add(new Scale(sc,sc));
                }
                if (job != null) {
                    System.out.println("Trabajo de impresion no nulo");
                    boolean success = job.printPage(pl, cuponImpresion);
                    if (success) {
                        job.endJob();
                        CuponJPAController cujpa = new CuponJPAController(ConexionBD.EMF.createEntityManager());
                        cujpa.marcarCuponImpreso(cu);
                    }
                }

                System.out.println("--------------------");
            } catch (IOException | NumberFormatException ex) {
                System.err.println("Error al cargar fxml: " + ex.getMessage());
                LOG.log(Level.SEVERE, "error al imprimi", ex);
            }
        }

    }

}
