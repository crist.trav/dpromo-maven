/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exabit.dpromo.impresion;

import com.exabit.dpromo.bd.controllers.ParametrosJPAController;
import com.exabit.dpromo.bd.entidades.Cliente;
import com.exabit.dpromo.bd.entidades.Cupon;
import com.exabit.dpromo.bd.entidades.Factura;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.beans.property.SimpleObjectProperty;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

/**
 * FXML Controller class
 *
 * @author cristhian
 */
public class PlantillaCuponController implements Initializable {

    @FXML
    private Text lblIdCupon;
    @FXML
    private Text lblNombres;
    @FXML
    private Text lblApellidos;
    @FXML
    private Text lblCi;
    @FXML
    private Text lblCelular;
    @FXML
    private Text lblTelefono;
    @FXML
    private Text lblDireccion;
    @FXML
    private Text lblEmail;
    @FXML
    private Text lblCiudad;
    //private ImageView ivwLogo;
    
    private final SimpleObjectProperty<Cupon> cuponProperty=new SimpleObjectProperty<>();
    @FXML
    private Text tituloNroCupon;
    @FXML
    private Text tituloNombres;
    @FXML
    private Text tituloApellidos;
    @FXML
    private Text tituloCi;
    @FXML
    private Text tituloCelular;
    @FXML
    private Text tituloTelefono;
    @FXML
    private Text tituloDireccion;
    @FXML
    private Text tituloEmail;
    @FXML
    private Text tituloCiudad;
    @FXML
    private VBox vboxItemsCupon;
    
    private List<Text> itemsTicket = new ArrayList<>();
    @FXML
    private Text lblTituloDShopping;
    

    /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        this.vboxItemsCupon.getChildren().forEach((item)->{
            if(item instanceof Text){
                this.itemsTicket.add((Text)item);
            }
        });
        this.itemsTicket.add(this.tituloNroCupon);
        this.itemsTicket.add(this.lblIdCupon);
        
        /**try{
            Image imgLogo=new Image(getClass().getResourceAsStream("/imagenes/d2.png"));
            this.ivwLogo.setImage(imgLogo);
        }catch(Exception ex){
            System.err.println("Error al cargar logo: "+ex.getMessage());
        }*/
        ParametrosJPAController pjpa = new ParametrosJPAController();
        String tamLetraStr = pjpa.getParametroSistema(ParametrosJPAController.TAMANIO_LETRA_CUPON);
        
        Double tamanioLetra = tamLetraStr != null ? Double.valueOf(tamLetraStr) : 13.0;
        
        this.cambiarTamanioLetra(tamanioLetra);
        /*this.vboxItemsCupon.widthProperty().addListener((ObservableValue<? extends Number> obs, Number oldValue, Number newValue)->{
            this.ivwLogo.setFitWidth((newValue.doubleValue() * 20.0)/100.0);
        });*/
    }    
    
    public void cargarDatos(Factura f, Cupon c){
        this.cuponProperty.setValue(c);
        System.out.println("Cupon Recibido: "+c.getIdCupon());
        System.out.println("factura: "+f.getCliente().getNombres());
        this.lblIdCupon.setText(c.getIdCupon().toString());
        this.lblApellidos.setText(f.getCliente().getApellidos());
        this.lblCelular.setText(f.getCliente().getCelular());
        this.lblCi.setText(f.getCliente().getCi().toString());
        this.lblCiudad.setText(f.getCliente().getCiudad().getNombre()+" - "+f.getCliente().getCiudad().getDepartamento().getNombre());
        this.lblDireccion.setText(f.getCliente().getDireccion());
        this.lblEmail.setText(f.getCliente().getEmail());
        this.lblNombres.setText(f.getCliente().getNombres());
        this.lblTelefono.setText(f.getCliente().getTelefono());
    }
    public void cargarDatos(Cliente cli, Cupon c){
        this.cuponProperty.setValue(c);
        System.out.println("Cupon Recibido: "+c.getIdCupon());
        System.out.println("factura: "+cli.getNombres());
        this.lblIdCupon.setText(c.getIdCupon().toString());
        this.lblApellidos.setText(cli.getApellidos());
        this.lblCelular.setText(cli.getCelular());
        this.lblCi.setText(cli.getCi().toString());
        this.lblCiudad.setText(cli.getCiudad().getNombre()+" - "+cli.getCiudad().getDepartamento().getNombre());
        this.lblDireccion.setText(cli.getDireccion());
        this.lblEmail.setText(cli.getEmail());
        this.lblNombres.setText(cli.getNombres());
        this.lblTelefono.setText(cli.getTelefono());
    }
    
    public Cupon getCupon(){
        return this.cuponProperty.getValue();
    }
    
    private void cambiarTamanioLetra(Double tamanio){
        this.itemsTicket.forEach((item)->{
            Font fuenteAnterior = ((Text)item).getFont();
            Font nuevaFuente = new Font(fuenteAnterior.getName(), tamanio);
            item.setFont(nuevaFuente);
        });
    }
    
}
