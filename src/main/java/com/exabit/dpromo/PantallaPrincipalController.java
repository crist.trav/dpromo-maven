/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exabit.dpromo;

import com.exabit.dpromo.bd.controllers.ParametrosJPAController;
import java.io.IOException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;

/**
 *
 * @author cristhian
 */
public class PantallaPrincipalController implements Initializable {
    
    @FXML
    private VBox vboxMenuPrinc;
    @FXML
    private TabPane tpanePrincipal;
    @FXML
    private ProgressIndicator prgInd;
    @FXML
    private Label lblStatus;
    @FXML
    private AnchorPane apaneLateralIza;
    @FXML
    private Button btnMenuTiendas;
    @FXML
    private Button btnGeneradorCupones;
    @FXML
    private ImageView imgvLogo;
    @FXML
    private Button btnMenuCiudades;
    @FXML
    private Button btnMenuClientes;
    @FXML
    private Button btnMenuInformes;
    @FXML
    private Button btnMenuMinimizar;
    
    private static final int MENU_MINIMIZADO=1;
    private static final int MENU_MAXIMIZADO=2;
    private int estadoMenu=MENU_MAXIMIZADO;
    
    public static final int TIPO_MENSAJE_INFO=1;
    public static final int TIPO_MENSAJE_ALERTA=2;
    public static final int TIPO_MENSAJE_ERROR=3;
    
    private final List<String> lstTitulosMenu=new ArrayList<>();
    @FXML
    private Button btnMenuDepartamentos;
    @FXML
    private Button btnMenuPromociones;
    @FXML
    private Button btnMenuAjustes;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        for(Node n:this.vboxMenuPrinc.getChildren()){
            if(n instanceof Button){
                Button btnm=(Button)n;
                this.lstTitulosMenu.add(btnm.getText());
            }
        }
        this.cargarIconos();
        this.apaneLateralIza.getStyleClass().add("fondo-menuprincipal");
        this.vboxMenuPrinc.getChildren().stream().forEach((menu)->{
            if(menu instanceof Button){
                Button b=(Button)menu;
                b.setAlignment(Pos.CENTER_LEFT);
                b.prefWidthProperty().bind(this.vboxMenuPrinc.widthProperty());
                b.getStyleClass().add("boton-menuprincipal");
            }
        });
        
        this.btnMenuMinimizar.getStyleClass().add("boton-menuprincipal");
        this.cargarPestania("Generador", "/fxml/generador/GeneradorCupones.fxml");
        this.cargarPreferencias();
    }
    
    private void cargarPreferencias(){
        ParametrosJPAController prejpa=new ParametrosJPAController();
        String estMenu=prejpa.getParametroSistema(ParametrosJPAController.ESTADO_MENU_PRINCIPAL);
        if(estMenu!=null){
            Integer estMenuInt=Integer.parseInt(estMenu);
            if(estMenuInt!=this.estadoMenu){
                this.maxMinMenuPrincipal();
            }
        }
    }
    
    public void mostrarMensaje(String msg, int tipoMensaje){
        Date fh=new Date();
        SimpleDateFormat df=new SimpleDateFormat("[dd/MM/yy HH:mm:ss]");
        String mensaje=df.format(fh);
        mensaje=mensaje+" "+msg;
        this.lblStatus.setText(mensaje);
        switch(tipoMensaje){
            case PantallaPrincipalController.TIPO_MENSAJE_INFO:
                this.lblStatus.setTextFill(Color.BLACK);
                break;
            case PantallaPrincipalController.TIPO_MENSAJE_ERROR:
                this.lblStatus.setTextFill(Color.RED);
                break;
            case PantallaPrincipalController.TIPO_MENSAJE_ALERTA:
                this.lblStatus.setTextFill(Color.DARKORANGE);
                break;
        }
    }
    
    public void mostrarProgress(boolean mostrar){
        this.prgInd.setVisible(mostrar);
    }
    
    private void cargarPestania(String nombrePestania, String urlModulo){
        
        boolean pestaniaAbierta=false;
        for(Tab t:this.tpanePrincipal.getTabs()){
            if(t.getText().equals(nombrePestania)){
                this.tpanePrincipal.getSelectionModel().select(t);
                this.mostrarMensaje("Cambiado a '"+nombrePestania+"'", TIPO_MENSAJE_INFO);
                pestaniaAbierta=true;
                break;
            }
        }
        if(!pestaniaAbierta){
            Tab pestania=new Tab();
            pestania.setText(nombrePestania);
            try {
                Node modulo = FXMLLoader.load(getClass().getResource(urlModulo));
                pestania.setContent(modulo);
                this.tpanePrincipal.getTabs().add(pestania);
                this.tpanePrincipal.getSelectionModel().select(pestania);
                this.mostrarMensaje("Cargado módulo '"+nombrePestania+"'", TIPO_MENSAJE_INFO);
            } catch (IOException ex) {
                Logger.getLogger(PantallaPrincipalController.class.getName()).log(Level.SEVERE, "Error al cargar pestania", ex);
            }
        }
    }
    
    
    private void cargarIconos(){
        Image imgLogo=new Image(getClass().getResourceAsStream("/imagenes/d2.png"));
        this.imgvLogo.setImage(imgLogo);
        
        this.imgvLogo.translateXProperty().bind(this.apaneLateralIza.widthProperty().divide(2).subtract(this.imgvLogo.fitWidthProperty().divide(2)));
        
        Image imgTiendas=new Image(getClass().getResourceAsStream("/iconos/x32/store-icon.png"));
        this.btnMenuTiendas.setGraphic(new ImageView(imgTiendas));
        
        Image imgCupon=new Image(getClass().getResourceAsStream("/iconos/x32/blue-ticket-circle.png"));
        this.btnGeneradorCupones.setGraphic(new ImageView(imgCupon));
        
        Image imgLugar=new Image(getClass().getResourceAsStream("/iconos/x32/location.png"));
        this.btnMenuCiudades.setGraphic(new ImageView(imgLugar));
       
        Image imgCliente=new Image(getClass().getResourceAsStream("/iconos/x32/contacts.png"));
        this.btnMenuClientes.setGraphic(new ImageView(imgCliente));
        
        Image imgInformes=new Image(getClass().getResourceAsStream("/iconos/x32/report.png"));
        this.btnMenuInformes.setGraphic(new ImageView(imgInformes));
        
        Image imgLugarDep=new Image(getClass().getResourceAsStream("/iconos/x32/location-alt1.png"));
        this.btnMenuDepartamentos.setGraphic(new ImageView(imgLugarDep));
        
        Image imgPromo=new Image(getClass().getResourceAsStream("/iconos/x32/promotion.png"));
        this.btnMenuPromociones.setGraphic(new ImageView(imgPromo));
        
        Image imgSettings=new Image(getClass().getResourceAsStream("/iconos/x32/settings.png"));
        this.btnMenuAjustes.setGraphic(new ImageView(imgSettings));
    }
    
    private void maxMinMenuPrincipal(){
        if(this.estadoMenu==PantallaPrincipalController.MENU_MAXIMIZADO){
            this.mostrarMensaje("Menu principal Minimizado", TIPO_MENSAJE_INFO);
            this.apaneLateralIza.setPrefWidth(50);
            this.estadoMenu=PantallaPrincipalController.MENU_MINIMIZADO;
            Image imgLogo=new Image(getClass().getResourceAsStream("/imagenes/app-dshopping.png"));
            this.imgvLogo.setImage(imgLogo);
            this.imgvLogo.setFitHeight(38);
            this.imgvLogo.setFitWidth(38);
            this.vboxMenuPrinc.getChildren().stream().forEach((n)->{
                if(n instanceof Button){
                    ((Button)n).setText("");
                }
            });
            this.btnMenuMinimizar.setText(">>");
        }else if(this.estadoMenu==PantallaPrincipalController.MENU_MINIMIZADO){
            this.mostrarMensaje("Menu principal Maximizado", TIPO_MENSAJE_INFO);
            this.estadoMenu=PantallaPrincipalController.MENU_MAXIMIZADO;
            Image imgLogo=new Image(getClass().getResourceAsStream("/imagenes/d2.png"));
            this.imgvLogo.setImage(imgLogo);
            this.imgvLogo.setFitHeight(100);
            this.imgvLogo.setFitWidth(100);
            for(Node n:this.vboxMenuPrinc.getChildren()){
                if(n instanceof Button){
                    Button bMenu=(Button)n;
                    bMenu.setText(this.lstTitulosMenu.get(this.vboxMenuPrinc.getChildren().indexOf(n)));
                    /*if(bMenu.getPrefWidth()>mayorTamanioMenu){
                        
                    }*/
                }
            }
            this.apaneLateralIza.setPrefWidth(160);
            this.btnMenuMinimizar.setText("<<");
        }
        ParametrosJPAController parjpa=new ParametrosJPAController();
        parjpa.setParametroSistema(ParametrosJPAController.ESTADO_MENU_PRINCIPAL, this.estadoMenu+"");
    }

    @FXML
    private void onActionBtnCupones(ActionEvent event) {
        this.cargarPestania("Generador", "/fxml/generador/GeneradorCupones.fxml");
    }

    @FXML
    private void onActionBtnTiendas(ActionEvent event) {
        this.cargarPestania("Tiendas", "/fxml/tiendas/Tiendas.fxml");
    }

    @FXML
    private void onActionBtnCiudades(ActionEvent event) {
        this.cargarPestania("Ciudades", "/fxml/ciudades/Ciudades.fxml");
    }

    @FXML
    private void onActionBtnClientes(ActionEvent event) {
        this.cargarPestania("Clientes", "/fxml/clientes/Clientes.fxml");
    }

    @FXML
    private void onActionBtnInformes(ActionEvent event) {
        this.cargarPestania("Informes", "/fxml/informes/Informes.fxml");
    }

    @FXML
    private void onActionMenuMinimizar(ActionEvent event) {
        this.maxMinMenuPrincipal();
    }

    @FXML
    private void onActionBtnDepartamentos(ActionEvent event) {
        this.cargarPestania("Departamentos","/fxml/departamentos/Departamentos.fxml");
    }

    @FXML
    private void onActionPromociones(ActionEvent event) {
        this.cargarPestania("Promociones", "/fxml/promociones/Promociones.fxml");
    }

    @FXML
    private void onActionBtnAjustes(ActionEvent event) {
        this.cargarPestania("Ajustes", "/fxml/ajustes/Ajustes.fxml");
    }
}
