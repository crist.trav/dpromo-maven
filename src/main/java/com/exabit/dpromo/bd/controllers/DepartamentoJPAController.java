/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exabit.dpromo.bd.controllers;

import com.exabit.dpromo.bd.entidades.Ciudad;
import com.exabit.dpromo.bd.entidades.Departamento;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

/**
 *
 * @author cristhian
 */
public class DepartamentoJPAController {
    
    private final EntityManager em;

    public DepartamentoJPAController(EntityManager em) {
        this.em = em;
    }
    
    public void modificarDepartamento(Departamento d){
        this.em.getTransaction().begin();
        this.em.merge(d);
        this.em.getTransaction().commit();
        
    }
    
    public List<Departamento> getDepartamentosOrdenado(){
        TypedQuery<Departamento> q=this.em.createQuery("SELECT d FROM Departamento d ORDER BY d.nombre ASC", Departamento.class);
        List<Departamento> lstRes=q.getResultList();
        lstRes.stream().forEach((d)->{
            this.em.refresh(d);
        });
        return lstRes;
    }
    
    public List<Departamento> getTodosDepartamentos(){
        TypedQuery<Departamento> q=this.em.createQuery("SELECT d FROM Departamento d", Departamento.class);
        List<Departamento> lstDep=q.getResultList();
        lstDep.stream().forEach((d)->{
            this.em.refresh(d);
        });
        return lstDep;
    }
    
    public void registrarDepartamento(Departamento d){
        this.em.getTransaction().begin();
        this.em.persist(d);
        this.em.getTransaction().commit();
    }
    
    public boolean registraCiudades(Departamento d){
        TypedQuery<Ciudad> q=this.em.createQuery("SELECT c FROM Ciudad c WHERE c.departamento=:dep", Ciudad.class);
        q.setParameter("dep", d);
        List<Ciudad> lstCiu=q.getResultList();
        return !lstCiu.isEmpty();
    }
    
    public void eliminarDepartamento(Departamento d){
        this.em.getTransaction().begin();
        this.em.remove(this.em.merge(d));
        this.em.getTransaction().commit();
    }
    
    public List<Departamento> buscarDepartamento(String criterio){
        TypedQuery<Departamento> q=this.em.createQuery("SELECT d FROM Departamento d WHERE LOWER(d.nombre) LIKE :crit",Departamento.class);
        q.setParameter("crit", "%"+criterio.toLowerCase()+"%");
        List<Departamento> lstDep=q.getResultList();
        lstDep.stream().forEach((d)->{
            this.em.refresh(d);
        });
        return lstDep;
    }
    
}
