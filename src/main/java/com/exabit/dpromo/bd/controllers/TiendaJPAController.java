/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exabit.dpromo.bd.controllers;

import com.exabit.dpromo.bd.entidades.Cliente;
import com.exabit.dpromo.bd.entidades.Factura;
import com.exabit.dpromo.bd.entidades.Promocion;
import com.exabit.dpromo.bd.entidades.Tienda;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

/**
 *
 * @author cristhian
 */
public class TiendaJPAController {
    
    private final EntityManager em;
    
    public static final List<Tienda> LST_TIENDAS_DEFAULT=new ArrayList<>();
    
    static{
        /*Tienda sin=new Tienda();
        sin.setIdTienda(1);
        sin.setNombre("(Sin Tienda)");
        sin.setActivo(true);
        LST_TIENDAS_DEFAULT.add(sin);*/
        
        Tienda dportivo=new Tienda();
        dportivo.setIdTienda(2);
        dportivo.setNombre("Dportivo");
        dportivo.setActivo(true);
        LST_TIENDAS_DEFAULT.add(dportivo);
        
        Tienda rinconsport=new Tienda();
        rinconsport.setIdTienda(3);
        rinconsport.setNombre("Rincón Sport");
        rinconsport.setActivo(true);
        LST_TIENDAS_DEFAULT.add(rinconsport);
        
        Tienda kilk=new Tienda();
        kilk.setIdTienda(4);
        kilk.setNombre("Kilkenny");
        kilk.setActivo(true);
        LST_TIENDAS_DEFAULT.add(kilk);
        
        Tienda pelupichi=new Tienda();
        pelupichi.setIdTienda(5);
        pelupichi.setNombre("Pelupichi Park");
        pelupichi.setActivo(true);
        LST_TIENDAS_DEFAULT.add(pelupichi);
        
        Tienda tobago=new Tienda();
        tobago.setIdTienda(6);
        tobago.setNombre("Tobago");
        tobago.setActivo(true);
        LST_TIENDAS_DEFAULT.add(tobago);
        
        Tienda luomo=new Tienda();
        luomo.setIdTienda(7);
        luomo.setNombre("L'uomo");
        luomo.setActivo(true);
        LST_TIENDAS_DEFAULT.add(luomo);
        
        Tienda nice=new Tienda();
        nice.setIdTienda(8);
        nice.setNombre("Nice");
        nice.setActivo(true);
        LST_TIENDAS_DEFAULT.add(nice);
        
        Tienda artemisa=new Tienda();
        artemisa.setIdTienda(9);
        artemisa.setNombre("Artemisa");
        artemisa.setActivo(true);
        LST_TIENDAS_DEFAULT.add(artemisa);
        
        Tienda rondina=new Tienda();
        rondina.setIdTienda(10);
        rondina.setNombre("Rondina");
        rondina.setActivo(true);
        LST_TIENDAS_DEFAULT.add(rondina);
        
        Tienda eustaquia=new Tienda();
        eustaquia.setIdTienda(11);
        eustaquia.setNombre("Ña Eustaquia");
        eustaquia.setActivo(true);
        LST_TIENDAS_DEFAULT.add(eustaquia);
        
        Tienda maranello=new Tienda();
        maranello.setIdTienda(12);
        maranello.setNombre("Maranello");
        maranello.setActivo(true);
        LST_TIENDAS_DEFAULT.add(maranello);
        
        Tienda buffalo=new Tienda();
        buffalo.setIdTienda(13);
        buffalo.setNombre("Buffalo");
        buffalo.setActivo(true);
        LST_TIENDAS_DEFAULT.add(buffalo);
        
        Tienda donvito=new Tienda();
        donvito.setIdTienda(14);
        donvito.setNombre("Don Vito");
        donvito.setActivo(true);
        LST_TIENDAS_DEFAULT.add(donvito);
        
        Tienda bellini=new Tienda();
        bellini.setIdTienda(15);
        bellini.setNombre("Bellini");
        bellini.setActivo(true);
        LST_TIENDAS_DEFAULT.add(bellini);
        
        Tienda amandau=new Tienda();
        amandau.setIdTienda(16);
        amandau.setNombre("Amandau");
        amandau.setActivo(true);
        LST_TIENDAS_DEFAULT.add(amandau);
        
        Tienda vision=new Tienda();
        vision.setIdTienda(17);
        vision.setNombre("Vision Banco");
        vision.setActivo(true);
        LST_TIENDAS_DEFAULT.add(vision);
        
        Tienda personal=new Tienda();
        personal.setIdTienda(18);
        personal.setNombre("Personal");
        personal.setActivo(true);
        LST_TIENDAS_DEFAULT.add(personal);
        
        Tienda tigo=new Tienda();
        tigo.setIdTienda(19);
        tigo.setNombre("Tigo");
        tigo.setActivo(true);
        LST_TIENDAS_DEFAULT.add(tigo);
        
        Tienda citymark=new Tienda();
        citymark.setIdTienda(20);
        citymark.setNombre("City Market");
        citymark.setActivo(true);
        LST_TIENDAS_DEFAULT.add(citymark);
        
        Tienda claro=new Tienda();
        claro.setIdTienda(21);
        claro.setNombre("Claro");
        claro.setActivo(true);
        LST_TIENDAS_DEFAULT.add(claro);
    }

    public TiendaJPAController(EntityManager em) {
        this.em = em;
    }
    
    public void modificarTienda(Tienda t){
        this.em.getTransaction().begin();
        this.em.merge(t);
        this.em.getTransaction().commit();
    }
    public void registrarTienda(Tienda t){
        this.em.getTransaction().begin();
        this.em.persist(t);
        this.em.getTransaction().commit();
    }
    
    public List<Tienda> getTiendasActivasOrdenado(){
        TypedQuery q=this.em.createQuery("SELECT t FROM Tienda t WHERE t.activo=true ORDER BY t.nombre ASC", Tienda.class);
        List<Tienda> lstRes=new ArrayList<>();
        lstRes.addAll(q.getResultList());
        lstRes.stream().forEach((ti)->{
            this.em.refresh(ti);
        });
        return lstRes;
    }
    
    public List<Tienda> getTodasTiendas(){
        TypedQuery q=this.em.createQuery("SELECT t FROM Tienda t", Tienda.class);
        List<Tienda> lstRes=new ArrayList<>();
        lstRes.addAll(q.getResultList());
        lstRes.stream().forEach((ti)->{
            this.em.refresh(ti);
        });
        return lstRes;
    }
    
    public void eliminarTienda(Tienda t){
        this.em.getTransaction().begin();
        this.em.remove(this.em.merge(t));
        this.em.getTransaction().commit();
    }
    
    public List<Tienda> buscarTienda(String crit){
        TypedQuery<Tienda> q=this.em.createQuery("SELECT t FROM Tienda t WHERE LOWER(t.nombre) LIKE :crit", Tienda.class);
        q.setParameter("crit", "%"+crit.toLowerCase()+"%");
        List<Tienda> lstRes=q.getResultList();
        lstRes.stream().forEach((t)->{
            this.em.refresh(t);
        });
        return lstRes;
    }
    
    public List<Tienda> getTiendasClientesPromocion(Promocion p, Cliente c){
        //System.out.println("get tiendas cliente promocion: "+p.getIdPromocion()+" "+c.getIdCliente());
        //TypedQuery<Tienda> q=this.em.createQuery("SELECT f.tienda FROM Factura f WHERE f.cliente=:cli AND f.promocion=:promo GROUP BY f.tienda", Tienda.class);
        TypedQuery<Factura> qf=this.em.createQuery("SELECT f FROM Factura f WHERE f.cliente.idCliente=:cli AND f.promocion.idPromocion=:promo", Factura.class);
        //q.setParameter("cli", c);
        //q.setParameter("promo", p);
        TypedQuery<Factura> f=this.em.createQuery("SELECT f FROM Factura f WHERE f.cliente.idCliente=973 AND f.promocion.idPromocion=6", Factura.class);
        System.out.println("facturas para 973: "+f.getResultList().size());
        qf.setParameter("cli", c.getIdCliente());
        qf.setParameter("promo", p.getIdPromocion());
        
        List<Factura> lstFacturas=qf.getResultList();
        List<Tienda> lstTiendas=new ArrayList<>();
        
        for(Factura fa:lstFacturas){
            boolean tiendaExiste=false;
            //if(c.getIdCliente().equals(973)){
                //System.out.println("Factura "+fa.getIdFactura()+" cliente 973: "+fa.getTienda().getNombre()+" "+fa.getMontoFactura());
            //}
            for(Tienda ti:lstTiendas){
                if(ti.getIdTienda().equals(fa.getTienda().getIdTienda())){
                    tiendaExiste=true;
                }
            }
            if(!tiendaExiste){
                lstTiendas.add(fa.getTienda());
            }
        }
        //if(c.getIdCliente().equals(973)){
            //System.out.println("Tiendas cliente: "+lstTiendas);
        //}
        //List lstTiendas=q.getResultList();
        lstTiendas.stream().forEach((t)->{
            this.em.refresh(t);
        });
        
        return lstTiendas;
    }
    public List<Tienda> getTiendasClientesPromocionRango(Promocion p, Cliente c, Calendar fdesde, Calendar fhasta){
        TypedQuery<Tienda> q=this.em.createQuery("SELECT f.tienda FROM Factura f WHERE f.fechaHoraRegistro>=:fdesde AND f.fechaHoraRegistro<=:fhasta AND f.cliente=:cli AND f.promocion=:promo GROUP BY f.tienda", Tienda.class);
        q.setParameter("cli", c);
        q.setParameter("promo", p);
        q.setParameter("fdesde", fdesde);
        q.setParameter("fhasta", fhasta);
        List lstTiendas=q.getResultList();
        lstTiendas.stream().forEach((t)->{
            this.em.refresh(t);
        });
        return lstTiendas;
    }
    
}
