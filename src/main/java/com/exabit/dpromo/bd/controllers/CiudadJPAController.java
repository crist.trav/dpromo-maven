/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exabit.dpromo.bd.controllers;

import com.exabit.dpromo.bd.entidades.Ciudad;
import com.exabit.dpromo.bd.entidades.Cliente;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

/**
 *
 * @author cristhian
 */
public class CiudadJPAController {
    private final EntityManager em;

    public CiudadJPAController(EntityManager em) {
        this.em = em;
    }
    
    public void modificarCiudad(Ciudad c){
        this.em.getTransaction().begin();
        this.em.merge(c);
        this.em.getTransaction().commit();
    }
    
    public List<Ciudad> getCiudadesOrdenado(){
        TypedQuery<Ciudad> q=this.em.createQuery("SELECT c FROM Ciudad c ORDER BY c.departamento.nombre, c.nombre ASC", Ciudad.class);
        List<Ciudad> lstResultado=new ArrayList<>();
        lstResultado.addAll(q.getResultList());
        lstResultado.stream().forEach((ciu) -> {
            this.em.refresh(ciu);
        });
        return lstResultado;
    }
    
    public List<Ciudad> getTodasCiudades(){
        TypedQuery<Ciudad> q=this.em.createQuery("SELECT c FROM Ciudad c", Ciudad.class);
        List<Ciudad> lstResultado=new ArrayList<>();
        lstResultado.addAll(q.getResultList());
        lstResultado.stream().forEach((ciu) -> {
            this.em.refresh(ciu);
        });
        return lstResultado;
    }
    
    public void registrarCiudad(Ciudad c){
        this.em.getTransaction().begin();
        this.em.persist(c);
        this.em.getTransaction().commit();
    }
    
    public boolean registraClientes(Ciudad c){
        TypedQuery<Cliente> q=this.em.createQuery("SELECT c FROM Cliente c WHERE c.ciudad=:ciu", Cliente.class);
        q.setParameter("ciu", c);
        return !q.getResultList().isEmpty();
    }
    
    public void elimnarCiudad(Ciudad c){
        this.em.getTransaction().begin();
        this.em.remove(this.em.merge(c));
        this.em.getTransaction().commit();
    }
    
    public List<Ciudad> buscarCiudad(String criterio){
        TypedQuery<Ciudad> q=this.em.createQuery("SELECT c FROM Ciudad c WHERE LOWER(c.nombre) LIKE :crit OR LOWER(c.departamento.nombre) LIKE :crit",Ciudad.class);
        q.setParameter("crit", "%"+criterio.toLowerCase()+"%");
        List<Ciudad> lstRes=q.getResultList();
        lstRes.stream().forEach((c)->{
            this.em.refresh(c);
        });
        return lstRes;
    }
}
