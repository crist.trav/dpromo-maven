/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exabit.dpromo.bd.controllers;

import com.exabit.dpromo.bd.entidades.Cliente;
import com.exabit.dpromo.bd.entidades.Cupon;
import com.exabit.dpromo.bd.entidades.Factura;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

/**
 *
 * @author cristhian
 */
public class ClienteJPAController {
    
    private final EntityManager em;

    public ClienteJPAController(EntityManager em) {
        this.em = em;
    }
    
    
    public void modificarCliente(Cliente c){
        this.em.getTransaction().begin();
        this.em.merge(c);
        this.em.getTransaction().commit();
    }
    
    public Cliente getClientePorCi(Integer ci){
        TypedQuery<Cliente> q=this.em.createQuery("SELECT c FROM Cliente c WHERE c.ci=:ci", Cliente.class);
        q.setParameter("ci", ci);
        List<Cliente> lstRes=new ArrayList<>();
        lstRes.addAll(q.getResultList());
        if(!lstRes.isEmpty()){
            this.em.refresh(lstRes.get(0));
            return lstRes.get(0);
        }else{
            return null;
        }
    }
    
    public void registrarCliente(Cliente c){
        this.em.getTransaction().begin();
        this.em.persist(c);
        this.em.getTransaction().commit();
    }
    
    public List<Cliente> getTodosLosClientes(){
        TypedQuery<Cliente> q=this.em.createQuery("SELECT c FROM Cliente c", Cliente.class);
        List<Cliente> lstRes=q.getResultList();
        lstRes.stream().forEach((c)->{
            this.em.refresh(c);
        });
        return lstRes;
    }
    
    public void eliminarCliente(Cliente c){
        this.em.getTransaction().begin();
        this.em.remove(this.em.merge(c));
        this.em.getTransaction().commit();
    }
    
    public boolean tieneRegistrosFactura(Cliente c){
        TypedQuery<Factura> q=this.em.createQuery("SELECT f FROM Factura f WHERE f.cliente=:cli", Factura.class);
        q.setParameter("cli", c);
        List<Factura> lstFacts=q.getResultList();
        return !lstFacts.isEmpty();
    }
    
    public boolean tieneRegistroCupones(Cliente c){
        TypedQuery<Cupon> q=this.em.createQuery("SELECT c FROM Cupon c WHERE c.cliente=:cli", Cupon.class);
        q.setParameter("cli", c);
        List<Cupon> lstRes=q.getResultList();
        return !lstRes.isEmpty();
    }
    
    public List<Cliente> buscarCliente(String crit){
        TypedQuery<Cliente> q=this.em.createQuery("SELECT c FROM Cliente c WHERE LOWER(c.nombres) LIKE :crit "
                + "OR LOWER(c.apellidos) LIKE :crit "
                + "OR LOWER(c.celular) LIKE :crit "
                + "OR LOWER(c.direccion) LIKE :crit "
                + "OR LOWER(c.telefono) LIKE :crit "
                + "OR LOWER(c.email) LIKE :crit",Cliente.class);
        q.setParameter("crit", "%"+crit.toLowerCase()+"%");
        List<Cliente> lstRes=q.getResultList();
        lstRes.stream().forEach((c)->{
            this.em.refresh(c);
        });
        return lstRes;
    }   
    
    /*public List<Cliente> getClientesPorPromocion(Promocion p){
        TypedQuery<Cliente> q=this.em.createQuery("SELECT c FROM Cliente c WHERE c.")
    }*/
    
}
