/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exabit.dpromo.bd.controllers;

import com.exabit.dpromo.bd.entidades.Cliente;
import com.exabit.dpromo.bd.entidades.Cupon;
import com.exabit.dpromo.bd.entidades.Factura;
import com.exabit.dpromo.bd.entidades.Promocion;
import com.exabit.dpromo.bd.entidades.RangoPromocion;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

/**
 *
 * @author cristhian
 */
public class CuponJPAController {

    private final EntityManager em;

    public CuponJPAController(EntityManager em) {
        this.em = em;
    }

    public List<Cupon> generarCupones(List<Factura> lstFact) {
        PromocionJPAController prjpa = new PromocionJPAController(em);
        Promocion promoActual = prjpa.getPromocionActiva();
        Integer totalFactura = 0;
        Cliente cli = lstFact.get(0).getCliente();
        this.em.getTransaction().begin();
        List<Cupon> lstCuponesGenerados = new ArrayList<>();
        for (Factura f : lstFact) {
            totalFactura = totalFactura + f.getMontoFactura();
            f.setProcesado(true);
            f.setPromocion(promoActual);
            this.em.merge(f);
        }
        Integer totalCupones = 0;
        if (!promoActual.isPorRango()) {
            totalCupones = totalFactura / promoActual.getMontoCupon();
        } else {
            for(RangoPromocion rp:prjpa.getRangosPromocion(promoActual)){
                if(totalFactura>=rp.getMontoDesde() && totalFactura<=rp.getMontoHasta()){
                    totalCupones=rp.getCantidadCupones();
                    break;
                }
            }
        }
        for (int i = 0; i < totalCupones; i++) {
            Cupon c = new Cupon();
            c.setCliente(cli);
            c.setPromocion(promoActual);
            c.setImpreso(false);
            c.setFechaHoraGenerado(new GregorianCalendar());
            this.em.persist(c);
            lstCuponesGenerados.add(c);
        }
        this.em.getTransaction().commit();
        return lstCuponesGenerados;
    }

    public void marcarCuponImpreso(Cupon c) {
        this.em.getTransaction().begin();
        c.setImpreso(true);
        this.em.merge(c);
        this.em.getTransaction().commit();

    }

    public List<Cupon> getCuponesNoImpresosPromoActual(Cliente c) {
        PromocionJPAController promojpa = new PromocionJPAController(em);
        Promocion pro = promojpa.getPromocionActiva();
        TypedQuery<Cupon> q = this.em.createQuery("SELECT c FROM Cupon c WHERE c.cliente=:cli AND c.impreso=false AND c.promocion=:promo", Cupon.class);
        q.setParameter("promo", pro);
        q.setParameter("cli", c);
        List<Cupon> lstRes = q.getResultList();
        lstRes.stream().forEach((cupon) -> {
            this.em.refresh(cupon);
        });
        return lstRes;
    }

    public List<Cupon> getCuponesImpresosPromoActual(Cliente c) {
        PromocionJPAController promojpa = new PromocionJPAController(em);
        Promocion pro = promojpa.getPromocionActiva();
        TypedQuery<Cupon> q = this.em.createQuery("SELECT c FROM Cupon c WHERE c.cliente=:cli AND c.impreso=true AND c.promocion=:promo", Cupon.class);
        q.setParameter("promo", pro);
        q.setParameter("cli", c);
        List<Cupon> lstRes = q.getResultList();
        lstRes.stream().forEach((cupon) -> {
            this.em.refresh(cupon);
        });
        return lstRes;
    }

    public List<Cupon> getCuponesPorPromocion(Promocion p) {
        TypedQuery<Cupon> q = this.em.createQuery("SELECT c FROM Cupon c WHERE c.promocion=:promo", Cupon.class);
        q.setParameter("promo", p);
        List<Cupon> lstCupo = q.getResultList();
        lstCupo.stream().forEach((cu) -> {
            this.em.refresh(cu);
        });
        return lstCupo;

    }

    public Integer getCantidadCupones(Promocion p, Cliente c) {
        TypedQuery<Cupon> q = this.em.createQuery("SELECT c FROM Cupon c WHERE c.promocion=:promo AND c.cliente=:cli", Cupon.class);
        q.setParameter("promo", p);
        q.setParameter("cli", c);
        return q.getResultList().size();
    }

    public Integer getCantidadCuponesRango(Promocion p, Cliente c, Calendar desde, Calendar hasta) {
        TypedQuery<Cupon> q = this.em.createQuery("SELECT c FROM Cupon c WHERE c.promocion=:promo AND c.cliente=:cli AND c.fechaHoraGenerado>=:fdesde AND c.fechaHoraGenerado<=:fhasta", Cupon.class);
        q.setParameter("promo", p);
        q.setParameter("cli", c);
        q.setParameter("fdesde", desde);
        q.setParameter("fhasta", hasta);
        return q.getResultList().size();
    }

}
