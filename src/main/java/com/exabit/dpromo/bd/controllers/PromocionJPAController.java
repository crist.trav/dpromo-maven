/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exabit.dpromo.bd.controllers;

import com.exabit.dpromo.bd.entidades.Cliente;
import com.exabit.dpromo.bd.entidades.Cupon;
import com.exabit.dpromo.bd.entidades.Factura;
import com.exabit.dpromo.bd.entidades.Promocion;
import com.exabit.dpromo.bd.entidades.RangoPromocion;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

/**
 *
 * @author cristhian
 */
public class PromocionJPAController {

    private final EntityManager em;

    public PromocionJPAController(EntityManager em) {
        this.em = em;
    }

    public void modificarPromocion(Promocion p) {
        this.em.getTransaction().begin();
        for (RangoPromocion rpe : this.getRangosPromocion(p)) {
            this.em.remove(this.em.merge(rpe));
        }
        this.em.merge(p);
        this.em.getTransaction().commit();
    }

    public void modificarPromocion(Promocion p, List<RangoPromocion> lstr) {
        this.em.getTransaction().begin();
        p.setMontoCupon(null);
        this.em.merge(p);
        for (RangoPromocion rpe : this.getRangosPromocion(p)) {
            this.em.remove(this.em.merge(rpe));
        }
        for (RangoPromocion rp : lstr) {
            rp.setPromocion(p);
            this.em.persist(rp);
        }
        this.em.getTransaction().commit();
    }

    public Promocion getPromocionActiva() {
        TypedQuery<Promocion> q = this.em.createQuery("SELECT p FROM Promocion p WHERE p.activo=:activo", Promocion.class);
        q.setParameter("activo", true);
        List<Promocion> lstRes = new ArrayList<>();
        lstRes.addAll(q.getResultList());
        if (lstRes.isEmpty()) {
            return null;
        } else {
            this.em.refresh(lstRes.get(0));
            return lstRes.get(0);
        }
    }

    public void crearPromocionDefault() {
        TypedQuery<Promocion> q = this.em.createQuery("SELECT p FROM Promocion p", Promocion.class);
        List<Promocion> lstres = q.getResultList();
        if (lstres.isEmpty()) {
            Promocion promo = new Promocion();
            promo.setMontoCupon(100000);
            promo.setDescripcion("Por Defecto");
            promo.setFechaInicio(new GregorianCalendar());
            Calendar fechaFin = promo.getFechaInicio();
            fechaFin.add(Calendar.DAY_OF_MONTH, 45);
            promo.setFechaFin(fechaFin);
            promo.setActivo(true);
            promo.setPorRango(false);
            this.em.getTransaction().begin();
            this.em.persist(promo);
            this.em.getTransaction().commit();
        }
    }

    public List<Cliente> getClientesParticipantes(Promocion p) {
        //TypedQuery<Cliente> q=this.em.createQuery("SELECT c.cliente FROM Cupon c WHERE c.promocion=:promo GROUP BY c.cliente ORDER BY c.cliente.apellidos ASC",Cliente.class);
        TypedQuery<Cupon> qc = this.em.createQuery("SELECT c FROM Cupon c WHERE c.promocion.idPromocion=:promo ORDER BY c.cliente.apellidos ASC", Cupon.class);
        System.out.println("Promocion a obtener: " + p.getIdPromocion());
        //q.setParameter("promo", p);
        qc.setParameter("promo", p.getIdPromocion());

        //List<Cliente> lstCliPromo=q.getResultList();
        List<Cliente> lstCliPromo = new ArrayList<>();
        List<Cupon> lstCupPromo = qc.getResultList();

        for (Cupon cu : lstCupPromo) {

            boolean cliEncontrado = false;
            //System.out.println("ID Promo: "+cu.getPromocion().getIdPromocion());
            for (Cliente cli : lstCliPromo) {
                if (cli.getIdCliente().equals(cu.getCliente().getIdCliente())) {
                    cliEncontrado = true;
                }
                if (cli.getIdCliente().equals(973)) {

                }
            }
            if (!cliEncontrado) {
                lstCliPromo.add(cu.getCliente());
            }
        }

        System.out.println("total clientes participantes: " + lstCliPromo.size());
        lstCliPromo.stream().forEach((cl) -> {
            //System.out.println("Cli: "+cl.getIdCliente()+" "+cl.getApellidos());
            this.em.refresh(cl);
            //System.out.println(cl.getApellidos()+", "+cl.getNombres());
        });
        return lstCliPromo;
    }

    public List<Cliente> getClientesParticipantesRango(Promocion p, Calendar desde, Calendar hasta) {
        TypedQuery<Cliente> q = this.em.createQuery("SELECT c.cliente FROM Cupon c WHERE c.promocion=:promo AND c.fechaHoraGenerado>=:fdesde AND c.fechaHoraGenerado<=:fhasta GROUP BY c.cliente ORDER BY c.cliente.apellidos ASC", Cliente.class);
        System.out.println("Promocion a obtener: " + p.getIdPromocion());
        q.setParameter("promo", p);
        q.setParameter("fdesde", desde);
        q.setParameter("fhasta", hasta);
        List<Cliente> lstCliPromo = q.getResultList();
        lstCliPromo.stream().forEach((cl) -> {
            this.em.refresh(cl);
            //System.out.println(cl.getApellidos()+", "+cl.getNombres());
        });
        return lstCliPromo;
    }

    public List<Promocion> getTodasPromociones() {
        TypedQuery<Promocion> q = this.em.createQuery("SELECT p FROM Promocion p", Promocion.class);
        List<Promocion> lstPromo = q.getResultList();
        lstPromo.stream().forEach((pr) -> {
            this.em.refresh(pr);
        });
        return lstPromo;
    }

    public void registrarPromocion(Promocion p) {
        this.em.getTransaction().begin();
        this.em.persist(p);
        this.em.getTransaction().commit();
    }

    public void registrarPromocion(Promocion p, List<RangoPromocion> lstr) {
        this.em.getTransaction().begin();
        this.em.persist(p);
        for (RangoPromocion rp : lstr) {
            rp.setPromocion(p);
            this.em.persist(rp);
        }
        this.em.getTransaction().commit();
    }

    public void activarPromocion(Promocion p) {
        TypedQuery<Promocion> q = this.em.createQuery("SELECT p FROM Promocion p", Promocion.class);
        List<Promocion> lstPro = q.getResultList();
        this.em.getTransaction().begin();
        lstPro.stream().forEach((pro) -> {
            pro.setActivo(false);
            this.em.merge(pro);
        });
        p.setActivo(true);
        this.em.merge(p);
        this.em.getTransaction().commit();
    }

    public boolean registraCupones(Promocion p) {
        TypedQuery<Cupon> q = this.em.createQuery("SELECT c FROM Cupon c WHERE c.promocion=:pro", Cupon.class);
        q.setParameter("pro", p);
        return !q.getResultList().isEmpty();
    }

    public boolean registraFacturas(Promocion p) {
        TypedQuery<Factura> q = this.em.createQuery("SELECT f FROM Factura f WHERE f.promocion=:pro", Factura.class);
        q.setParameter("pro", p);
        return !q.getResultList().isEmpty();
    }

    public void eliminarPromocion(Promocion p) {
        this.em.getTransaction().begin();
        for (RangoPromocion rpe : this.getRangosPromocion(p)) {
            this.em.remove(this.em.merge(rpe));
        }
        this.em.remove(this.em.merge(p));
        this.em.getTransaction().commit();
    }

    public List<Promocion> buscarPromocion(String criterio) {
        TypedQuery<Promocion> q = this.em.createQuery("SELECT p FROM Promocion p WHERE LOWER(p.descripcion) LIKE :crit", Promocion.class);
        q.setParameter("crit", "%" + criterio.toLowerCase() + "%");
        List<Promocion> lstPromo = q.getResultList();
        lstPromo.stream().forEach((p) -> {
            this.em.refresh(p);
        });
        return lstPromo;
    }

    public List<RangoPromocion> getRangosPromocion(Promocion p) {
        TypedQuery<RangoPromocion> q = this.em.createQuery("SELECT rp FROM RangoPromocion rp WHERE rp.promocion=:promo", RangoPromocion.class);
        q.setParameter("promo", p);
        return q.getResultList();
    }
}
