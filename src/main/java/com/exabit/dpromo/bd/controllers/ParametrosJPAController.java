/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exabit.dpromo.bd.controllers;

import com.exabit.dpromo.bd.entidades.ParametroSistema;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

/**
 *
 * @author cristhian
 */
public class ParametrosJPAController {
    
    public static final String ESTADO_MENU_PRINCIPAL="EstadoMenuPrincipal";
    public static final String VENTANA_PRINCIPAL_MAX="VentanaPrincipalMax";
    
    public static final String IMPRESORA_TICKETS="IMPRESORA_TICKETS";
    public static final String ESCALA_TICKET="ESCALA_TICKET";
    public static final String TAMANIO_LETRA_CUPON = "TAMANIO_LETRA_CUPON";
    
    public static final String TIPO_BD="TIPO_BD";
    public static final String TIPO_BD_H2="TIPO_BD_H2";
    public static final String TIPO_BD_MYSQL="TIPO_BD_MYSQL";
    
    public static final String MYSQL_HOST="MYSQL_HOST";
    public static final String MYSQL_PORT="MYSQL_PORT";
    public static final String MYSQL_USER="MYSQL_USER";
    public static final String MYSQL_PASS="MYSQL_PASS";
    public static final String MYSQL_DB="MYSQL_DB";
    
    
    private final EntityManager em;

    public ParametrosJPAController(EntityManager em) {
        this.em = em;
    }
    
    public ParametrosJPAController(){
        this.em=Persistence.createEntityManagerFactory("DPromoPU").createEntityManager();
    }
    
    public void setParametroSistema(String clave, String valor){
        this.em.getTransaction().begin();
        ParametroSistema par=new ParametroSistema();
        par.setClave(clave);
        par.setValor(valor);
        this.em.merge(par);
        this.em.getTransaction().commit();
    }
    
    public String getParametroSistema(String clave){
        TypedQuery<ParametroSistema> q=this.em.createQuery("SELECT p FROM ParametroSistema p WHERE p.clave=:clave", ParametroSistema.class);
        q.setParameter("clave", clave);
        List<ParametroSistema> lstPars=q.getResultList();
        lstPars.stream().forEach((p)->{
            this.em.refresh(p);
        });
        if(lstPars.isEmpty()){
            return null;
        }else{
            return lstPars.get(0).getValor();
        }
    }
    
}
