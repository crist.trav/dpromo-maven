/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exabit.dpromo.bd.controllers;

import com.exabit.dpromo.bd.entidades.Cliente;
import com.exabit.dpromo.bd.entidades.Cupon;
import com.exabit.dpromo.bd.entidades.Factura;
import com.exabit.dpromo.bd.entidades.Promocion;
import com.exabit.dpromo.bd.entidades.Tienda;
import java.util.Calendar;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

/**
 *
 * @author cristhian
 */
public class FacturaJPAController {
    
    private final EntityManager em;

    public FacturaJPAController(EntityManager em) {
        this.em = em;
    }
    
    public void registrarFactura(Factura f){
        PromocionJPAController projpa=new PromocionJPAController(em);
        Promocion promoActiva=projpa.getPromocionActiva();
        if(promoActiva!=null){
            this.em.getTransaction().begin();
            this.em.persist(f);
            /*Integer cantCupones=f.getMontoFactura()/f.getMontoCupon();
            System.out.println("Cantidad de cupones: "+cantCupones);
            for(int i=0;i<cantCupones;i++){
                Cupon c=new Cupon();
                c.setFactura(f);
                this.em.persist(c);
            }*/
            this.em.getTransaction().commit();
        }else{
            System.err.println("No hay promocion Activa");
        }
    }
    
    public List<Cupon> getCuponesFactura(Factura f){
        System.out.println("Factura: "+f);
        TypedQuery<Cupon> q=this.em.createQuery("SELECT c FROM Cupon c WHERE c.factura=:fact", Cupon.class);
        q.setParameter("fact", f);
        List<Cupon> lstRes=q.getResultList();
        lstRes.stream().forEach((cu)->{
            System.out.println("Cupon: "+cu);
            this.em.refresh(cu);
        });
        return lstRes;
    }
    
    public List<Factura> getFacturasCliente(Cliente c){
        TypedQuery<Factura> q=this.em.createQuery("SELECT f FROM Factura f WHERE f.cliente=:cli", Factura.class);
        q.setParameter("cli", c);
        List<Factura> lstRes=q.getResultList();
        lstRes.stream().forEach((f)->{
            this.em.refresh(f);
        });
        return lstRes;
    }
    
    public List<Factura> getFacturasClienteSinProcesar(Cliente c){
        TypedQuery<Factura> q=this.em.createQuery("SELECT f FROM Factura f WHERE f.cliente=:cli AND f.procesado=false", Factura.class);
        q.setParameter("cli", c);
        List<Factura> lstRes=q.getResultList();
        lstRes.stream().forEach((f)->{
            this.em.refresh(f);
        });
        return lstRes;
    }
    public List<Factura> getFacturasClienteProcesadas(Cliente c){
        TypedQuery<Factura> q=this.em.createQuery("SELECT f FROM Factura f WHERE f.cliente=:cli AND f.procesado=true", Factura.class);
        q.setParameter("cli", c);
        List<Factura> lstRes=q.getResultList();
        lstRes.stream().forEach((f)->{
            this.em.refresh(f);
        });
        return lstRes;
    }
    
    public void eliminarFactura(Factura f){
        this.em.getTransaction().begin();
        this.em.remove(this.em.merge(f));
        this.em.getTransaction().commit();
    }
    
    public List<Factura> getFacturasPorPromocionCliente(Promocion p, Cliente c){
        TypedQuery<Factura> q=this.em.createQuery("SELECT f FROM Factura f WHERE f.cliente=:cli AND f.promocion=:promo AND f.procesado=true",Factura.class);
        q.setParameter("promo", p);
        q.setParameter("cli", c);
        List<Factura> lstFact=q.getResultList();
        lstFact.stream().forEach((fa)->{
            this.em.refresh(fa);
        });
        return lstFact;
    }
    
    public Integer getTotalFacturas(Promocion p, Cliente c){
        Integer totalFacturas=0;
        TypedQuery<Factura> q=this.em.createQuery("SELECT f FROM Factura f WHERE f.cliente=:cli AND f.promocion=:promo", Factura.class);
        q.setParameter("cli", c);
        q.setParameter("promo", p);
        List<Factura> lstFact=q.getResultList();
        for(Factura fa:lstFact){
            this.em.refresh(fa);
            totalFacturas=totalFacturas+fa.getMontoFactura();
        }
        return totalFacturas;
    }
    public Integer getTotalFacturasRango(Promocion p, Cliente c, Calendar desde, Calendar hasta){
        Integer totalFacturas=0;
        TypedQuery<Factura> q=this.em.createQuery("SELECT f FROM Factura f WHERE f.cliente=:cli AND f.promocion=:promo AND f.fechaHoraRegistro>=:fdesde AND f.fechaHoraRegistro<=:fhasta", Factura.class);
        q.setParameter("cli", c);
        q.setParameter("promo", p);
        q.setParameter("fdesde", desde);
        q.setParameter("fhasta", hasta);
        List<Factura> lstFact=q.getResultList();
        for(Factura fa:lstFact){
            this.em.refresh(fa);
            totalFacturas=totalFacturas+fa.getMontoFactura();
        }
        return totalFacturas;
    }
    
    public Integer getTotalFacturacionTiendaCliente(Promocion p, Tienda t, Cliente c){
        TypedQuery<Factura> q=this.em.createQuery("SELECT f FROM Factura f WHERE f.promocion=:promo AND f.cliente=:cli AND f.tienda=:tie", Factura.class);
        q.setParameter("promo", p);
        q.setParameter("cli", c);
        q.setParameter("tie", t);
        Integer total=0;
        List<Factura> lstFact=q.getResultList();
        for(Factura f:lstFact){
            //if(c.getIdCliente().equals(973)){
                //System.out.println("ID Cli:"+f.getCliente().getIdCliente()+" Monto factura: "+f.getMontoFactura());
            //}
            this.em.refresh(f);
            total=total+f.getMontoFactura();
        }
        return total;
    }
    public Integer getTotalFacturacionTiendaClienteRango(Promocion p, Tienda t, Cliente c, Calendar fdesde, Calendar fhasta){
        TypedQuery<Factura> q=this.em.createQuery("SELECT f FROM Factura f WHERE f.fechaHoraRegistro>=:fdesde AND f.fechaHoraRegistro<=:fhasta AND f.promocion=:promo AND f.cliente=:cli AND f.tienda=:tie", Factura.class);
        q.setParameter("promo", p);
        q.setParameter("cli", c);
        q.setParameter("tie", t);
        q.setParameter("fdesde", fdesde);
        q.setParameter("fhasta", fhasta);
        Integer total=0;
        List<Factura> lstFact=q.getResultList();
        for(Factura f:lstFact){
            this.em.refresh(f);
            total=total+f.getMontoFactura();
        }
        return total;
    }
    
}
