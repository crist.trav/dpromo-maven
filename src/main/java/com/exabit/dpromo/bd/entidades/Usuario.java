package com.exabit.dpromo.bd.entidades;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.TableGenerator;

@Entity
public class Usuario {

    @Id
    @GeneratedValue(generator = "idUsuarioSeq", strategy = GenerationType.TABLE)
    @TableGenerator(name = "idUsuarioSeq", table = "Seq_idUsuario", allocationSize = 1, initialValue = 10)
    private Integer idUsuario;

    @Column(length = 100)
    @Basic
    private String nombres;

    @Column(length = 100)
    @Basic
    private String apellidos;

    @Basic
    private Integer ci;

    @Basic
    private String pass;

    public Integer getIdUsuario() {
        return this.idUsuario;
    }

    public void setIdUsuario(Integer idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getNombres() {
        return this.nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidos() {
        return this.apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public Integer getCi() {
        return this.ci;
    }

    public void setCi(Integer ci) {
        this.ci = ci;
    }

    public String getPass() {
        return this.pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

}