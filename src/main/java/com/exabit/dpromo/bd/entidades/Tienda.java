package com.exabit.dpromo.bd.entidades;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.TableGenerator;

@Entity
public class Tienda {

    @Id
    @GeneratedValue(generator = "idTianda_seq", strategy = GenerationType.TABLE)
    @TableGenerator(name = "idTianda_seq", table = "Seq_idTienda", allocationSize = 1, initialValue = 10)
    private Integer idTienda;

    @Basic
    private String nombre;

    @Basic
    private Boolean activo;

    public Integer getIdTienda() {
        return this.idTienda;
    }

    public void setIdTienda(Integer idTienda) {
        this.idTienda = idTienda;
    }

    public String getNombre() {
        return this.nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Boolean isActivo() {
        return this.activo;
    }

    public void setActivo(Boolean activo) {
        this.activo = activo;
    }

}