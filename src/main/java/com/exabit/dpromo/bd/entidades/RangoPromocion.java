package com.exabit.dpromo.bd.entidades;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

/**
 * @author traver
 */
@Entity
public class RangoPromocion {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long idRangoPromocion;

    @Basic(optional = false)
    private int montoDesde = -1;

    @Basic(optional = false)
    private int montoHasta = -1;

    @Basic(optional = false)
    private int cantidadCupones = 0;

    @ManyToOne(optional = false, targetEntity = Promocion.class)
    private Promocion promocion;

    public Long getIdRangoPromocion() {
        return this.idRangoPromocion;
    }

    public void setIdRangoPromocion(Long idRangoPromocion) {
        this.idRangoPromocion = idRangoPromocion;
    }

    public int getMontoDesde() {
        return this.montoDesde;
    }

    public void setMontoDesde(int montoDesde) {
        this.montoDesde = montoDesde;
    }

    public int getMontoHasta() {
        return this.montoHasta;
    }

    public void setMontoHasta(int montoHasta) {
        this.montoHasta = montoHasta;
    }

    public int getCantidadCupones() {
        return this.cantidadCupones;
    }

    public void setCantidadCupones(int cantidadCupones) {
        this.cantidadCupones = cantidadCupones;
    }

    public Promocion getPromocion() {
        return this.promocion;
    }

    public void setPromocion(Promocion promocion) {
        this.promocion = promocion;
    }

}