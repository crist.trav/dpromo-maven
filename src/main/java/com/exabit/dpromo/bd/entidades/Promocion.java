package com.exabit.dpromo.bd.entidades;

import java.util.Calendar;
import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class Promocion {

    @Id
    @GeneratedValue(generator = "idPromocionSeq", strategy = GenerationType.TABLE)
    @TableGenerator(name = "idPromocionSeq", table = "Seq_idPromocion", allocationSize = 1, initialValue = 5)
    private Integer idPromocion;

    @Basic
    private String descripcion;

    @Basic
    @Temporal(TemporalType.DATE)
    private Calendar fechaInicio;

    @Basic
    @Temporal(TemporalType.DATE)
    private Calendar fechaFin;

    @Basic
    private Integer montoCupon;

    @Basic
    private Boolean activo;

    @Basic(optional = false)
    private boolean porRango = false;

    public Integer getIdPromocion() {
        return this.idPromocion;
    }

    public void setIdPromocion(Integer idPromocion) {
        this.idPromocion = idPromocion;
    }

    public String getDescripcion() {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Calendar getFechaInicio() {
        return this.fechaInicio;
    }

    public void setFechaInicio(Calendar fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public Calendar getFechaFin() {
        return this.fechaFin;
    }

    public void setFechaFin(Calendar fechaFin) {
        this.fechaFin = fechaFin;
    }

    public Integer getMontoCupon() {
        return this.montoCupon;
    }

    public void setMontoCupon(Integer montoCupon) {
        this.montoCupon = montoCupon;
    }

    public Boolean isActivo() {
        return this.activo;
    }

    public void setActivo(Boolean activo) {
        this.activo = activo;
    }

    public boolean isPorRango() {
        return this.porRango;
    }

    public void setPorRango(boolean porRango) {
        this.porRango = porRango;
    }

}