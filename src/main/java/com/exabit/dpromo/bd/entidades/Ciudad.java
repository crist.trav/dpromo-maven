package com.exabit.dpromo.bd.entidades;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.TableGenerator;

@Entity
public class Ciudad {

    @Id
    @GeneratedValue(generator = "idCiudadSeq", strategy = GenerationType.TABLE)
    @TableGenerator(name = "idCiudadSeq", table = "Seq_idCiudad", allocationSize = 1, initialValue = 50)
    private Integer idCiudad;

    @Basic
    private String nombre;

    @ManyToOne(targetEntity = Departamento.class)
    private Departamento departamento;

    public Integer getIdCiudad() {
        return this.idCiudad;
    }

    public void setIdCiudad(Integer idCiudad) {
        this.idCiudad = idCiudad;
    }

    public String getNombre() {
        return this.nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Departamento getDepartamento() {
        return this.departamento;
    }

    public void setDepartamento(Departamento departamento) {
        this.departamento = departamento;
    }

}