package com.exabit.dpromo.bd.entidades;

import java.util.Calendar;
import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class Factura {

    @Id
    @GeneratedValue(generator = "idFacturaSeq", strategy = GenerationType.TABLE)
    @TableGenerator(name = "idFacturaSeq", table = "Seq_idFactura", allocationSize = 1, initialValue = 1)
    private Integer idFactura;

    @Basic
    private Integer montoFactura;

    @Basic
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar fechaHoraRegistro;

    @Basic
    private Boolean procesado;

    @ManyToOne(targetEntity = Cliente.class)
    private Cliente cliente;

    @ManyToOne(targetEntity = Tienda.class)
    private Tienda tienda;

    @ManyToOne(targetEntity = Promocion.class)
    private Promocion promocion;

    public Integer getIdFactura() {
        return this.idFactura;
    }

    public void setIdFactura(Integer idFactura) {
        this.idFactura = idFactura;
    }

    public Integer getMontoFactura() {
        return this.montoFactura;
    }

    public void setMontoFactura(Integer montoFactura) {
        this.montoFactura = montoFactura;
    }

    public Calendar getFechaHoraRegistro() {
        return this.fechaHoraRegistro;
    }

    public void setFechaHoraRegistro(Calendar fechaHoraRegistro) {
        this.fechaHoraRegistro = fechaHoraRegistro;
    }

    public Boolean isProcesado() {
        return this.procesado;
    }

    public void setProcesado(Boolean procesado) {
        this.procesado = procesado;
    }

    public Cliente getCliente() {
        return this.cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Tienda getTienda() {
        return this.tienda;
    }

    public void setTienda(Tienda tienda) {
        this.tienda = tienda;
    }

    public Promocion getPromocion() {
        return this.promocion;
    }

    public void setPromocion(Promocion promocion) {
        this.promocion = promocion;
    }

}