package com.exabit.dpromo.bd.entidades;

import java.util.Calendar;
import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class Cupon {

    @Id
    @GeneratedValue(generator = "idCuponSeq", strategy = GenerationType.TABLE)
    @TableGenerator(name = "idCuponSeq", table = "Seq_idCupon", allocationSize = 1, initialValue = 1)
    private Integer idCupon;

    @Basic
    private Boolean impreso;

    @Basic
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar fechaHoraGenerado;

    @ManyToOne(targetEntity = Cliente.class)
    private Cliente cliente;

    @ManyToOne(targetEntity = Promocion.class)
    private Promocion promocion;

    public Integer getIdCupon() {
        return this.idCupon;
    }

    public void setIdCupon(Integer idCupon) {
        this.idCupon = idCupon;
    }

    public Boolean isImpreso() {
        return this.impreso;
    }

    public void setImpreso(Boolean impreso) {
        this.impreso = impreso;
    }

    public Calendar getFechaHoraGenerado() {
        return this.fechaHoraGenerado;
    }

    public void setFechaHoraGenerado(Calendar fechaHoraGenerado) {
        this.fechaHoraGenerado = fechaHoraGenerado;
    }

    public Cliente getCliente() {
        return this.cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Promocion getPromocion() {
        return this.promocion;
    }

    public void setPromocion(Promocion promocion) {
        this.promocion = promocion;
    }

}