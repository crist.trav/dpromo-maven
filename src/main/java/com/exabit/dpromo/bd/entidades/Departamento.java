package com.exabit.dpromo.bd.entidades;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.TableGenerator;

@Entity
public class Departamento {

    @Id
    @GeneratedValue(generator = "idDepartamentoSeq", strategy = GenerationType.TABLE)
    @TableGenerator(name = "idDepartamentoSeq", table = "Seq_idDepartamento", allocationSize = 1, initialValue = 50)
    private Integer idDepartamento;

    @Column(length = 100)
    @Basic
    private String nombre;

    public Integer getIdDepartamento() {
        return this.idDepartamento;
    }

    public void setIdDepartamento(Integer idDepartamento) {
        this.idDepartamento = idDepartamento;
    }

    public String getNombre() {
        return this.nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

}