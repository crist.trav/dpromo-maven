package com.exabit.dpromo.bd.entidades;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.TableGenerator;

@Entity
public class Cliente {

    @Id
    @GeneratedValue(generator = "idClienteSeq", strategy = GenerationType.TABLE)
    @TableGenerator(name = "idClienteSeq", table = "Seq_idCliente", allocationSize = 1, initialValue = 10)
    private Integer idCliente;

    @Column(length = 100)
    @Basic
    private String nombres;

    @Column(length = 100)
    @Basic
    private String apellidos;

    @Basic
    private String direccion;

    @Basic
    private String telefono;

    @Basic
    private String celular;

    @Basic
    private String email;

    @Column(unique = true, length = 50)
    @Basic
    private Integer ci;

    @ManyToOne(targetEntity = Ciudad.class)
    private Ciudad ciudad;

    public Integer getIdCliente() {
        return this.idCliente;
    }

    public void setIdCliente(Integer idCliente) {
        this.idCliente = idCliente;
    }

    public String getNombres() {
        return this.nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidos() {
        return this.apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getDireccion() {
        return this.direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getTelefono() {
        return this.telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getCelular() {
        return this.celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getCi() {
        return this.ci;
    }

    public void setCi(Integer ci) {
        this.ci = ci;
    }

    public Ciudad getCiudad() {
        return this.ciudad;
    }

    public void setCiudad(Ciudad ciudad) {
        this.ciudad = ciudad;
    }

}