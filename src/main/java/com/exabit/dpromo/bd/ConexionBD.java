/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exabit.dpromo.bd;

import com.exabit.dpromo.bd.controllers.ParametrosJPAController;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author cristhian
 */
public class ConexionBD {

    private static final Logger LOG = Logger.getLogger(ConexionBD.class.getName());

    public static EntityManagerFactory EMF;

    static {
        ParametrosJPAController pjpa = new ParametrosJPAController();
        String tipobd = pjpa.getParametroSistema(ParametrosJPAController.TIPO_BD);
        if (tipobd != null) {
            if (tipobd.equals(ParametrosJPAController.TIPO_BD_MYSQL)) {
                try {
                    String host = pjpa.getParametroSistema(ParametrosJPAController.MYSQL_HOST);
                    String port = pjpa.getParametroSistema(ParametrosJPAController.MYSQL_PORT);
                    String bd = pjpa.getParametroSistema(ParametrosJPAController.MYSQL_DB);

                    Map<String, String> pars = new HashMap<>();
                    pars.put("javax.persistence.jdbc.url", "jdbc:mysql://" + host + ":" + port + "/" + bd + "?zeroDateTimeBehavior=convertToNull");
                    pars.put("javax.persistence.jdbc.user", pjpa.getParametroSistema(ParametrosJPAController.MYSQL_USER));
                    pars.put("javax.persistence.jdbc.password", pjpa.getParametroSistema(ParametrosJPAController.MYSQL_PASS));
                    EMF = Persistence.createEntityManagerFactory("DPromoPU-MySQL", pars);
                } catch (Exception ex) {
                    LOG.log(Level.SEVERE, "Error al conectar a base de datos mysql", ex);
                    Alert dlg = new Alert(AlertType.ERROR);
                    dlg.setTitle("Error al conectar a la Base de Datos");
                    dlg.setContentText("No se puede conectar a la base de datos central. Se inicia en modo local");
                    dlg.showAndWait();
                    EMF = Persistence.createEntityManagerFactory("DPromoPU");
                }
            } else {
                EMF = Persistence.createEntityManagerFactory("DPromoPU");
            }
            //EMFh2=Persistence.createEntityManagerFactory("DPromoPU");
        } else {
            EMF = Persistence.createEntityManagerFactory("DPromoPU");
        }
    }

}
