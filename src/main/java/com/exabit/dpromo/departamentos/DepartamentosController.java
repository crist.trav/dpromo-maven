/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exabit.dpromo.departamentos;

import com.exabit.dpromo.MainApp;
import com.exabit.dpromo.PantallaPrincipalController;
import com.exabit.dpromo.bd.ConexionBD;
import com.exabit.dpromo.bd.controllers.DepartamentoJPAController;
import com.exabit.dpromo.bd.entidades.Departamento;
import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Accordion;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.TitledPane;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

/**
 * FXML Controller class
 *
 * @author cristhian
 */
public class DepartamentosController implements Initializable {

    @FXML
    private Button btnRegistrar;
    @FXML
    private Button btnNuevo;
    @FXML
    private Button btnEliminar;
    @FXML
    private ImageView imgvBuscar;
    @FXML
    private TextField txfBuscarDepartamento;
    @FXML
    private TableView<Departamento> tblDepartamentos;
    @FXML
    private TableColumn<Departamento, Integer> colCodigoDepartamento;
    @FXML
    private TableColumn<Departamento, String> colNombreDepartamento;
    @FXML
    private TextField txfCodigoDepartamento;
    @FXML
    private TextField txfNombreDepartamento;
    @FXML
    private Accordion accDatosDep;
    @FXML
    private TitledPane tpaneDatos;
    
    private final ObservableList<Departamento> lstDepartamentos=FXCollections.observableArrayList();
    private final ObjectProperty<Departamento> departamentoSeleccionado=new SimpleObjectProperty<>();
    
    private static final int OPERACION_REGISTRAR=1;
    private static final int OPERACION_MODIFICAR=2;    
    
    int operacionActual=OPERACION_REGISTRAR;
    
    private final ChangeListener<Departamento> chLnrSeleccionDepartamento=(ObservableValue<? extends Departamento> obs, Departamento oldValue, Departamento newValue)->{
        if(newValue!=null){
            this.departamentoSeleccionado.setValue(newValue);
        }
    };
    private final ChangeListener<Departamento> chLnrDepartamentoSeleccionado=(ObservableValue<? extends Departamento> obs, Departamento oldValue, Departamento newValue)->{
        if(newValue!=null){
            this.operacionActual=DepartamentosController.OPERACION_MODIFICAR;
            this.btnRegistrar.setText("Modificar");
            Image imgOperacion=new Image(getClass().getResourceAsStream("/iconos/x16/edit.png"));
            this.btnRegistrar.setGraphic(new ImageView(imgOperacion));
            this.txfCodigoDepartamento.setText(newValue.getIdDepartamento().toString());
            this.txfNombreDepartamento.setText(newValue.getNombre());
        }else{
            this.operacionActual=DepartamentosController.OPERACION_REGISTRAR;
            this.btnRegistrar.setText("Registrar");
            Image imgOperacion=new Image(getClass().getResourceAsStream("/iconos/x16/save.png"));
            this.btnRegistrar.setGraphic(new ImageView(imgOperacion));
            this.txfCodigoDepartamento.setText("");
            this.txfNombreDepartamento.setText("");
        }
    };
    
    private final ChangeListener<String> chLnrBusquedaDep=(ObservableValue<? extends String> obs, String oldValue, String newValue)->{
        DepartamentoJPAController djpa=new DepartamentoJPAController(ConexionBD.EMF.createEntityManager());
        this.lstDepartamentos.clear();
        if(newValue.isEmpty()){
            this.lstDepartamentos.addAll(djpa.getTodosDepartamentos());
        }else{
            this.lstDepartamentos.addAll(djpa.buscarDepartamento(newValue));
        }
    };

    /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        this.cargarIconos();
        this.inicializarTablaDepartamentos();
        this.tblDepartamentos.getSelectionModel().selectedItemProperty().addListener(this.chLnrSeleccionDepartamento);
        this.departamentoSeleccionado.addListener(this.chLnrDepartamentoSeleccionado);
        this.txfBuscarDepartamento.textProperty().addListener(this.chLnrBusquedaDep);
        this.accDatosDep.setExpandedPane(this.tpaneDatos);
        DepartamentoJPAController djpa=new DepartamentoJPAController(ConexionBD.EMF.createEntityManager());
        this.lstDepartamentos.addAll(djpa.getTodosDepartamentos());
    }    
    
    private boolean camposValidos(){
        boolean cmpValidos=false;
        if(!this.txfNombreDepartamento.getText().isEmpty()){
            cmpValidos=true;
        }else{
            Alert dlg=new Alert(AlertType.WARNING);
            dlg.setTitle("Registrar Departamento");
            dlg.setContentText("El campo 'Nombre' está en blanco");
            dlg.showAndWait();
        }
        return cmpValidos;
    }
    
    private void cargarIconos(){
        Image imgRegistrar=new Image(getClass().getResourceAsStream("/iconos/x16/save.png"));
        this.btnRegistrar.setGraphic(new ImageView(imgRegistrar));
        
        Image imgNuevo=new Image(getClass().getResourceAsStream("/iconos/x16/document.png"));
        this.btnNuevo.setGraphic(new ImageView(imgNuevo));
        
        Image imgEliminar=new Image(getClass().getResourceAsStream("/iconos/x16/delete.png"));
        this.btnEliminar.setGraphic(new ImageView(imgEliminar));
        
        Image imgBuscar=new Image(getClass().getResourceAsStream("/iconos/x16/search.png"));
        this.imgvBuscar.setImage(imgBuscar);
    }
    
    private void inicializarTablaDepartamentos(){
        this.tblDepartamentos.setItems(lstDepartamentos);
        this.colCodigoDepartamento.setCellValueFactory(new PropertyValueFactory<>("idDepartamento"));
        this.colNombreDepartamento.setCellValueFactory(new PropertyValueFactory<>("nombre"));
    }
    
    private void registrarDepartamento(){
        if(this.camposValidos()){
            DepartamentoJPAController djpa=new DepartamentoJPAController(ConexionBD.EMF.createEntityManager());
            Departamento d=new Departamento();
            d.setNombre(this.txfNombreDepartamento.getText());
            djpa.registrarDepartamento(d);
            MainApp.mostrarMensaje("Departamento '"+d.getIdDepartamento()+"-"+d.getNombre()+"' registrado", PantallaPrincipalController.TIPO_MENSAJE_INFO);
            this.lstDepartamentos.add(d);
        }
    }
    
    private void modificarDepartamento(){
        if(this.camposValidos()){
            DepartamentoJPAController djpa=new DepartamentoJPAController(ConexionBD.EMF.createEntityManager());
            int indiceDep=this.lstDepartamentos.indexOf(this.departamentoSeleccionado.getValue());
            Departamento d=new Departamento();
            d.setIdDepartamento(this.departamentoSeleccionado.getValue().getIdDepartamento());
            d.setNombre(this.txfNombreDepartamento.getText());
            djpa.modificarDepartamento(d);
            this.lstDepartamentos.remove(indiceDep);
            this.lstDepartamentos.add(indiceDep,d);
            this.tblDepartamentos.getSelectionModel().select(d);
            MainApp.mostrarMensaje("Departamento '"+d.getIdDepartamento()+"-"+d.getNombre()+"' modificado", PantallaPrincipalController.TIPO_MENSAJE_INFO);
        }
    }
    
    private void eliminarDepartamento(){
        if(this.tblDepartamentos.getSelectionModel().isEmpty()){
            Alert dlg=new Alert(AlertType.INFORMATION);
            dlg.setTitle("Eliminar Departamento");
            dlg.setContentText("Debe seleccionar por lo menos un Departamento");
            dlg.showAndWait();
        }else{
            Alert conf=new Alert(AlertType.CONFIRMATION);
            conf.setTitle("Eliminar Departamentos");
            conf.setContentText("Se eliminarán los departamentos seleccionados.\n¿Desea continuar?");
            Optional<ButtonType> seleccion=conf.showAndWait();
            if(seleccion.get().equals(ButtonType.OK)){
                DepartamentoJPAController djpa=new DepartamentoJPAController(ConexionBD.EMF.createEntityManager());
                for(Departamento d:this.tblDepartamentos.getSelectionModel().getSelectedItems()){
                    if(djpa.registraCiudades(d)){
                        Alert error=new Alert(AlertType.ERROR);
                        error.setTitle("Eliminar Departamento");
                        error.setContentText("No puede eliminarse el departamento '"+d.getNombre()+"'.\nTiene ciudades registradas.");
                        error.showAndWait();
                    }else{
                        djpa.eliminarDepartamento(d);
                        this.lstDepartamentos.remove(d);
                        MainApp.mostrarMensaje("Departamentos eliminados", PantallaPrincipalController.TIPO_MENSAJE_INFO);
                    }
                }
            }
        }
    }

    @FXML
    private void onActionBtnRegistrar(ActionEvent event) {
        if(this.operacionActual==DepartamentosController.OPERACION_REGISTRAR){
            this.registrarDepartamento();
        }else if(this.operacionActual==DepartamentosController.OPERACION_MODIFICAR){
            this.modificarDepartamento();
        }
        
    }
    

    @FXML
    private void onActionBtnNuevo(ActionEvent event) {
        this.departamentoSeleccionado.setValue(null);
    }

    @FXML
    private void onActionBtnEliminar(ActionEvent event) {
        this.eliminarDepartamento();
    }
    
}
