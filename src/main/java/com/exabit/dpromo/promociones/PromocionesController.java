/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exabit.dpromo.promociones;

import com.exabit.dpromo.bd.ConexionBD;
import com.exabit.dpromo.bd.controllers.PromocionJPAController;
import com.exabit.dpromo.bd.entidades.Promocion;
import com.exabit.dpromo.bd.entidades.RangoPromocion;
import java.net.URL;
import java.text.DecimalFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.control.Accordion;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.TitledPane;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.VBox;

/**
 * FXML Controller class
 *
 * @author cristhian
 */
public class PromocionesController implements Initializable {

    @FXML
    private Button btnRegistrarPromo;
    @FXML
    private Button btnEliminarPromo;
    @FXML
    private Button btnNuevaPromo;
    @FXML
    private TableView<Promocion> tblPromociones;
    @FXML
    private TableColumn<Promocion, Integer> colCodigoPromo;
    @FXML
    private TableColumn<Promocion, String> colDescripcionPromo;
    @FXML
    private TableColumn<Promocion, Calendar> colInicioPromo;
    @FXML
    private TableColumn<Promocion, Calendar> colFinPromo;
    @FXML
    private TableColumn<Promocion, Boolean> colPromoVigente;
    @FXML
    private TextField txfCodigoPromo;
    @FXML
    private Accordion accDatosPromo;
    @FXML
    private TitledPane tpaneDatosPromo;
    @FXML
    private TextField txfDescripcionPromo;
    @FXML
    private DatePicker dpkFechaInicio;
    @FXML
    private DatePicker dpkFechaFin;
    @FXML
    private TextField txfPrecioCupon;
    @FXML
    private VBox vboxDatosPromo;
    @FXML
    private ImageView imgvBuscar;
    @FXML
    private TextField txfBuscarPromo;
    @FXML
    private TitledPane tpRangos;
    @FXML
    private Button btnAgregarRango;
    @FXML
    private TableView<RangoPromoModelo> tblRangos;
    @FXML
    private TableColumn<RangoPromoModelo, Number> colDesde;
    @FXML
    private TableColumn<RangoPromoModelo, Number> colHasta;
    @FXML
    private TableColumn<RangoPromoModelo, Number> colNroCupones;
    @FXML
    private TableColumn<Promocion, Boolean> colGenerarPor;
    @FXML
    private TableColumn colElim;
    @FXML
    private Label lblPrecioCupon;
    @FXML
    private TextField txfDesdeRango;
    @FXML
    private TextField txfHastaRango;
    @FXML
    private TextField txfCantCupones;
    @FXML
    private RadioButton rdbMontoUnico;
    @FXML
    private ToggleGroup tgbTipo;
    @FXML
    private RadioButton rdbRango;

    private static final int OPERACION_REGISTRAR = 1;
    private static final int OPERACION_MODIFICAR = 2;
    private int operacionActual = OPERACION_REGISTRAR;
    private final ObservableList<Promocion> lstPromociones = FXCollections.observableArrayList();
    private final ObjectProperty<Promocion> promoActual = new SimpleObjectProperty<>();

    private final ChangeListener<Promocion> chLnrPromoActual = (ObservableValue<? extends Promocion> obs, Promocion oldValue, Promocion newValue) -> {
        if (newValue != null) {
            PromocionJPAController pjpa = new PromocionJPAController(ConexionBD.EMF.createEntityManager());
            this.operacionActual = PromocionesController.OPERACION_MODIFICAR;
            this.btnRegistrarPromo.setText("Modificar");
            Image imgRegistrar = new Image(getClass().getResourceAsStream("/iconos/x16/edit.png"));
            this.btnRegistrarPromo.setGraphic(new ImageView(imgRegistrar));
            this.txfCodigoPromo.setText(newValue.getIdPromocion().toString());
            this.txfDescripcionPromo.setText(newValue.getDescripcion());

            if (newValue.getFechaInicio() != null) {
                int anio = newValue.getFechaInicio().get(Calendar.YEAR);
                int mes = newValue.getFechaInicio().get(Calendar.MONTH) + 1;
                int dia = newValue.getFechaInicio().get(Calendar.DAY_OF_MONTH);
                LocalDate ldi = LocalDate.of(anio, mes, dia);
                this.dpkFechaInicio.setValue(ldi);
            }
            if (newValue.getFechaFin() != null) {
                int anio = newValue.getFechaFin().get(Calendar.YEAR);
                int mes = newValue.getFechaFin().get(Calendar.MONTH) + 1;
                int dia = newValue.getFechaFin().get(Calendar.DAY_OF_MONTH);
                LocalDate ldf = LocalDate.of(anio, mes, dia);
                this.dpkFechaFin.setValue(ldf);
            }
            if (newValue.isPorRango()) {
                this.txfPrecioCupon.clear();
                this.rdbRango.setSelected(true);
                this.tblRangos.getItems().clear();
                for (RangoPromocion rp : pjpa.getRangosPromocion(newValue)) {
                    this.tblRangos.getItems().add(new RangoPromoModelo(rp));
                }
            } else {
                this.tblRangos.getItems().clear();
                this.rdbMontoUnico.setSelected(true);
                DecimalFormat df=new DecimalFormat("#,###,###,###");
                this.txfPrecioCupon.setText(df.format(newValue.getMontoCupon()));
            }
        } else {
            this.operacionActual = PromocionesController.OPERACION_REGISTRAR;
            this.btnRegistrarPromo.setText("Registrar");
            Image imgRegistrar = new Image(getClass().getResourceAsStream("/iconos/x16/save.png"));
            this.btnRegistrarPromo.setGraphic(new ImageView(imgRegistrar));
            this.txfCodigoPromo.setText("");
            this.txfDescripcionPromo.setText("");
            this.txfPrecioCupon.setText("0");
            this.dpkFechaInicio.setValue(LocalDate.now());
            this.dpkFechaFin.setValue(LocalDate.now());
            this.tblRangos.getItems().clear();
            this.txfDesdeRango.clear();
            this.txfHastaRango.clear();
            this.txfCantCupones.clear();
        }
    };
    private final ChangeListener<Promocion> chLnrSeleccionPromo = (ObservableValue<? extends Promocion> obs, Promocion oldValue, Promocion newValue) -> {
//        if (newValue != null) {
            this.promoActual.setValue(newValue);
//        }
    };

    private final ChangeListener<String> chLnrBusqueda = (ObservableValue<? extends String> obs, String oldValue, String newValue) -> {
        this.lstPromociones.clear();
        PromocionJPAController promojpa = new PromocionJPAController(ConexionBD.EMF.createEntityManager());
        if (newValue.isEmpty()) {
            this.lstPromociones.addAll(promojpa.getTodasPromociones());
        } else {
            this.lstPromociones.addAll(promojpa.buscarPromocion(newValue));
        }
    };

    /**
     * Initializes the controller class.
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        this.accDatosPromo.getPanes().remove(this.tpRangos);
        this.inicializarTablaPromociones();
        this.cargarIconos();
        this.dpkFechaInicio.prefWidthProperty().bind(this.vboxDatosPromo.widthProperty());
        this.dpkFechaFin.prefWidthProperty().bind(this.vboxDatosPromo.widthProperty());
        PromocionJPAController promojpa = new PromocionJPAController(ConexionBD.EMF.createEntityManager());
        this.lstPromociones.addAll(promojpa.getTodasPromociones());
        this.accDatosPromo.setExpandedPane(this.tpaneDatosPromo);
        this.tblPromociones.getSelectionModel().selectedItemProperty().addListener(this.chLnrSeleccionPromo);
        this.promoActual.addListener(this.chLnrPromoActual);
        this.txfBuscarPromo.textProperty().addListener(this.chLnrBusqueda);
        this.dpkFechaFin.setValue(LocalDate.now());
        this.dpkFechaInicio.setValue(LocalDate.now());
        this.inicializarTablaRangos();
        this.txfDesdeRango.textProperty().addListener((ObservableValue<? extends String> obs, String o, String n) -> {
            if (!n.isEmpty()) {
                DecimalFormat df = new DecimalFormat("#,###,###,###");
                String nss = n.replace(".", "").replace(",", "");
                try {
                    Integer monto = Integer.parseInt(nss);
                    this.txfDesdeRango.setText(df.format(monto));
                } catch (NumberFormatException ex) {
                    System.out.println("Error al convertir desde de string a int: " + ex);
                    this.txfDesdeRango.setText(o);
                }
            }
        });
        this.txfHastaRango.textProperty().addListener((ObservableValue<? extends String> obs, String o, String n) -> {
            if (!n.isEmpty()) {
                DecimalFormat df = new DecimalFormat("#,###,###,###");
                String nss = n.replace(".", "").replace(",", "");
                try {
                    Integer monto = Integer.parseInt(nss);
                    this.txfHastaRango.setText(df.format(monto));
                } catch (NumberFormatException ex) {
                    System.out.println("Error al convertir desde de string a int: " + ex);
                    this.txfHastaRango.setText(o);
                }
            }
        });

        this.txfCantCupones.textProperty().addListener((ObservableValue<? extends String> obs, String o, String n) -> {
            if (!n.isEmpty()) {
                DecimalFormat df = new DecimalFormat("#,###,###,###");
                String nss = n.replace(".", "").replace(",", "");
                try {
                    Integer monto = Integer.parseInt(nss);
                    this.txfCantCupones.setText(df.format(monto));
                } catch (NumberFormatException ex) {
                    System.out.println("Error al convertir desde de string a int: " + ex);
                    this.txfCantCupones.setText(o);
                }
            }
        });
        this.txfPrecioCupon.textProperty().addListener((ObservableValue<? extends String> obs, String o, String n) -> {
            if (!n.isEmpty()) {
                DecimalFormat df = new DecimalFormat("#,###,###,###");
                String nss = n.replace(".", "").replace(",", "");
                try {
                    Integer monto = Integer.parseInt(nss);
                    this.txfPrecioCupon.setText(df.format(monto));
                } catch (NumberFormatException ex) {
                    System.out.println("Error al convertir desde de string a int: " + ex);
                    this.txfPrecioCupon.setText(o);
                }
            }
        });
    }

    private void inicializarTablaRangos() {
        this.tblRangos.setOnKeyPressed((e) -> {
            System.out.println("Key press tabla");
            if (e.getCode().equals(KeyCode.ENTER)) {
                this.tblRangos.edit(this.tblRangos.getItems().size() - 1, this.colDesde);
            }
        });
        this.tblRangos.widthProperty().addListener((ObservableValue<? extends Number> obs, Number o, Number n) -> {
            Double ajuste = this.colElim.getPrefWidth() + 2;
            this.colDesde.setPrefWidth((n.doubleValue() - ajuste) * 0.35);
            this.colHasta.setPrefWidth((n.doubleValue() - ajuste) * 0.35);
            this.colNroCupones.setPrefWidth((n.doubleValue() - ajuste) * 0.3);
        });

        this.colDesde.setCellValueFactory((TableColumn.CellDataFeatures<RangoPromoModelo, Number> p) -> {
            return p.getValue().desdeProperty();
        });

        this.colDesde.setCellFactory((TableColumn<RangoPromoModelo, Number> p) -> {
            return new TableCell<RangoPromoModelo, Number>() {

                @Override
                protected void updateItem(Number item, boolean empty) {
                    super.updateItem(item, empty);
                    this.setAlignment(Pos.CENTER_LEFT);
                    if (!empty) {
                        DecimalFormat df = new DecimalFormat("#,###,###,###");
                        this.setText(df.format(item.intValue()));
                    } else {
                        this.setText("");
                    }
                }

            };
        });

        this.colHasta.setCellValueFactory((TableColumn.CellDataFeatures<RangoPromoModelo, Number> p) -> {
            return p.getValue().hastaProperty();
        });

        this.colHasta.setCellFactory((TableColumn<RangoPromoModelo, Number> p) -> {
            return new TableCell<RangoPromoModelo, Number>() {
                @Override
                protected void updateItem(Number item, boolean empty) {
                    super.updateItem(item, empty);
                    this.setAlignment(Pos.CENTER_LEFT);
                    if (!empty) {
                        if (item.intValue() == Integer.MAX_VALUE) {
                            this.setText("(Sin límite)");
                        } else {
                            DecimalFormat df = new DecimalFormat("#,###,###,###");
                            this.setText(df.format(item.intValue()));
                        }
                    } else {
                        this.setText("");
                    }
                }
            };
        });

        this.colNroCupones.setCellValueFactory((TableColumn.CellDataFeatures<RangoPromoModelo, Number> p) -> {
            return p.getValue().cuponesProperty();
        });

        this.colNroCupones.setCellFactory((TableColumn<RangoPromoModelo, Number> p) -> {
            return new TableCell<RangoPromoModelo, Number>() {
                @Override
                protected void updateItem(Number item, boolean empty) {
                    super.updateItem(item, empty);
                    this.setAlignment(Pos.CENTER_LEFT);
                    if (!empty) {
                        this.setText(item.intValue() + "");
                    } else {
                        this.setText("");
                    }
                }
            };
        });

        this.colElim.setCellFactory((TableColumn) -> {
            return new TableCell() {
                @Override
                protected void updateItem(Object item, boolean empty) {
                    super.updateItem(item, empty);
                    this.setAlignment(Pos.CENTER);
                    if (!empty) {
                        Button btne = new Button();
                        try {
                            ImageView img = new ImageView(new Image(getClass().getResourceAsStream("/iconos/x16/delete.png")));
                            btne.setGraphic(img);
                            btne.setOnAction((e) -> {
                                this.getTableView().getItems().remove(this.getTableRow().getIndex());
                            });
                        } catch (Exception ex) {
                            System.out.println("error al cargar icono eliminar: " + ex);
                        }
                        this.setGraphic(btne);
                    } else {
                        this.setGraphic(null);
                    }
                }
            };
        });

        this.rdbMontoUnico.selectedProperty().addListener((ObservableValue<? extends Boolean> obs, Boolean o, Boolean n) -> {
            if (n) {
                this.accDatosPromo.getPanes().remove(this.tpRangos);
                if (!this.vboxDatosPromo.getChildren().contains(this.lblPrecioCupon)) {
                    this.vboxDatosPromo.getChildren().add(this.lblPrecioCupon);
                }
                if (!this.vboxDatosPromo.getChildren().contains(this.txfPrecioCupon)) {
                    this.vboxDatosPromo.getChildren().add(this.txfPrecioCupon);
                }
            } else {
                this.vboxDatosPromo.getChildren().remove(this.lblPrecioCupon);
                this.vboxDatosPromo.getChildren().remove(this.txfPrecioCupon);
                if (!this.accDatosPromo.getPanes().contains(this.tpRangos)) {
                    this.accDatosPromo.getPanes().add(this.tpRangos);
                    this.accDatosPromo.setExpandedPane(this.tpRangos);
                }
            }
        });
    }

    private void inicializarTablaPromociones() {
        this.tblPromociones.setItems(lstPromociones);
        this.colCodigoPromo.setCellValueFactory(new PropertyValueFactory<>("idPromocion"));
        this.colDescripcionPromo.setCellValueFactory(new PropertyValueFactory<>("descripcion"));
        this.colFinPromo.setCellValueFactory(new PropertyValueFactory<>("fechaFin"));
        this.colInicioPromo.setCellValueFactory(new PropertyValueFactory<>("fechaInicio"));
        this.colGenerarPor.setCellValueFactory(new PropertyValueFactory<>("porRango"));
        this.colPromoVigente.setCellValueFactory(new PropertyValueFactory<>("activo"));

        this.colInicioPromo.setCellFactory((TableColumn<Promocion, Calendar> v) -> new FechaTableCell());
        this.colFinPromo.setCellFactory((TableColumn<Promocion, Calendar> v) -> new FechaTableCell());
//        this.colPrecioCupon.setCellFactory((TableColumn<Promocion, Integer> v) -> new PrecioCuponTableCell());
        this.colPromoVigente.setCellFactory((TableColumn<Promocion, Boolean> v) -> new PromoVigenteTableCell(this));
        this.colGenerarPor.setCellFactory((TableColumn<Promocion, Boolean> p) -> {
            return new TableCell<Promocion, Boolean>() {
                @Override
                protected void updateItem(Boolean item, boolean empty) {
                    super.updateItem(item, true);
                    if (!empty) {
                        if (item) {
                            this.setText("Rango");
                        } else {
                            this.setText("Monto");
                        }
                    } else {
                        this.setText("");
                    }
                }
            };
        });
    }

    private void cargarIconos() {
        Image imgBuscar = new Image(getClass().getResourceAsStream("/iconos/x16/search.png"));
        this.imgvBuscar.setImage(imgBuscar);

        Image imgRegistrar = new Image(getClass().getResourceAsStream("/iconos/x16/save.png"));
        this.btnRegistrarPromo.setGraphic(new ImageView(imgRegistrar));

        Image imgNuevo = new Image(getClass().getResourceAsStream("/iconos/x16/document.png"));
        this.btnNuevaPromo.setGraphic(new ImageView(imgNuevo));

        Image imgEliminar = new Image(getClass().getResourceAsStream("/iconos/x16/delete.png"));
        this.btnEliminarPromo.setGraphic(new ImageView(imgEliminar));
    }

    private boolean camposCorrectos() {
        Alert al = new Alert(AlertType.WARNING);
        al.setTitle("Guardar Promoción");
        boolean validado = false;
        if (this.rdbMontoUnico.isSelected()) {
            if (!this.txfPrecioCupon.getText().isEmpty()) {
                try {
                    Integer pre = Integer.parseInt(txfPrecioCupon.getText().replace(".", "").replace(",", ""));
                    validado = true;
                } catch (NumberFormatException ex) {
                    al.setContentText("Introduzca un valor válido en el campo 'Precio'");
                    al.show();
                    System.out.println("Error al convertir monto de cupon a Entero: " + ex.getMessage());
                }
            } else {
                al.setContentText("Introduzca un valor en el campo 'Precio'");
                al.show();
            }
        } else {
            if (this.tblRangos.getItems().isEmpty()) {
                al.setContentText("Debe ingresar un rango como mínimo");
                al.show();
            }else{
                validado = true;
            }
        }
        return validado;
    }

    private void guardarPromocion() {
        if (this.camposCorrectos()) {
            PromocionJPAController promojpa = new PromocionJPAController(ConexionBD.EMF.createEntityManager());
            Promocion pr = new Promocion();
            pr.setDescripcion(this.txfDescripcionPromo.getText());
            pr.setActivo(false);

            if (this.dpkFechaInicio.getValue() != null) {
                Calendar fi = new GregorianCalendar();
                fi.set(Calendar.YEAR, this.dpkFechaInicio.getValue().getYear());
                fi.set(Calendar.MONTH, this.dpkFechaInicio.getValue().getMonthValue() - 1);
                fi.set(Calendar.DAY_OF_MONTH, this.dpkFechaInicio.getValue().getDayOfMonth());
                pr.setFechaInicio(fi);
            }
            if (this.dpkFechaFin.getValue() != null) {
                Calendar ff = new GregorianCalendar();
                ff.set(Calendar.YEAR, this.dpkFechaFin.getValue().getYear());
                ff.set(Calendar.MONTH, this.dpkFechaFin.getValue().getMonthValue() - 1);
                ff.set(Calendar.DAY_OF_MONTH, this.dpkFechaFin.getValue().getDayOfMonth());
                pr.setFechaFin(ff);
            }

            
            pr.setPorRango(this.rdbRango.isSelected());
            if (this.rdbRango.isSelected()) {
                List<RangoPromocion> lstr = new ArrayList<>();
                for (RangoPromoModelo rpm : this.tblRangos.getItems()) {
                    lstr.add(rpm.toRangoPromocion());
                }
                promojpa.registrarPromocion(pr, lstr);
                this.lstPromociones.add(pr);
            } else {
                pr.setMontoCupon(Integer.parseInt(this.txfPrecioCupon.getText().replace(".", "").replace(",", "")));
                promojpa.registrarPromocion(pr);
                this.lstPromociones.add(pr);
            }
        }
    }

    private void modificarPromocion() {
        PromocionJPAController promojpa = new PromocionJPAController(ConexionBD.EMF.createEntityManager());
        if (this.camposCorrectos()) {
            int posicionPromo = this.lstPromociones.indexOf(this.promoActual.getValue());
            Promocion pr = new Promocion();
            pr.setIdPromocion(this.promoActual.getValue().getIdPromocion());
            pr.setDescripcion(this.txfDescripcionPromo.getText());
            pr.setActivo(this.promoActual.getValue().isActivo());

            if (this.dpkFechaInicio.getValue() != null) {
                Calendar fi = new GregorianCalendar();
                fi.set(Calendar.YEAR, this.dpkFechaInicio.getValue().getYear());
                fi.set(Calendar.MONTH, this.dpkFechaInicio.getValue().getMonthValue() - 1);
                fi.set(Calendar.DAY_OF_MONTH, this.dpkFechaInicio.getValue().getDayOfMonth());
                pr.setFechaInicio(fi);
            }
            if (this.dpkFechaFin.getValue() != null) {
                Calendar ff = new GregorianCalendar();
                ff.set(Calendar.YEAR, this.dpkFechaFin.getValue().getYear());
                ff.set(Calendar.MONTH, this.dpkFechaFin.getValue().getMonthValue() - 1);
                ff.set(Calendar.DAY_OF_MONTH, this.dpkFechaFin.getValue().getDayOfMonth());
                pr.setFechaFin(ff);
            }
            pr.setPorRango(this.rdbRango.isSelected());
            if(pr.isPorRango()){
                pr.setMontoCupon(null);
                List<RangoPromocion> lstr=new ArrayList<>();
                for(RangoPromoModelo rpm:this.tblRangos.getItems()){
                    lstr.add(rpm.toRangoPromocion());
                }
                promojpa.modificarPromocion(pr, lstr);
            }else{
                pr.setMontoCupon(Integer.parseInt(this.txfPrecioCupon.getText().replace(".", "").replace(",", "")));
                promojpa.modificarPromocion(pr);
            }            
            this.lstPromociones.remove(posicionPromo);
            this.lstPromociones.add(posicionPromo, pr);
            this.tblPromociones.getSelectionModel().clearSelection();
        }
    }

    public void recargarPromociones() {
        PromocionJPAController projpa = new PromocionJPAController(ConexionBD.EMF.createEntityManager());
        this.lstPromociones.clear();
        this.lstPromociones.addAll(projpa.getTodasPromociones());
    }

    public void eliminarPromociones() {
        PromocionJPAController projpa = new PromocionJPAController(ConexionBD.EMF.createEntityManager());
        Alert dlg = new Alert(AlertType.WARNING);
        dlg.setTitle("Eliminar Promoción");
        if (this.tblPromociones.getSelectionModel().isEmpty()) {
            dlg.setContentText("Debe seleccionar una promoción");
            dlg.showAndWait();
        } else {
            Alert conf = new Alert(AlertType.CONFIRMATION);
            conf.setTitle("Eliminar Promociones");
            conf.setContentText("Se eliminarán las promociones seleccionadas.\n ¿Desea continuar?");
            Optional<ButtonType> eleccion = conf.showAndWait();
            if (eleccion.get().equals(ButtonType.OK)) {

                for (Promocion pr : this.tblPromociones.getSelectionModel().getSelectedItems()) {
                    if (projpa.registraCupones(pr)) {
                        dlg.setContentText("La promoción con código '" + pr.getIdPromocion() + "' tiene cupones registrados\n No puede eliminarse.");
                        dlg.showAndWait();
                    } else if (projpa.registraFacturas(pr)) {
                        dlg.setContentText("La promoción con código '" + pr.getIdPromocion() + "' tiene facturas registradas\n No puede eliminarse.");
                        dlg.showAndWait();
                    } else {

                        projpa.eliminarPromocion(pr);
                        this.lstPromociones.remove(pr);

                    }
                }
            }
        }
    }

    private void agregarRango() {
        RangoPromoModelo rp = new RangoPromoModelo();
        if (this.txfDesdeRango.getText().isEmpty()) {
            rp.setDesde(0);
        } else {
            rp.setDesde(Integer.parseInt(this.txfDesdeRango.getText().replace(".", "").replace(",", "")));
        }
        if (this.txfHastaRango.getText().isEmpty()) {
            rp.setHasta(Integer.MAX_VALUE);
        } else {
            rp.setHasta(Integer.parseInt(this.txfHastaRango.getText().replace(".", "").replace(",", "")));
        }
        if (this.txfCantCupones.getText().isEmpty()) {
            rp.setCupones(1);
        } else {
            rp.setCupones(Integer.parseInt(this.txfCantCupones.getText().replace(".", "").replace(",", "")));
        }
        if (rp.getDesde() < rp.getHasta()) {
            if (this.validarRango(rp)) {
                this.tblRangos.getItems().add(rp);
                this.txfDesdeRango.clear();
                this.txfHastaRango.clear();
                this.txfCantCupones.clear();
            }
        } else {
            Alert al = new Alert(AlertType.ERROR);
            al.setHeaderText("Rango inválido");
            al.setContentText("Rango invertido. El monto 'Desde' debe ser menor al monto 'Hasta'");
            al.show();
        }
    }

    private boolean validarRango(RangoPromoModelo rpm) {
        boolean val = true;
        DecimalFormat df = new DecimalFormat("#,###,###,###");
        if (!this.tblRangos.getItems().isEmpty()) {
            for (RangoPromoModelo rp : this.tblRangos.getItems()) {
                if (rpm.getDesde() >= rp.getDesde() && rpm.getDesde() <= rp.getHasta()) {
                    val = false;
                    Alert al = new Alert(AlertType.ERROR);
                    al.setHeaderText("Rango inválido");
                    al.setContentText(df.format(rpm.getDesde()) + " está dentro del rango " + df.format(rp.getDesde()) + "-" + df.format(rp.getHasta()));
                    al.show();
                    break;
                }
                if (rpm.getHasta() >= rp.getDesde() && rpm.getHasta() <= rp.getHasta()) {
                    val = false;
                    Alert al = new Alert(AlertType.ERROR);
                    al.setHeaderText("Rango inválido");
                    al.setContentText(df.format(rpm.getHasta()) + " está dentro del rango " + df.format(rp.getDesde()) + "-" + df.format(rp.getHasta()));
                    al.show();
                    break;
                }
            }
        }
        return val;
    }

    @FXML
    private void onActionBtnRegistrar(ActionEvent event) {
        if (this.operacionActual == PromocionesController.OPERACION_REGISTRAR) {
            System.out.println("Registrar");
            this.guardarPromocion();
        } else if (this.operacionActual == PromocionesController.OPERACION_MODIFICAR) {
            System.out.println("Modificar");
            this.modificarPromocion();
        }
    }

    @FXML
    private void onActionBtnEliminar(ActionEvent event) {
        this.eliminarPromociones();
    }

    @FXML
    private void onActionBtnNuevo(ActionEvent event) {
        this.promoActual.setValue(null);

    }

    @FXML
    private void onActionBtnAgregarRango(ActionEvent event) {
        this.agregarRango();
    }

    @FXML
    private void onActionTxfDesdeRango(ActionEvent event) {
        this.txfHastaRango.requestFocus();
    }

    @FXML
    private void onActionTxfHastaRango(ActionEvent event) {
        this.txfCantCupones.requestFocus();
    }

    @FXML
    private void onActionTxfCantCuponesRango(ActionEvent event) {
        this.agregarRango();
        this.txfDesdeRango.requestFocus();
    }

}
