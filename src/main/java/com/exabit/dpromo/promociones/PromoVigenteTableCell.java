/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exabit.dpromo.promociones;

import com.exabit.dpromo.bd.ConexionBD;
import com.exabit.dpromo.bd.controllers.PromocionJPAController;
import com.exabit.dpromo.bd.entidades.Promocion;
import javafx.geometry.Pos;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TableCell;

/**
 *
 * @author cristhian
 */
public class PromoVigenteTableCell extends TableCell<Promocion, Boolean> {

    private final PromocionesController promoController;

    public PromoVigenteTableCell(PromocionesController promoController) {
        this.promoController = promoController;
    }
    
    @Override
    protected void updateItem(Boolean item, boolean empty){
        this.setText("");
        this.setAlignment(Pos.CENTER);
        if(item!=null){
            CheckBox chk=new CheckBox();
            chk.setSelected(item);
            chk.setOnAction(evt->{
                if(chk.isSelected()){
                    Promocion promo=(Promocion) this.getTableRow().getItem();
                    PromocionJPAController promojpa=new PromocionJPAController(ConexionBD.EMF.createEntityManager());
                    promojpa.activarPromocion(promo);
                    this.promoController.recargarPromociones();
                }else{
                    chk.setSelected(true);
                }
                
            });
            this.setGraphic(chk);
        }else{
            this.setGraphic(null);
        }
        super.updateItem(item, true);
    }
}
