/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exabit.dpromo.promociones;

import com.exabit.dpromo.bd.entidades.Promocion;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import javafx.scene.control.TableCell;

/**
 *
 * @author cristhian
 */
public class FechaTableCell extends TableCell<Promocion, Calendar> {
    
    @Override
    protected void updateItem(Calendar item, boolean empty){
        if(item!=null){
            Date d=new Date(item.getTimeInMillis());
            SimpleDateFormat df=new SimpleDateFormat("dd/MM/yy");
            this.setText(df.format(d));
        }else{
            this.setText("");
        }
        super.updateItem(item, empty);
    }
    
}
