/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exabit.dpromo.promociones;

import com.exabit.dpromo.bd.entidades.Promocion;
import java.text.DecimalFormat;
import javafx.geometry.Pos;
import javafx.scene.control.TableCell;

/**
 *
 * @author cristhian
 */
public class PrecioCuponTableCell extends TableCell<Promocion, Integer> {
    @Override
    protected void updateItem(Integer item, boolean empty){
        this.setAlignment(Pos.CENTER_RIGHT);
        if(item!=null){
            DecimalFormat df=new DecimalFormat("###,###,###,###,###,###");
            this.setText(df.format(item));
        }else{
            this.setText("");
        }
        super.updateItem(item, empty);
    }
    
}
