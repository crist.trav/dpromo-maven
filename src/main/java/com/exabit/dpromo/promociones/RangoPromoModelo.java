package com.exabit.dpromo.promociones;

import com.exabit.dpromo.bd.entidades.RangoPromocion;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;

/**
 *
 * @author traver
 */
public class RangoPromoModelo {

    private final IntegerProperty desde = new SimpleIntegerProperty();
    private final IntegerProperty cupones = new SimpleIntegerProperty();
    private final IntegerProperty hasta = new SimpleIntegerProperty();

    public RangoPromoModelo() {
    }
    
    public RangoPromoModelo(RangoPromocion rp){
        this.desde.setValue(rp.getMontoDesde());
        this.hasta.setValue(rp.getMontoHasta());
        this.cupones.setValue(rp.getCantidadCupones());
    }

    public int getDesde() {
        return desde.get();
    }

    public void setDesde(int value) {
        desde.set(value);
    }

    public int getHasta() {
        return hasta.get();
    }

    public void setHasta(int value) {
        hasta.set(value);
    }

    public int getCupones() {
        return cupones.get();
    }

    public void setCupones(int value) {
        cupones.set(value);
    }

    public IntegerProperty cuponesProperty() {
        return cupones;
    }
    
    public IntegerProperty desdeProperty() {
        return desde;
    }
    
    public IntegerProperty hastaProperty() {
        return hasta;
    }
    
    public RangoPromocion toRangoPromocion(){
        RangoPromocion rp=new RangoPromocion();
        rp.setCantidadCupones(this.getCupones());
        rp.setMontoDesde(this.desde.getValue());
        rp.setMontoHasta(this.hastaProperty().getValue());
        return rp;
    }
}
