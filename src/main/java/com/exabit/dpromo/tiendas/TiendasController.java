/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exabit.dpromo.tiendas;

import com.exabit.dpromo.bd.ConexionBD;
import com.exabit.dpromo.bd.controllers.TiendaJPAController;
import com.exabit.dpromo.bd.entidades.Tienda;
import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Accordion;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.TitledPane;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

/**
 * FXML Controller class
 *
 * @author cristhian
 */
public class TiendasController implements Initializable {

    @FXML
    private Button btnNuevaTienda;
    @FXML
    private Button btnEliminarTienda;
    @FXML
    private ImageView imgvSearch;
    @FXML
    private Button btnGuardarTienda;
    @FXML
    private TextField txfBuscarTienda;
    @FXML
    private TableView<Tienda> tblTiendas;
    @FXML
    private TableColumn<Tienda, Integer> colCodigoTienda;
    @FXML
    private TableColumn<Tienda, String> colNombreTienda;
    @FXML
    private TableColumn<Tienda, Boolean> colEstadoTienda;
    @FXML
    private Accordion accDatosTienda;
    @FXML
    private TitledPane tpaneDatosTienda;
    @FXML
    private TextField txfIdTienda;
    @FXML
    private TextField txfNombreTienda;
    
    private static final int OPERACION_MODIFICAR=2;
    private static final int OPERACION_CREAR=1;
    private int operacionActual=TiendasController.OPERACION_CREAR;
    
    private final ObservableList<Tienda> lstTienda=FXCollections.observableArrayList();
    
    private final SimpleObjectProperty<Tienda> tiendaSeleccionada=new SimpleObjectProperty<>();
    
    /*private final ListChangeListener<Tienda> lstChLnrTiendasSeleccionadas=(ListChangeListener.Change<? extends Tienda> c)->{
        c.next();
        if(c.getList().size()==1){
            this.tiendaSeleccionada.setValue(c.getList().get(0));
        }else{
            this.tiendaSeleccionada.setValue(null);
        }
    };*/
    
    private final ChangeListener<Tienda> chLnrSeleccionTienda=(ObservableValue<? extends Tienda> obs, Tienda oldValue, Tienda newValue)->{
        if(newValue!=null){
            this.tiendaSeleccionada.setValue(newValue);
        }
    };
    private final ChangeListener<Tienda> chLnrAsignacionTienda=(ObservableValue<? extends Tienda> obs, Tienda oldValue, Tienda newValue)->{
        if(newValue!=null){
            this.operacionActual=TiendasController.OPERACION_MODIFICAR;
            this.btnGuardarTienda.setText("Modificar");
            Image imgEditar=new Image(getClass().getResourceAsStream("/iconos/x16/edit.png"));
            this.btnGuardarTienda.setGraphic(new ImageView(imgEditar));
            this.txfIdTienda.setText(newValue.getIdTienda()==null?"":newValue.getIdTienda().toString());
            this.txfNombreTienda.setText(newValue.getNombre());
        }else{
            this.operacionActual=TiendasController.OPERACION_CREAR;
            this.txfIdTienda.setText("");
            this.txfNombreTienda.setText("");
            this.btnGuardarTienda.setText("Registrar");
            Image imgEditar=new Image(getClass().getResourceAsStream("/iconos/x16/save.png"));
            this.btnGuardarTienda.setGraphic(new ImageView(imgEditar));
            
        }
    };
    
    private final ChangeListener<String> chLnrBusqueda=(ObservableValue<? extends String> obs, String oldValue, String newValue)->{
        this.lstTienda.clear();
        TiendaJPAController tjpa=new TiendaJPAController(ConexionBD.EMF.createEntityManager());
        if(newValue==null){
            this.lstTienda.addAll(tjpa.getTodasTiendas());
        }else if(newValue.isEmpty()){
            this.lstTienda.addAll(tjpa.getTodasTiendas());
        }else{
            this.lstTienda.addAll(tjpa.buscarTienda(newValue));
        }
    };
    
    /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        this.inicializarIconos();
        this.inicializarTabla();
        TiendaJPAController tjpa=new TiendaJPAController(ConexionBD.EMF.createEntityManager());
        this.lstTienda.addAll(tjpa.getTodasTiendas());
        this.accDatosTienda.setExpandedPane(tpaneDatosTienda);
        //this.tblTiendas.getSelectionModel().getSelectedItems().addListener(this.lstChLnrTiendasSeleccionadas);
        this.tblTiendas.getSelectionModel().selectedItemProperty().addListener(this.chLnrSeleccionTienda);
        this.tiendaSeleccionada.addListener(this.chLnrAsignacionTienda);
        this.txfBuscarTienda.textProperty().addListener(this.chLnrBusqueda);
    }    

    @FXML
    private void onActionBtnGuardar(ActionEvent event) {
        if(this.operacionActual==TiendasController.OPERACION_CREAR){
            this.guardarTienda();
        }else{
            this.modificarTienda();
        }
    }
    
    
    private void inicializarTabla(){
        this.tblTiendas.setItems(lstTienda);
        this.colCodigoTienda.setCellValueFactory(new PropertyValueFactory<>("idTienda"));
        this.colNombreTienda.setCellValueFactory(new PropertyValueFactory<>("nombre"));
        this.colEstadoTienda.setCellValueFactory(new PropertyValueFactory<>("activo"));
        
        this.colEstadoTienda.setCellFactory((TableColumn<Tienda, Boolean> data)-> new EstadoTiendaTableCell());
    }
    
    private void inicializarIconos(){
        Image imgSearch=new Image(getClass().getResourceAsStream("/iconos/x16/search.png"));
        this.imgvSearch.setImage(imgSearch);        
        
        Image imgNuevo=new Image(getClass().getResourceAsStream("/iconos/x16/document.png"));
        this.btnNuevaTienda.setGraphic(new ImageView(imgNuevo));
        
        Image imgGuardar=new Image(getClass().getResourceAsStream("/iconos/x16/save.png"));
        this.btnGuardarTienda.setGraphic(new ImageView(imgGuardar));
        
        Image imgEliminar=new Image(getClass().getResourceAsStream("/iconos/x16/delete.png"));
        this.btnEliminarTienda.setGraphic(new ImageView(imgEliminar));
    }
    
    private void guardarTienda(){
        if(this.txfNombreTienda.getText().isEmpty()){
            Alert al=new Alert(AlertType.INFORMATION);
            al.setTitle("Nueva Tienda");
            al.setContentText("El nombre de la tienda está en blanco");
            al.showAndWait();
        }else{
            TiendaJPAController tjpa=new TiendaJPAController(ConexionBD.EMF.createEntityManager());
            Tienda t=new Tienda();
            t.setNombre(this.txfNombreTienda.getText());
            t.setActivo(true);
            tjpa.registrarTienda(t);
            this.txfIdTienda.setText(t.getIdTienda().toString());
            this.lstTienda.add(t);
            this.txfIdTienda.setText("");
            this.txfNombreTienda.setText("");
            this.tiendaSeleccionada.setValue(null);
        }
    }
    
    private void modificarTienda(){
        if(this.txfNombreTienda.getText().isEmpty()){
            Alert al=new Alert(AlertType.INFORMATION);
            al.setTitle("Nueva Tienda");
            al.setContentText("El nombre de la tienda está en blanco");
            al.showAndWait();
        }else{
            TiendaJPAController tjpa=new TiendaJPAController(ConexionBD.EMF.createEntityManager());
            Tienda ti=this.tiendaSeleccionada.getValue();
            Tienda tMod=new Tienda();
            int posicionTienda=this.lstTienda.indexOf(ti);
            tMod.setIdTienda(ti.getIdTienda());
            tMod.setActivo(ti.isActivo());
            tMod.setNombre(this.txfNombreTienda.getText());
            tjpa.modificarTienda(tMod);
            this.lstTienda.indexOf(ti);
            this.lstTienda.remove(posicionTienda);
            this.lstTienda.add(posicionTienda, tMod);
            this.tblTiendas.getSelectionModel().select(tMod);
        }
    }

    @FXML
    private void onActionBtnNuevo(ActionEvent event) {
        this.tiendaSeleccionada.setValue(null);
    }

    @FXML
    private void onActionBtnEliminar(ActionEvent event) {
        if(this.tblTiendas.getSelectionModel().isEmpty()){
            Alert al=new Alert(AlertType.WARNING);
            al.setTitle("Eliminar Tiendas");
            al.setContentText("Debe seleccionar por lo menos una tienda");
            al.showAndWait();
        }else{
            Alert conf=new Alert(AlertType.CONFIRMATION);
            conf.setTitle("Eliminar Tiendas");
            conf.setContentText("Se eliminarán las tiendas seleccionadas. ¿Desea continuar?");
            Optional<ButtonType> eleccion=conf.showAndWait();
            if(eleccion.get().equals(ButtonType.OK)){
                TiendaJPAController tjpa=new TiendaJPAController(ConexionBD.EMF.createEntityManager());
                Tienda tiendaEliminar=this.tblTiendas.getSelectionModel().getSelectedItem();
                tjpa.eliminarTienda(tiendaEliminar);
                this.lstTienda.remove(tiendaEliminar);
            }
        }
    }
}
