/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exabit.dpromo.tiendas;

import com.exabit.dpromo.bd.ConexionBD;
import com.exabit.dpromo.bd.controllers.TiendaJPAController;
import com.exabit.dpromo.bd.entidades.Tienda;
import javafx.geometry.Pos;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TableCell;

/**
 *
 * @author cristhian
 */
public class EstadoTiendaTableCell extends TableCell<Tienda,Boolean> {
    
    @Override
    protected void updateItem(Boolean a, boolean empty){
        this.setAlignment(Pos.CENTER);
        if(a!=null){
            Tienda t=(Tienda) this.getTableRow().getItem();
                if(t!=null){
                CheckBox c=new CheckBox();
                c.setSelected(a);
                c.setOnAction((evt)->{
                    TiendaJPAController tjpa=new TiendaJPAController(ConexionBD.EMF.createEntityManager());

                    t.setActivo(c.isSelected());
                    tjpa.modificarTienda(t);
                });
                if(t.getIdTienda().equals(1)){
                    c.setDisable(true);
                }
                this.setGraphic(c);
            }
        }else{
            this.setGraphic(null);
        }
        
    }
    
}
