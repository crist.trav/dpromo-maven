/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exabit.dpromo.generador;

import com.exabit.dpromo.bd.ConexionBD;
import com.exabit.dpromo.bd.controllers.FacturaJPAController;
import com.exabit.dpromo.bd.entidades.Factura;
import javafx.collections.ObservableList;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.TableCell;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

/**
 *
 * @author cristhian
 */
public class EliminarFacturaTableCell extends TableCell<FacturaCuponModelo, Factura> {

    @Override
    protected void updateItem(Factura f, boolean empty){
        this.setAlignment(Pos.CENTER);
        if(f!=null){
            Image imgElim=new Image(getClass().getResourceAsStream("/iconos/x16/document-delete.png"));
            Button e=new Button();
            Tooltip tooltip=new Tooltip();
            tooltip.setText("Eliminar esta factura sin procesar");
            e.setTooltip(tooltip);
            FacturaJPAController fajpa=new FacturaJPAController(ConexionBD.EMF.createEntityManager());
            e.getStyleClass().add("boton-tabla");
            e.setGraphic(new ImageView(imgElim));
            
            e.setOnAction((evt)->{
                ObservableList<FacturaCuponModelo> lstFCM=this.getTableView().getItems();
                for(FacturaCuponModelo fcm : lstFCM){
                    if(fcm.getFactura().getIdFactura().equals(f.getIdFactura())){
                        fajpa.eliminarFactura(f);
                        lstFCM.remove(fcm);
                        break;
                    }
                }
            });
            if(f.isProcesado()){
                e.setDisable(true);
            }else{
                e.setDisable(false);
            }
            super.setGraphic(e);
        }else{
            super.setGraphic(null);
        }
        super.updateItem(f, empty);
    }
    
}
