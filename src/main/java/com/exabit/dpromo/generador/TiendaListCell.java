/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exabit.dpromo.generador;

import com.exabit.dpromo.bd.entidades.Tienda;
import javafx.scene.control.ListCell;

/**
 *
 * @author cristhian
 */
public class TiendaListCell extends ListCell<Tienda> {
    
    @Override
    protected void updateItem(Tienda t, boolean empty){
        if(t!=null){
            super.setText(t.getNombre());
        }else{
            super.setText("");
        }
        super.updateItem(t, empty);
    }
}
