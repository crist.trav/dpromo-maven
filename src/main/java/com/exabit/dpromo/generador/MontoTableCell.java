/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exabit.dpromo.generador;

import java.text.DecimalFormat;
import javafx.geometry.Pos;
import javafx.scene.control.TableCell;

/**
 *
 * @author cristhian
 */
public class MontoTableCell extends TableCell<FacturaCuponModelo, Number> {
    
    @Override
    protected void updateItem(Number m, boolean empty){
        this.setAlignment(Pos.CENTER_LEFT);
        if(m!=null){
            DecimalFormat df=new DecimalFormat("###,###,###,###,###,###");
            super.setText(df.format(m));
        }else{
            super.setText("");
        }
    }
    
}
