/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exabit.dpromo.generador;

import com.exabit.dpromo.bd.entidades.Factura;
import com.exabit.dpromo.bd.entidades.Tienda;
import java.util.Date;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;

/**
 *
 * @author cristhian
 */
public class FacturaCuponModelo {

    private final ObjectProperty<Tienda> tienda = new SimpleObjectProperty<>();
    private final IntegerProperty monto = new SimpleIntegerProperty();
    //private final IntegerProperty cantCupones = new SimpleIntegerProperty();
    private final ObjectProperty<Date> fechaHoraReg = new SimpleObjectProperty<>();
    private final ObjectProperty<Factura> factura = new SimpleObjectProperty<>();

    public FacturaCuponModelo(Factura f) {
        this.tienda.setValue(f.getTienda());
        this.monto.setValue(f.getMontoFactura());
        Date d=new Date(f.getFechaHoraRegistro().getTimeInMillis());
        this.fechaHoraReg.setValue(d);
        this.factura.setValue(f);
        //this.cantCupones.setValue(f.getMontoFactura()/f.getMontoCupon());
    }

    public FacturaCuponModelo() {
    }
    
    
    public Factura getFactura() {
        return factura.get();
    }

    public void setFactura(Factura value) {
        factura.set(value);
    }

    public ObjectProperty facturaProperty() {
        return factura;
    }
    

    public Date getFechaHoraReg() {
        return fechaHoraReg.get();
    }

    public void setFechaHoraReg(Date value) {
        fechaHoraReg.set(value);
    }

    public ObjectProperty<Date> fechaHoraRegProperty() {
        return fechaHoraReg;
    }
    

    /*public int getCantCupones() {
        return cantCupones.get();
    }

    public void setCantCupones(int value) {
        cantCupones.set(value);
    }

    public IntegerProperty cantCuponesProperty() {
        return cantCupones;
    }*/
    

    public int getMonto() {
        return monto.get();
    }

    public void setMonto(int value) {
        monto.set(value);
    }

    public IntegerProperty montoProperty() {
        return monto;
    }
    

    public Tienda getTienda() {
        return tienda.get();
    }

    public void setTienda(Tienda value) {
        tienda.set(value);
    }

    public ObjectProperty<Tienda> tiendaProperty() {
        return tienda;
    }
    
    
    
}
