/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exabit.dpromo.generador;

import com.exabit.dpromo.bd.entidades.Cupon;
import com.exabit.dpromo.impresion.ImpresionCupon;
import java.util.ArrayList;
import java.util.List;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;

/**
 *
 * @author cristhian
 */
public class CuponListCell extends ListCell<Cupon> {
    
    @Override
    protected void updateItem(Cupon c, boolean empty){
        super.setText("");
        if(c!=null){
            String estado="";
            if(c.isImpreso()){
                estado="(Impreso)";
            }else{
                estado="(Pendiente)";
            }
            HBox hbox=new HBox();
            hbox.setAlignment(Pos.CENTER_LEFT);
            Button impr=new Button();
            impr.getStyleClass().add("boton-tabla");
            Tooltip tooltip=new Tooltip();
            tooltip.setText("Imprimir este cupón");
            impr.setTooltip(tooltip);
            impr.setOnAction((evt)->{
                ImpresionCupon impresionCupon=new ImpresionCupon();
                List<Cupon> lstCupon=new ArrayList<>();
                lstCupon.add(c);
                impresionCupon.imprimir(c.getCliente(), lstCupon);
            });
            Image imgImprimir=new Image(getClass().getResourceAsStream("/iconos/printer-16.png"));
            impr.setGraphic(new ImageView(imgImprimir));
            if(!c.isImpreso()){
                impr.setDisable(true);
            }
            
            hbox.getChildren().add(impr);
            hbox.getChildren().add(new Label(c.getIdCupon()+estado));
            
            super.setGraphic(hbox);
            //super.setText(c.getIdCupon().toString()+estado);
        }else{
            //super.setText("");
            super.setGraphic(null);
        }
        super.updateItem(c, empty);
    }
    
}
