/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exabit.dpromo.generador;

import com.exabit.dpromo.bd.ConexionBD;
import com.exabit.dpromo.bd.controllers.CiudadJPAController;
import com.exabit.dpromo.bd.controllers.ClienteJPAController;
import com.exabit.dpromo.bd.controllers.CuponJPAController;
import com.exabit.dpromo.bd.controllers.FacturaJPAController;
import com.exabit.dpromo.bd.controllers.PromocionJPAController;
import com.exabit.dpromo.bd.controllers.TiendaJPAController;
import com.exabit.dpromo.bd.entidades.Ciudad;
import com.exabit.dpromo.bd.entidades.Cliente;
import com.exabit.dpromo.bd.entidades.Cupon;
import com.exabit.dpromo.bd.entidades.Factura;
import com.exabit.dpromo.bd.entidades.Promocion;
import com.exabit.dpromo.bd.entidades.RangoPromocion;
import com.exabit.dpromo.bd.entidades.Tienda;
import com.exabit.dpromo.impresion.ImpresionCupon;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.ToolBar;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;

/**
 * FXML Controller class
 *
 * @author cristhian
 */
public class GeneradorCuponesController implements Initializable {

    private static final Logger LOG = Logger.getLogger(GeneradorCuponesController.class.getName());

    @FXML
    private TextField txfCi;
    @FXML
    private TextField txfNombres;
    @FXML
    private TextField txfApellidos;
    @FXML
    private TextField txfDireccion;
    @FXML
    private TextField txfTelefono;
    @FXML
    private TextField txfCelular;
    @FXML
    private TextField txfEmail;
    @FXML
    private ComboBox<Ciudad> cmbCiudad;
    @FXML
    private ComboBox<Tienda> cmbTienda;
    @FXML
    private TextField txfMontoCompra;
    @FXML
    private Button btnGenerarCupones;
    @FXML
    private TableView<FacturaCuponModelo> tblCupones;
    @FXML
    private TableColumn<FacturaCuponModelo, Tienda> colTienda;
    @FXML
    private TableColumn<FacturaCuponModelo, Number> colMonto;
    @FXML
    private Label lblMonto;
    @FXML
    private TableColumn<FacturaCuponModelo, Date> colFechaHoraReg;
    @FXML
    private ListView<Cupon> lvwCupones;
    @FXML
    private Button btnImprimirCupones;
    @FXML
    private Button btnLimpiar;
    @FXML
    private ToolBar tbarGenerador;
    @FXML
    private Button btnClienteNuevo;
    @FXML
    private Label lblTotalFactura;
    @FXML
    private Button btnAgregarFactura;
    @FXML
    private CheckBox chkFactProcesadas;
    @FXML
    private CheckBox chkCuponesImpresos;
    @FXML
    private Label lblCantidadCupones;
    @FXML
    private TableColumn<FacturaCuponModelo, Factura> colEliminarFactura;
    @FXML
    private Button btnActualizarCiudades;
    @FXML
    private SplitPane sppanePrincipal;
    @FXML
    private AnchorPane apPrincipal;

    private final ObservableList<Tienda> lstTiendas = FXCollections.observableArrayList();
    private final ObservableList<Ciudad> lstCiudades = FXCollections.observableArrayList();
    private final ObservableList<FacturaCuponModelo> lstFacturaCuponModelo = FXCollections.observableArrayList();
    private final ObservableList<Cupon> lstCupones = FXCollections.observableArrayList();

    private final DecimalFormat formato = new DecimalFormat("###,###,###,###,###");

    private final SimpleObjectProperty<Cliente> clienteActual = new SimpleObjectProperty<>();
    private final SimpleObjectProperty<Promocion> promocionActual = new SimpleObjectProperty<>();

    private final List<Button> lstModoClienteNuevo = new ArrayList<>();
    private final List<Button> lstModoOperacion = new ArrayList<>();

    private final SimpleBooleanProperty modoAgregarCliente = new SimpleBooleanProperty();

    private final ChangeListener<Cliente> chLnrClienteActual = (ObservableValue<? extends Cliente> obs, Cliente oldValue, Cliente newValue) -> {
        if (newValue == null) {
            this.txfMontoCompra.setDisable(true);
            this.cmbTienda.setDisable(true);
            this.btnGenerarCupones.setDisable(true);
            //this.btnImprimirCupones.setDisable(true);
            this.btnAgregarFactura.setDisable(true);

            this.txfNombres.setText("");
            this.txfApellidos.setText("");
            this.txfCelular.setText("");
            this.txfDireccion.setText("");
            this.txfEmail.setText("");
            this.txfTelefono.setText("");
            this.cmbCiudad.getSelectionModel().clearSelection();

            this.lstFacturaCuponModelo.clear();
            this.lstCupones.clear();
            this.chkFactProcesadas.setSelected(false);
            this.chkFactProcesadas.setDisable(true);

            this.chkCuponesImpresos.setSelected(false);
            this.chkCuponesImpresos.setDisable(true);
        } else {
            this.btnAgregarFactura.setDisable(false);
            this.txfMontoCompra.setDisable(false);
            this.cmbTienda.setDisable(false);
            if (!this.chkFactProcesadas.isSelected() && !this.lstFacturaCuponModelo.isEmpty()) {
                this.btnGenerarCupones.setDisable(false);
            } else {
                this.btnGenerarCupones.setDisable(true);
            }

            this.txfNombres.setText(newValue.getNombres() != null ? newValue.getNombres() : "");
            this.txfApellidos.setText(newValue.getApellidos() != null ? newValue.getApellidos() : "");
            this.txfCelular.setText(newValue.getCelular() != null ? newValue.getCelular() : "");
            this.txfDireccion.setText(newValue.getDireccion() != null ? newValue.getDireccion() : "");
            this.txfEmail.setText(newValue.getEmail() != null ? newValue.getEmail() : "");
            this.txfTelefono.setText(newValue.getTelefono() != null ? newValue.getTelefono() : "");

            this.chkFactProcesadas.setDisable(false);
            this.activarChkFacturasProcesadas();

            this.chkCuponesImpresos.setDisable(false);
            this.activarChkCuponesImpresos();

            if (newValue.getCiudad() != null) {
                for (Ciudad ciu : this.lstCiudades) {
                    if (ciu.getIdCiudad().equals(newValue.getCiudad().getIdCiudad())) {
                        this.cmbCiudad.getSelectionModel().select(ciu);
                        break;
                    }
                }
            }
            FacturaJPAController facjpa = new FacturaJPAController(ConexionBD.EMF.createEntityManager());
            this.lstFacturaCuponModelo.clear();
            List<Factura> lstFact = facjpa.getFacturasClienteSinProcesar(newValue);
            lstFact.stream().forEach((f) -> {
                FacturaCuponModelo fcm = new FacturaCuponModelo(f);
                this.lstFacturaCuponModelo.add(fcm);
            });

            /*CuponJPAController cujpa=new CuponJPAController(ConexionBD.EMF.createEntityManager());
            this.lstCupones.addAll(cujpa.getCuponesNoImpresosPromoActual(this.clienteActual.getValue()));*/
        }
    };

    private final ChangeListener<String> chLnrFormatoNumero = (ObservableValue<? extends String> obs, String oldValue, String newValue) -> {

        try {
            Integer ci = Integer.parseInt(newValue);
            txfCi.setText(formato.format(ci));
            System.out.println("Conversion");
            String strNumero;
        } catch (NumberFormatException ex) {
            /*try{
                Number
            }catch(ParseException pex){
                
            }*/
            LOG.log(Level.WARNING, "Error al hacer formato inverso de numero", ex);
        }
        /*try{
            Integer ci=Integer.parseInt(newValue);
            
        }catch(NumberFormatException ex){
            LOG.log(Level.WARNING, "Error al convertir string a integer", ex);
        }*/
    };

    private final ChangeListener<FacturaCuponModelo> chLnrFacturaCupon = (ObservableValue<? extends FacturaCuponModelo> obs, FacturaCuponModelo oldValue, FacturaCuponModelo newValue) -> {
        if (newValue == null) {
            this.btnImprimirCupones.setDisable(true);
            this.lstCupones.clear();
        } else {
            this.btnImprimirCupones.setDisable(false);
            FacturaJPAController fajpa = new FacturaJPAController(ConexionBD.EMF.createEntityManager());
            this.lstCupones.clear();
            this.lstCupones.addAll(fajpa.getCuponesFactura(newValue.getFactura()));
        }
    };

    private final ChangeListener<Boolean> chLnrModoAgregarCli = (ObservableValue<? extends Boolean> obs, Boolean oldValue, Boolean newValue) -> {
        if (newValue) {
            this.clienteActual.setValue(null);
            this.txfCi.setText("");

            this.txfNombres.setEditable(true);
            this.txfApellidos.setEditable(true);
            this.txfCelular.setEditable(true);
            this.txfDireccion.setEditable(true);
            this.txfEmail.setEditable(true);
            this.txfTelefono.setEditable(true);
            this.cmbCiudad.setDisable(false);

            this.txfMontoCompra.setDisable(true);
            this.cmbTienda.setDisable(true);
            this.btnGenerarCupones.setDisable(true);
        } else {
            this.txfNombres.setEditable(false);
            this.txfApellidos.setEditable(false);
            this.txfCelular.setEditable(false);
            this.txfDireccion.setEditable(false);
            this.txfEmail.setEditable(false);
            this.txfTelefono.setEditable(false);
            this.cmbCiudad.setDisable(true);
        }
    };

    ListChangeListener<FacturaCuponModelo> lstChlnrFacturas = (ListChangeListener.Change<? extends FacturaCuponModelo> c) -> {
        c.next();
        DecimalFormat df = new DecimalFormat("###,###,###,###,###,###");
        if (c.getList().isEmpty()) {
            this.lblTotalFactura.setText("0");
            this.btnGenerarCupones.setDisable(true);
        } else {
            if (!this.chkFactProcesadas.isSelected()) {
                this.btnGenerarCupones.setDisable(false);
            }
            Integer totalFactura = 0;
            for (FacturaCuponModelo fcm : this.lstFacturaCuponModelo) {
                totalFactura = totalFactura + fcm.getMonto();
            }
            this.lblTotalFactura.setText(df.format(totalFactura));
            /*if(!this.chkFactProcesadas.isSelected()){
                this.btnEliminarFact.setDisable(false);
            }*/
        }
    };

    private final ListChangeListener<Cupon> lstChLnrCupones = (ListChangeListener.Change<? extends Cupon> c) -> {
        c.next();
        if (c.getList().isEmpty()) {
            this.btnImprimirCupones.setDisable(true);
            this.lblCantidadCupones.setText("0");
        } else {
            this.lblCantidadCupones.setText(this.lstCupones.size() + "");
            if (!this.chkCuponesImpresos.isSelected()) {
                this.btnImprimirCupones.setDisable(false);
            }
        }
    };

    private final ChangeListener<Boolean> chLnrFactProcesadas = (ObservableValue<? extends Boolean> obs, Boolean oldValue, Boolean newValue) -> {
        /*if(newValue){
            this.btnEliminarFact.setDisable(true);
        }else{
            this.btnEliminarFact.setDisable(false);
        }*/
    };
    
    

    /**
     * Initializes the controller class.
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        this.cmbTienda.prefWidthProperty().bind(this.txfMontoCompra.widthProperty());
        //this.cmbTienda.setPrefWidth(this.txfMontoCompra.getWidth());
        this.btnActualizarCiudades.setText("");
        this.cargarIconos();
        this.lstFacturaCuponModelo.addListener(this.lstChlnrFacturas);
        this.lstCupones.addListener(this.lstChLnrCupones);
        this.inicializarCiudades();
        this.inicializarTiendas();
        this.inicializarTablaCupones();
        this.inicializarListaCupones();
        this.clienteActual.addListener(chLnrClienteActual);
        this.chkFactProcesadas.selectedProperty().addListener(this.chLnrFactProcesadas);

        this.cmbCiudad.prefWidthProperty().bind(this.txfCi.widthProperty());

        //this.tblCupones.getSelectionModel().selectedItemProperty().addListener(chLnrFacturaCupon);
        this.modoAgregarCliente.addListener(this.chLnrModoAgregarCli);

        PromocionJPAController projpa = new PromocionJPAController(ConexionBD.EMF.createEntityManager());
        this.promocionActual.setValue(projpa.getPromocionActiva());

        this.lstModoOperacion.add(this.btnClienteNuevo);
        this.lstModoOperacion.add(this.btnGenerarCupones);
        this.lstModoOperacion.add(this.btnImprimirCupones);
        this.lstModoOperacion.add(this.btnLimpiar);

        Button btnAceptarCliente = new Button();
        btnAceptarCliente.setText("Guardar");
        Button btnCancelarCliente = new Button();
        btnCancelarCliente.setText("Cancelar");

        Image imgGuardar = new Image(getClass().getResourceAsStream("/iconos/save-16.png"));
        btnAceptarCliente.setGraphic(new ImageView(imgGuardar));

        Image imgCancelar = new Image(getClass().getResourceAsStream("/iconos/cancel-16.png"));
        btnCancelarCliente.setGraphic(new ImageView(imgCancelar));

        btnCancelarCliente.setOnAction((evt) -> {
            this.tbarGenerador.getItems().clear();
            this.tbarGenerador.getItems().addAll(this.lstModoOperacion);
            this.modoAgregarCliente.setValue(false);
        });

        btnAceptarCliente.setOnAction((evt) -> {

            this.registrarCliente();
        });

        this.lstModoClienteNuevo.add(btnAceptarCliente);
        this.lstModoClienteNuevo.add(btnCancelarCliente);
        this.txfCi.requestFocus();
        /*Platform.runLater(()->{
            double dvPos=sppanePrincipal.getDividerPositions()[0];
            dvPos=dvPos-0.1;
            sppanePrincipal.setDividerPosition(0, dvPos);
        });*/
        this.txfMontoCompra.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> obs, String o, String n) {
                if(!n.isEmpty()){
                    DecimalFormat df=new DecimalFormat("#,###,###,###");
                    try{
                        Integer monto=Integer.parseInt(n.replace(".", "").replace(",", ""));
                        txfMontoCompra.setText(df.format(monto));
                    }catch(NumberFormatException ex){
                        txfMontoCompra.setText(o);
                        System.out.println("Error al convetir monto de string a int: "+ex);
                    }
                }
            }
        });
    }

    private void cargarIconos() {
        Image imgAddCli = new Image(getClass().getResourceAsStream("/iconos/add-user-16.png"));
        this.btnClienteNuevo.setGraphic(new ImageView(imgAddCli));

        Image imgLimpiar = new Image(getClass().getResourceAsStream("/iconos/clean-16.png"));
        this.btnLimpiar.setGraphic(new ImageView(imgLimpiar));

        Image imgImprimir = new Image(getClass().getResourceAsStream("/iconos/printer-16.png"));
        this.btnImprimirCupones.setGraphic(new ImageView(imgImprimir));

        Image imgTicket = new Image(getClass().getResourceAsStream("/iconos/ticket-16.png"));
        this.btnGenerarCupones.setGraphic(new ImageView(imgTicket));

        Image imgAddFact = new Image(getClass().getResourceAsStream("/iconos/add-notes-16.png"));
        this.btnAgregarFactura.setGraphic(new ImageView(imgAddFact));

        Image imgDelFactura = new Image(getClass().getResourceAsStream("/iconos/document-delete-16.png"));
        //this.btnEliminarFact.setGraphic(new ImageView(imgDelFactura));

        Image imgRefresh = new Image(getClass().getResourceAsStream("/iconos/x16/refresh.png"));
        this.btnActualizarCiudades.setGraphic(new ImageView(imgRefresh));
    }

    private void inicializarTiendas() {
        this.cmbTienda.setItems(lstTiendas);
        this.cmbTienda.setCellFactory((ListView<Tienda> valor) -> new TiendaListCell());
        this.cmbTienda.setButtonCell(new TiendaListCell());
        TiendaJPAController tijpa = new TiendaJPAController(ConexionBD.EMF.createEntityManager());
        this.lstTiendas.addAll(tijpa.getTiendasActivasOrdenado());
    }

    private void inicializarCiudades() {
        this.cmbCiudad.setItems(lstCiudades);
        this.cmbCiudad.setCellFactory((ListView<Ciudad> valor) -> new CiudadListCell());
        this.cmbCiudad.setButtonCell(new CiudadListCell());
        CiudadJPAController ciujpa = new CiudadJPAController(ConexionBD.EMF.createEntityManager());
        this.lstCiudades.addAll(ciujpa.getCiudadesOrdenado());
    }

    private void inicializarTablaCupones() {
        this.tblCupones.setItems(lstFacturaCuponModelo);
        this.colTienda.setCellValueFactory(cellData -> cellData.getValue().tiendaProperty());
        this.colMonto.setCellValueFactory(cellData -> cellData.getValue().montoProperty());
        //this.colCantidad.setCellValueFactory(cellData -> cellData.getValue().cantCuponesProperty());
        this.colFechaHoraReg.setCellValueFactory(cellData -> cellData.getValue().fechaHoraRegProperty());

        this.colTienda.setCellFactory((TableColumn<FacturaCuponModelo, Tienda> data) -> new TiendaTableCell());
        this.colFechaHoraReg.setCellFactory((TableColumn<FacturaCuponModelo, Date> data) -> new FechaRegistroTableCell());
        this.colMonto.setCellFactory((TableColumn<FacturaCuponModelo, Number> data) -> new MontoTableCell());

        this.colEliminarFactura.setCellValueFactory(cellData -> cellData.getValue().facturaProperty());
        this.colEliminarFactura.setCellFactory((TableColumn<FacturaCuponModelo, Factura> data) -> new EliminarFacturaTableCell());
    }

    private void inicializarListaCupones() {
        this.lvwCupones.setItems(lstCupones);
        this.lvwCupones.setCellFactory((ListView<Cupon> data) -> new CuponListCell());
    }

    private void agregarFactura() {
        PromocionJPAController prjpa = new PromocionJPAController(ConexionBD.EMF.createEntityManager());
        this.promocionActual.setValue(prjpa.getPromocionActiva());
        this.chkFactProcesadas.setSelected(false);
        this.activarChkFacturasProcesadas();
        if (!this.modoAgregarCliente.getValue()) {
            Alert al = new Alert(AlertType.WARNING);
            al.setTitle("Generar Cupones");
            if (this.clienteActual.getValue() != null) {
                if (!this.cmbTienda.getSelectionModel().isEmpty()) {
                    try {
                        Integer montoFactura = Integer.parseInt(this.txfMontoCompra.getText().replace(".", "").replace(",", ""));
                        if (montoFactura >= 0) {
                            FacturaJPAController factjpa = new FacturaJPAController(ConexionBD.EMF.createEntityManager());
                            FacturaCuponModelo fc = new FacturaCuponModelo();
                            fc.setTienda(this.cmbTienda.getSelectionModel().getSelectedItem());
                            fc.setMonto(montoFactura);
                            fc.setFechaHoraReg(new Date());

                            Factura fa = new Factura();
                            fa.setProcesado(false);
                            fa.setCliente(this.clienteActual.getValue());
                            fa.setFechaHoraRegistro(new GregorianCalendar());
                            fa.setMontoFactura(fc.getMonto());
                            fa.setPromocion(this.promocionActual.getValue());
                            fa.setTienda(fc.getTienda());
                            factjpa.registrarFactura(fa);

                            fc.setFactura(fa);
                            this.lstFacturaCuponModelo.add(fc);
                            this.cmbTienda.getSelectionModel().clearSelection();
                            this.txfMontoCompra.clear();
                            this.cmbTienda.requestFocus();
                        } else {
                            al.setContentText("El monto ingresado debe ser mayor a Cero");
                            al.showAndWait();
                        }
                    } catch (Exception ex) {
                        al.setContentText("Introduzca un monto válido en el campo:\n" + this.lblMonto.getText());
                        al.showAndWait();
                        LOG.log(Level.WARNING, "Error al convertir monto de venta de String a Integer", ex);
                    }
                } else {
                    al.setContentText("Seleccione una tienda");
                    al.showAndWait();
                }
            } else {
                al.setContentText("Ingrese un cliente");
                al.showAndWait();
            }
        }
    }

    private void registrarCliente() {
        Alert al = new Alert(AlertType.WARNING);
        al.setTitle("Registrar Cliente");
        try {
            Integer ci = Integer.parseInt(this.txfCi.getText());
            if (!this.txfNombres.getText().isEmpty()) {
                if (!this.txfApellidos.getText().isEmpty()) {
                    if (!this.cmbCiudad.getSelectionModel().isEmpty()) {
                        Cliente c = new Cliente();
                        c.setCi(ci);
                        c.setApellidos(this.txfApellidos.getText());
                        c.setCelular(this.txfCelular.getText());
                        c.setCiudad(this.cmbCiudad.getSelectionModel().getSelectedItem());
                        c.setDireccion(this.txfDireccion.getText());
                        c.setEmail(this.txfEmail.getText());
                        c.setNombres(this.txfNombres.getText());
                        c.setTelefono(this.txfTelefono.getText());
                        ClienteJPAController clijpa = new ClienteJPAController(ConexionBD.EMF.createEntityManager());
                        clijpa.registrarCliente(c);
                        this.clienteActual.setValue(c);

                        this.tbarGenerador.getItems().clear();
                        this.tbarGenerador.getItems().addAll(this.lstModoOperacion);
                        this.modoAgregarCliente.setValue(false);
                    } else {
                        al.setContentText("No se ha seleccionado ninguna Ciudad");
                        al.showAndWait();
                    }

                } else {
                    al.setContentText("El campo 'Apellidos' está vacío");
                    al.showAndWait();
                }
            } else {
                al.setContentText("El campo 'Nombres' está vacío");
                al.showAndWait();
            }
        } catch (Exception ex) {
            LOG.log(Level.WARNING, "Error al convertir String  a Integer ci", ex);
            al.setContentText("Ingrese un RUC/CI válido");
            al.showAndWait();
        }
    }

    private void activarChkFacturasProcesadas() {
        FacturaJPAController fajpa = new FacturaJPAController(ConexionBD.EMF.createEntityManager());
        this.lstFacturaCuponModelo.clear();
        if (this.chkFactProcesadas.isSelected()) {
            this.btnGenerarCupones.setDisable(true);
            List<Factura> lstFacturasProc = fajpa.getFacturasClienteProcesadas(this.clienteActual.getValue());
            lstFacturasProc.stream().forEach((fp) -> {
                this.lstFacturaCuponModelo.add(new FacturaCuponModelo(fp));
            });
        } else {
            List<Factura> lstFacturasProc = fajpa.getFacturasClienteSinProcesar(this.clienteActual.getValue());
            lstFacturasProc.stream().forEach((fp) -> {
                this.lstFacturaCuponModelo.add(new FacturaCuponModelo(fp));
            });
            if (!this.lstFacturaCuponModelo.isEmpty()) {
                this.btnGenerarCupones.setDisable(false);
            }
        }
    }

    private void activarChkCuponesImpresos() {
        CuponJPAController cujpa = new CuponJPAController(ConexionBD.EMF.createEntityManager());
        this.lstCupones.clear();
        if (this.chkCuponesImpresos.isSelected()) {
            this.btnImprimirCupones.setDisable(true);
            this.lstCupones.addAll(cujpa.getCuponesImpresosPromoActual(this.clienteActual.getValue()));
        } else {

            this.lstCupones.addAll(cujpa.getCuponesNoImpresosPromoActual(this.clienteActual.getValue()));
            if (!this.lstCupones.isEmpty()) {
                this.btnImprimirCupones.setDisable(false);
            }
        }
    }

    @FXML
    private void onActionCi(ActionEvent event) {
        if (this.txfCi.getText().isEmpty()) {
            this.clienteActual.setValue(null);
        } else {
            try {
                Integer ci = Integer.parseInt(this.txfCi.getText());
                ClienteJPAController clijpa = new ClienteJPAController(ConexionBD.EMF.createEntityManager());

                Cliente c = clijpa.getClientePorCi(ci);
                if (c != null) {
                    this.clienteActual.setValue(c);
                        this.cmbTienda.requestFocus();
                } else {
                    Alert al = new Alert(AlertType.INFORMATION);
                    al.setTitle("Cliente");
                    al.setContentText("Cliente con RUC/CI número " + ci + " no encontrado");
                    al.showAndWait();
                }
            } catch (NumberFormatException ex) {
                System.out.println("Error al convertir ci");
            }
        }
    }

    @FXML
    private void onActionTxfMonto(ActionEvent event) {
        this.agregarFactura();
    }

    @FXML
    private void onActionBtnAgregarFactura(ActionEvent event) {
        this.agregarFactura();
    }

    @FXML
    private void onActionBtnImprimirCupones(ActionEvent event) {
        ImpresionCupon impr = new ImpresionCupon();
        impr.imprimir(this.clienteActual.getValue(), this.lstCupones);
        this.activarChkCuponesImpresos();
    }

    @FXML
    private void onActionBtnLimpiar(ActionEvent event) {
        this.clienteActual.setValue(null);
        this.txfCi.setText("");
    }

    @FXML
    private void onActionBtnClienteNuevo(ActionEvent event) {
        this.tbarGenerador.getItems().clear();
        this.tbarGenerador.getItems().addAll(this.lstModoClienteNuevo);
        this.modoAgregarCliente.setValue(true);
    }

    @FXML
    private void onActionBtnGenerarCupones(ActionEvent event) {
        if (this.generaCupones()) {
            CuponJPAController cujpa = new CuponJPAController(ConexionBD.EMF.createEntityManager());
            List<Factura> lstFacturas = new ArrayList<>();
            this.lstFacturaCuponModelo.stream().forEach((fcm) -> {
                lstFacturas.add(fcm.getFactura());
            });
            this.lstFacturaCuponModelo.clear();
            List<Cupon> lstCuponesGenerados = cujpa.generarCupones(lstFacturas);
            if (!this.chkCuponesImpresos.isSelected()) {
                this.lstCupones.addAll(lstCuponesGenerados);
            }
        } else {
            Alert al=new Alert(AlertType.WARNING);
            al.setHeaderText("No se generaron cupones");
            al.setContentText("Monto insuficiente o fuera de rango");
            al.show();
        }
    }

    @FXML
    private void onActionChkFactProcesadas(ActionEvent event) {
        this.activarChkFacturasProcesadas();
    }

    @FXML
    private void onActionChkCuponesImpresos(ActionEvent event) {
        this.activarChkCuponesImpresos();
    }

    private void onActionBtnEliminarFactura(ActionEvent event) {
        if (this.tblCupones.getSelectionModel().isEmpty()) {
            Alert al = new Alert(AlertType.INFORMATION);
            al.setTitle("Quitar Factura");
            al.setContentText("Seleccione una factura");
            al.show();
        } else {
            FacturaJPAController fajpa = new FacturaJPAController(ConexionBD.EMF.createEntityManager());
            fajpa.eliminarFactura(this.tblCupones.getSelectionModel().getSelectedItem().getFactura());
            this.lstFacturaCuponModelo.remove(this.tblCupones.getSelectionModel().getSelectedItem());
        }
    }

    @FXML
    private void onActionBtnActualizarCiudades(ActionEvent event) {
        this.lstCiudades.clear();
        CiudadJPAController cjpa = new CiudadJPAController(ConexionBD.EMF.createEntityManager());
        this.lstCiudades.addAll(cjpa.getCiudadesOrdenado());
    }

    private boolean generaCupones() {
        PromocionJPAController pjpa = new PromocionJPAController(ConexionBD.EMF.createEntityManager());
        Promocion promo = pjpa.getPromocionActiva();
        int totalfacturas = 0;
        for (FacturaCuponModelo fc : this.lstFacturaCuponModelo) {
            totalfacturas = totalfacturas + fc.getMonto();
        }
        if (promo.isPorRango()) {
            boolean caeenrango = false;
            List<RangoPromocion> lstrp = pjpa.getRangosPromocion(promo);
            for (RangoPromocion rp : lstrp) {
                if (totalfacturas >= rp.getMontoDesde() && totalfacturas <= rp.getMontoHasta()) {
                    caeenrango = true;
                    break;
                }
            }
            return caeenrango;
        } else {
            if (totalfacturas < promo.getMontoCupon()) {
                return false;
            } else {
                return true;
            }
        }
    }

    @FXML
    private void onActionCmbTienda(ActionEvent event) {
       
    }

    @FXML
    private void onKeyPressCmbTienda(KeyEvent event) {
        if(event.getCode().equals(KeyCode.SPACE)){
            if(!this.cmbTienda.isShowing()){
                this.cmbTienda.show();
            }
        }
    }

}
