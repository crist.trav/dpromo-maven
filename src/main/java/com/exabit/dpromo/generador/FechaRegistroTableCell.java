/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exabit.dpromo.generador;

import java.text.SimpleDateFormat;
import java.util.Date;
import javafx.geometry.Pos;
import javafx.scene.control.TableCell;

/**
 *
 * @author cristhian
 */
public class FechaRegistroTableCell extends TableCell<FacturaCuponModelo, Date> {
 
    @Override
    protected void updateItem(Date d, boolean empty){
        this.setAlignment(Pos.CENTER_LEFT);
        if(d!=null){
            SimpleDateFormat dateFormat= new SimpleDateFormat("dd/MM/yy HH:mm");
            super.setText(dateFormat.format(d));
        }else{
            super.setText("");
        }
        super.updateItem(d, empty);
    }
    
}
