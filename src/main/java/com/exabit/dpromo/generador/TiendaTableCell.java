/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exabit.dpromo.generador;

import com.exabit.dpromo.bd.entidades.Tienda;
import javafx.geometry.Pos;
import javafx.scene.control.TableCell;

/**
 *
 * @author cristhian
 */
public class TiendaTableCell extends TableCell<FacturaCuponModelo, Tienda> {
    
    @Override
    protected void updateItem(Tienda t, boolean empty){
        this.setAlignment(Pos.CENTER_LEFT);
        if(t!=null){
            super.setText(t.getNombre());
        }else{
            super.setText("");
        }
        super.updateItem(t, empty);
    }
    
}
