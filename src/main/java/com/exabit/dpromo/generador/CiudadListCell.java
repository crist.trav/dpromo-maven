/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exabit.dpromo.generador;

import com.exabit.dpromo.bd.entidades.Ciudad;
import javafx.scene.control.ListCell;

/**
 *
 * @author cristhian
 */
public class CiudadListCell extends ListCell<Ciudad> {
    
    @Override
    protected void updateItem(Ciudad ciu, boolean empty){
        if(ciu!=null){
            super.setText(ciu.getNombre()+" - "+ciu.getDepartamento().getNombre());
        }else{
            super.setText("");
        }
        super.updateItem(ciu, empty);
    }
    
}
